﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebPids.Web.Maintenance.Startup))]
namespace WebPids.Web.Maintenance
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
        }
    }
}
