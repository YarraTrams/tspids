﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Web.Mvc; 
using PagedList;
using WebPids.EntityFramework;
using WebPids.BusinessLogic;
using WebPids.Common;

namespace WebPids.Web.Maintenance.Controllers
{
    /// <summary>
    /// The PIDs CRUD
    /// </summary>
    [Authorize]
    public class PidsConfigurationController : Controller
    {
        /// <summary>
        /// The max number of configs to display on this page
        /// </summary>
        private const int ConfigPageSize = 5;
        private const string testUrl = "TestUrl";
 
        #region Configuration CRUD
        /// <summary>
        /// Main page 
        /// </summary>
        /// <param name="sortColumn">column used for sorting</param>
        /// <param name="currentFilter">the filter</param>
        /// <param name="searchString">the search string</param>
        /// <param name="page">the current page</param>
        /// <returns>the main page</returns>
        public ActionResult Index(string sortColumn, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortColumn;
            ViewBag.Message = "Following are the existing Configurations";

            ViewBag.IdSortParm = String.IsNullOrEmpty(sortColumn) ? "Id desc" : "";
            ViewBag.StationNameSortParm = sortColumn == "StationName" ? "StationName desc" : "StationName";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            using (var sp = ServiceProvider.CreateInstance)
            {
                var services = sp.GetService<GlobalSettingServices>();
                var baseDisplayUrl = services.GetValByName(testUrl);

                var configurationServices = sp.GetService<ConfigurationServices>();
                var configurations = configurationServices.GetAllWithHealth();

                foreach (var item in configurations)
                {
                    item.BaseDisplayUrl = baseDisplayUrl;
                }

                if (!String.IsNullOrEmpty(searchString))
                {
                    configurations = configurations.Where(s => s.StationName.ToLower().Contains(searchString.ToLower()));
                }

                switch (sortColumn)
                {
                    case "Id desc":
                        configurations = configurations.OrderByDescending(s => s.Id);
                        break;
                    case "StationName":
                        configurations = configurations.OrderBy(s => s.StationName);
                        break;
                    case "StationName desc":
                        configurations = configurations.OrderByDescending(s => s.StationName);
                        break;
                    default:
                        configurations = configurations.OrderBy(s => s.Id);
                        break;
                }

                return View(configurations.ToList());
            }
        }

        /// <summary>
        /// Create page
        /// </summary>
        /// <returns>the page with a new model object</returns>
        public ActionResult Create()
        {
            var configuration = new Configuration() {NumOfLines = 3};
            return PartialView("Create", configuration);
        }

        /// <summary>
        /// Create a new config record and return back to root
        /// </summary>
        /// <param name="model">the model to save</param>
        /// <returns>the root page</returns>
        public ActionResult CreateNew(Configuration model)
        {
            model.MonitorHeartBeat = true;
            model.PIDName = string.Empty;
            model.DirectionsId = 1;

            using (var sp = ServiceProvider.CreateInstance)
            {
                var configurationServices = sp.GetService<ConfigurationServices>();
                if (ModelState.IsValid)
                {
                    this.CallCrudMethod(configurationServices.Add, model);
                }

                return RedirectToAction("Index");
            }
        }

        /// <summary>
        /// Display the edit page
        /// </summary>
        /// <param name="model">the model</param>
        /// <returns>contents of the edit page</returns>
        public ActionResult Edit(Configuration model)
        {
            using (var sp = ServiceProvider.CreateInstance)
            {
                var configurationServices = sp.GetService<ConfigurationServices>();
                // The default includes are missing from the model. Lets requery the db
                var obj = configurationServices.GetConfiguration(model.Id);
                return PartialView("Edit", obj);
            }
        }

        /// <summary>
        /// Save the configuration
        /// </summary>
        /// <param name="model">the object ot save</param>
        /// <returns>the root</returns>
        [HttpPost]
        public ActionResult EditConfiguration(Configuration model)
        {
            model.PIDName = string.Empty;

            using (var sp = ServiceProvider.CreateInstance)
            {
                var configurationServices = sp.GetService<ConfigurationServices>();
                this.CallCrudMethod(configurationServices.Update, model);

                return RedirectToAction("Index");
            }
        }

        /// <summary>
        /// Display the delete page
        /// </summary>
        /// <param name="model">the model to use</param>
        /// <returns>the delete page</returns>
        public ActionResult Delete(Configuration model)
        {
            return PartialView("Delete", model);
        }

        /// <summary>
        /// Display the details page
        /// </summary>
        /// <param name="model">the model to use</param>
        /// <returns>the details page</returns>
        public ActionResult Detail(Configuration model)
        {
            return PartialView("Detail", model);
        }

        /// <summary>
        /// Delete a configuration
        /// </summary>
        /// <param name="model">the model to delete</param>
        /// <returns>the root</returns>
        public ActionResult DeleteConfiguration(Configuration model)
        {
            using (var sp = ServiceProvider.CreateInstance)
            {
                var configurationServices = sp.GetService<ConfigurationServices>();
                configurationServices.Delete(model);
                return RedirectToAction("Index");
            }
        }
        #endregion

        #region Stops list methods
        /// <summary>
        /// Initialize the stops list partial view 
        /// </summary>
        /// <param name="model">the stops to populate</param>
        /// <returns>the stopslist view</returns>
        public PartialViewResult InitStopsList(IEnumerable<Stop> model)
        {
            ViewBag.Stops = model;
            return PartialView("StopsList");
        }

        #endregion

        #region Stops CRUD
        /// <summary>
        /// Init the create stops control  
        /// </summary>
        /// <param name="configurationId">the config id</param>
        /// <returns>the control</returns>
        public ActionResult RenderCreateStop(long configurationId)
        {
            return PartialView("CreateStop", new Stop() {ConfigurationId = configurationId});
        }

        /// <summary>
        /// Crate a new stop
        /// </summary>
        /// <param name="model">the model</param>
        /// <returns>error or success</returns>
        [HttpPost]
        public ActionResult CreateNewStop(Stop model)
        {
            using (var sp = ServiceProvider.CreateInstance)
            {
                var stopServices = sp.GetService<StopServices>();
                return this.CallCrudMethod(stopServices.Add, model);
            }
        }

        /// <summary>
        /// Init the edit stops control
        /// </summary>
        /// <returns>the control</returns>
        public ActionResult RenderEditStop()
        {
            return PartialView("EditStop", new Stop());
        }

        /// <summary>
        /// Open the edit stops control
        /// </summary>
        /// <param name="model">the model used to populate</param>
        /// <returns>the control</returns>
        public ActionResult EditStop(Stop model)
        {
            using (var sp = ServiceProvider.CreateInstance)
            {
                var stopServices = sp.GetService<StopServices>();
                var stop = stopServices.GetStopById(model.Id);
                return PartialView("EditStop", stop);
            }
        }

        /// <summary>
        /// Same the stop in db
        /// </summary>
        /// <param name="model">the object</param>
        /// <returns>error or success</returns>
        [HttpPost]
        public ActionResult UpdateStop(Stop model)
        {
            using (var sp = ServiceProvider.CreateInstance)
            {
                var stopServices = sp.GetService<StopServices>();
                return this.CallCrudMethod(stopServices.Update, model);
            }
        }

        /// <summary>
        /// Init the delete stop control
        /// </summary>
        /// <returns>the control</returns>
        public PartialViewResult RenderDeleteStop()
        {
            return PartialView("DeleteStop", new Stop());
        }

        /// <summary>
        /// Open the delete stop control with a object
        /// </summary>
        /// <param name="model">the object</param>
        /// <returns>the control</returns>
        public ActionResult DeleteStop(Stop model)
        {
            return PartialView("DeleteStop", model);
        }

        /// <summary>
        /// Delete the stop from db
        /// </summary>
        /// <param name="model">the object</param>
        /// <returns>error or success</returns>
        [HttpPost]
        public ActionResult RemoveStop(Stop model)
        {
            using (var sp = ServiceProvider.CreateInstance)
            {
                var stopServices = sp.GetService<StopServices>();
                Stop stop = null;
                try
                {
                    stop = stopServices.GetStopById(model.Id);
                    if (stop == null)
                    {
                        return null;
                    }
                }
                catch (Exception excp)
                {
                    var errorDetails = ErrorLogger.LogGenericError(excp);
                    return new ContentResult() { Content = errorDetails };
                }
                return this.CallCrudMethod(stopServices.Delete, stop);
            }
        }

        #endregion
    }
}