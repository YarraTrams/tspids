﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using WebPids.BusinessLogic;
using WebPids.EntityFramework;

namespace WebPids.Web.Maintenance.Controllers
{
    [Authorize]
    public class PidsActivityController : Controller
    {
        /// <summary>
        /// The max number of configs to display on this page
        /// </summary>
        private const int ActivityPageSize = 100;

        // GET: PidsActivity
        /// <summary>
        /// Main page 
        /// </summary>
        /// <param name="sortColumn">column used for sorting</param>
        /// <param name="currentFilter">the filter</param>
        /// <param name="searchString">the search string</param>
        /// <param name="page">the current page</param>
        /// <returns>the main page</returns>
        public ActionResult Index(string sortColumn, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortColumn;
            ViewBag.Message = "Following are the PIDs activity logs";

            ViewBag.IdSortParm = sortColumn == "Id" ? "Id desc" : "Id";
            ViewBag.StationNameSortParm = sortColumn == "StationName" ? "StationName desc" : "StationName";
            ViewBag.MessageSortParm = sortColumn == "Message" ? "Message desc" : "Message";
            ViewBag.UserNameSortParm = sortColumn == "UserName" ? "UserName desc" : "UserName";
            ViewBag.CreatedDateSortParm = sortColumn == "CreatedDate" ? "CreatedDate desc" : "CreatedDate";


            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            using (var sp = ServiceProvider.CreateInstance)
            {
                var configurationsHistoryServices = sp.GetService<ConfigurationsHistoryServices>();
                var records = configurationsHistoryServices.GetAll();

                if (!String.IsNullOrEmpty(searchString))
                {
                    records = records.Where(x => x.ConfigurationsId.ToString().Equals(searchString));
                }

                foreach (var item in records)
                {
                    if (item.Configuration == null)
                    {
                        item.Configuration = new Configuration { Id = 0 };
                    }
                }

                switch (sortColumn)
                {
                    case "Id":
                        records = records.OrderBy(x => x.Configuration.Id);
                        break;
                    case "Id desc":
                        records = records.OrderByDescending(x => x.Configuration.Id);
                        break;
                    case "StationName":
                        records = records.OrderBy(x => x.Configuration.StationName);
                        break;
                    case "StationName desc":
                        records = records.OrderByDescending(x => x.Configuration.StationName);
                        break;
                    case "Message":
                        records = records.OrderBy(x => x.Message);
                        break;
                    case "Message desc":
                        records = records.OrderByDescending(x => x.Message);
                        break;
                    case "UserName":
                        records = records.OrderBy(x => x.UserName);
                        break;
                    case "UserName desc":
                        records = records.OrderByDescending(x => x.UserName);
                        break;
                    case "CreatedDate":
                        records = records.OrderBy(x => x.CreatedDate);
                        break;
                    case "CreatedDate desc":
                        records = records.OrderByDescending(x => x.CreatedDate);
                        break;
                    default:
                        records = records.OrderByDescending(x => x.Id);
                        break;
                }

                var pageNumber = (page ?? 1);
                return View(records.ToList().ToPagedList(pageNumber, ActivityPageSize));
            }
        }
    }
}