﻿using System;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using PagedList;
using WebPids.BusinessLogic;
using WebPids.Common;
using WebPids.EntityFramework;

namespace WebPids.Web.Maintenance.Controllers
{
    [Authorize]
    public class GlobalSettingsController : Controller
    {
        /// <summary>
        /// Main CRUD display
        /// </summary>
        /// <param name="currentFilter"></param>
        /// <param name="searchString"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public ActionResult Index(string currentFilter, string searchString, int? page)
        {
            ViewBag.Message = "Following is the existing ticker message";
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            using (var sp = ServiceProvider.CreateInstance)
            {
                var services = sp.GetService<GlobalSettingServices>();

                var data = services.GetAll();

                if (!string.IsNullOrEmpty(searchString))
                {
                    data = data.Where(s => s.Name.Contains(searchString));
                }

                var pageNumber = (page ?? 1);
                return View(data.ToList().ToPagedList(pageNumber, 4));
            }
        }
         

        /// <summary>
        /// Create a new display style object
        /// </summary>
        /// <returns>the view</returns>
        public ActionResult Create()
        {
            return PartialView("Create", new WebPids.EntityFramework.GlobalSetting());
        }

        /// <summary>
        /// Save the object to db
        /// </summary>
        /// <param name="model">the object</param>
        /// <returns>returns back to the root</returns>
        [HttpPost]
        public ActionResult CreateNew(GlobalSetting model)
        {
            using (var sp = ServiceProvider.CreateInstance)
            {
                var services = sp.GetService<GlobalSettingServices>();
                this.CallCrudMethod(services.Add, model);
                return RedirectToAction("Index");
            }
        }

        /// <summary>
        /// Open the edit page
        /// </summary>
        /// <param name="model">the object used</param>
        /// <returns>the page</returns>
        public ActionResult Edit(GlobalSetting model)
        {
            using (var sp = ServiceProvider.CreateInstance)
            {
                var services = sp.GetService<GlobalSettingServices>();

                // The default includes are missing from the model. Lets requery the db
                var obj = services.Get(model.Id);

                return PartialView("Edit", obj);
            }
        }

        /// <summary>
        /// Save the global setting
        /// </summary>
        /// <param name="model">the oject to save</param>
        /// <returns>the root page</returns>
        public ActionResult EditGlobalSetting(GlobalSetting model)
        {
            model.Value = model.Value ?? string.Empty;
            using (var sp = ServiceProvider.CreateInstance)
            {
                var services = sp.GetService<GlobalSettingServices>();
                this.CallCrudMethod(services.Update, model);
                return RedirectToAction("Index");
            }
        }

        /// <summary>
        /// Init the delete page
        /// </summary>
        /// <param name="model">the model to use</param>
        /// <returns>the delete page</returns>
        public ActionResult Delete(GlobalSetting model)
        {
            return PartialView("Delete", model);
        }

        /// <summary>
        /// Delete the object from db
        /// </summary>
        /// <param name="model">the model to use</param>
        /// <returns>back to root</returns>
        public ActionResult DeleteGlobalSetting(GlobalSetting model)
        {
            using (var sp = ServiceProvider.CreateInstance)
            {
                var services = sp.GetService<GlobalSettingServices>();
                this.CallCrudMethod(services.Delete, model);
                return RedirectToAction("Index");
            }
        }
    }
}