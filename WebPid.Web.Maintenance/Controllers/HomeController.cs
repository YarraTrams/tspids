﻿using System.Web.Mvc; 

namespace WebPids.Web.Maintenance.Controllers
{
    /// <summary>
    /// The mother page (does nothing much at this stage)
    /// </summary>
    [Authorize]
    public class HomeController : Controller
    {
        /// <summary>
        /// The main page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
    }
}