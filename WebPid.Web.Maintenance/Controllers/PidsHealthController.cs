﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using WebPids.BusinessLogic;
using WebPids.EntityFramework;

namespace WebPids.Web.Maintenance.Controllers
{
    [Authorize]
    public class PidsHealthController : Controller
    {
        /// <summary>
        /// The max number of pids to display on this page
        /// </summary>

        /// <summary>
        /// Main page 
        /// </summary>
        /// <param name="sortColumn">column used for sorting</param>
        /// <param name="currentFilter">the filter</param>
        /// <param name="searchString">the search string</param>
        /// <returns>the main page</returns>
        public ActionResult Index(string sortColumn, string currentFilter, string searchString, string refreshRate)
        {

            if (searchString == null)
            {
                searchString = currentFilter;
            }

            UpdateViewBagParameters(sortColumn, searchString, refreshRate);


            var items = GetConfigurations(sortColumn, searchString);

            return View(items);
        }

        public ActionResult RefreshStatus(string sortColumn, string currentFilter, string searchString, string refreshRate)
        {
            UpdateViewBagParameters(sortColumn, searchString, refreshRate);
            return PartialView("HealthStatus", GetConfigurations(sortColumn, searchString));
        }

        public ActionResult Details(Configuration model)
        {
            // Load errors
            using (var sp = ServiceProvider.CreateInstance)
            {
                var service = sp.GetService<PidsElevatedLogServices>();
                model.Errors = service.GetByPidId(model.Id);
            } 
            return PartialView("Details", model);
        }

        #region Error List Methods

        public PartialViewResult InitErrorList(IEnumerable<PidsElevatedLog> model)
        {
            Session["Errors"] = model;
            ViewBag.Errors = model.ToPagedList(1, Configuration.ErrorPageSize);
            return PartialView("ErrorList");
        }

        public PartialViewResult GetErrorList(int? page)
        {
            var pageNumber = (page ?? 1);
            var model = Session["Errors"] as IEnumerable<PidsElevatedLog>;
            ViewBag.Errors = model.ToPagedList(pageNumber, Configuration.ErrorPageSize);

            return PartialView("ErrorList");
        }
        #endregion


        #region Private Methods

        private void UpdateViewBagParameters(string sortColumn, string searchString, string refreshRate)
        {
            ViewBag.LastUpdated = string.Format("PIDs Health Statuses at {0} on {1}", DateTime.Now.ToString("hh:mm tt"), DateTime.Now.ToString("dd/MM/yyyy"));
            ViewBag.CurrentFilter = searchString;

            ViewBag.CurrentSort = sortColumn;
            ViewBag.IdSortParm = "IdDesc";
            ViewBag.HealthStatusSortParam = "HealthStatusDesc";

            switch (sortColumn)
            {
                case "Id":
                    ViewBag.IdSortParm = "IdDesc";
                    break;
                case "IdDesc":
                    ViewBag.IdSortParm = "Id";
                    break;
                case "HealthStatus":
                    ViewBag.HealthStatusSortParam = "HealthStatusDesc";
                    break;
                case "HealthStatusDesc":
                    ViewBag.HealthStatusSortParam = "HealthStatus";
                    break;
            }

            if (refreshRate != null)
            {
                ViewBag.RefreshRate = refreshRate;
            }
            else
            {
                ViewBag.RefreshRate = 15000;
            }
        }

        private static IEnumerable<Configuration> GetConfigurations(string sortColumn, string searchString)
        {
            IEnumerable<Configuration> configList;
            using (var sp = ServiceProvider.CreateInstance)
            {
                var configurationServices = sp.GetService<ConfigurationServices>();
                var configurations = configurationServices.GetAllByHealthStatus();

                if (!String.IsNullOrEmpty(searchString) && searchString != HealthStatus.WildCard.Text)
                {
                    configurations = configurations.Where(s => s.HealthStatus.Text.Contains(searchString));
                }

                switch (sortColumn)
                {
                    case "IdDesc":
                        configurations = configurations.OrderByDescending(x => x.Id); 
                        break;
                    case "HealthStatus":
                        configurations = configurations.OrderBy(x => x.HealthStatus.Severity);
                        break;
                    case "HealthStatusDesc":
                        configurations = configurations.OrderByDescending(x => x.HealthStatus.Severity);
                        break;
                    default:
                        configurations = configurations.OrderBy(x => x.Id);
                        break;
                }
                
                configList = configurations.ToList();
            }
            return configList;
        }

        #endregion
    }
}