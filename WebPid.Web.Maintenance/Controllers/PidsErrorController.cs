﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using WebPids.BusinessLogic;
using WebPids.EntityFramework;
using System.Linq.Expressions;

namespace WebPids.Web.Maintenance.Controllers
{
    public class PidsErrorController : Controller
    {
        //
        // GET: /PidsError/
        public ActionResult Index(string searchString, string currentFilter)
        {
            Expression<Func<PidsElevatedLog,bool>> filter = null;
            if (!String.IsNullOrEmpty(searchString))
            {
                filter = x => x.ConfigurationId.ToString().Equals(searchString);
            }

            IEnumerable<PidsElevatedLog> errors;
            using (var sp = ServiceProvider.CreateInstance)
            {
                var service = sp.GetService<PidsElevatedLogServices>();
                errors = service.Get(filter, x => x.OrderByDescending(y => y.Id));
            }

            Session["Errors"] = errors;
            return View();
        }

        public PartialViewResult InitErrorList(int? pageNumber)
        {
            var model = Session["Errors"] as IEnumerable<PidsElevatedLog>;
            ViewBag.Errors = model.ToPagedList(pageNumber ?? 1, PidsElevatedLog.ErrorPageSize);
            return PartialView("ErrorList");
        }

        public ActionResult GetErrorList(int? pageNumber)
        {
            ViewBag.PageNumber = pageNumber;
            return View("Index");
        }
    }
}
