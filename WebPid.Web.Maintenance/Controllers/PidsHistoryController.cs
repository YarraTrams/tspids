﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebPids.BusinessLogic;
using WebPids.EntityFramework;
using WebPids.Web.Maintenance.ViewModels;

namespace WebPids.Web.Maintenance.Controllers
{
    [Authorize]
    public class PidsHistoryController : Controller
    {
        public IEnumerable<PidsElevatedLog> AllIncidents { get; private set; }
        //
        // GET: /PidsHistory/
        public ActionResult Index(string fromDateString, string toDateString, string network)
        {
            var dateFormatString = "dd/MM/yyyy";

            DateTime fromDate;
            DateTime toDate;
            bool showDayWeekMonth;

            var networkType = string.IsNullOrWhiteSpace(network) ? "all" : network;
            ViewBag.NetworkType = networkType;

            if (!(string.IsNullOrWhiteSpace(fromDateString) || string.IsNullOrWhiteSpace(toDateString)))
            {
                ViewBag.ShowDayWeekMonth = false;
                showDayWeekMonth = false;

                fromDate = DateTime.ParseExact(fromDateString, dateFormatString, CultureInfo.InvariantCulture);
                toDate = DateTime.ParseExact(toDateString, dateFormatString, CultureInfo.InvariantCulture).AddDays(1);
            }
            else
            {
                ViewBag.ShowDayWeekMonth = true;
                showDayWeekMonth = true;

                fromDate = DateTime.Today.AddDays(-30);
                toDate = DateTime.Today.AddDays(1);
            }

            ViewBag.FromDate = fromDate.ToString(dateFormatString);
            ViewBag.ToDate = toDate.ToString(dateFormatString);

            var items = GetPidHistoryDetails(fromDate, toDate, showDayWeekMonth, networkType);

            ViewBag.Pids = items.Select(x => string.Format("{0} - {1}", x.PidId, x.PidDescription));

            if (showDayWeekMonth)
            {
                ViewBag.Availability = items.Select(x => x.AvailabilityLastMonth);
                ViewBag.Incidents = items.Select(x => x.NumberOfIncidentsLastMonth);
            }
            else
            {
                ViewBag.Availability = items.Select(x => x.AvailabilityForDuration);
                ViewBag.Incidents = items.Select(x => x.NumberOfIncidentsForDuration);
            }

            return View(items);
        }

        private IEnumerable<PidHistoryViewModel> GetPidHistoryDetails(DateTime fromDate, DateTime toDate, bool showDayWeekMonth, string network)
        {
            var pidHistories = new List<PidHistoryViewModel>();
            var pids = new List<Configuration>();

            using (var sp = ServiceProvider.CreateInstance)
            {
                var service = sp.GetService<ConfigurationServices>();
                
                if (string.Equals(network, "all", StringComparison.OrdinalIgnoreCase))
                {
                    pids = service.GetAll().ToList();
                }
                else
                {
                    pids = service.Get(x => network.Equals(x.NetworkName, StringComparison.OrdinalIgnoreCase)).ToList();
                }

            }

            GetAllIncidents(fromDate, toDate);

            foreach (var pid in pids)
            {
                pidHistories.Add(GetHistoryForPid(pid, fromDate, toDate, showDayWeekMonth));
            }

            if (showDayWeekMonth)
            {
                return pidHistories.OrderByDescending(x => x.DownTimeLastMonth).ThenBy(x => x.PidId).ToList();
            }
            else
            {
                return pidHistories.OrderByDescending(x => x.DownTimeForDuration).ThenBy(x => x.PidId).ToList();
            }
        }

        private void GetAllIncidents(DateTime fromDate, DateTime toDate)
        {
            using (var sp = ServiceProvider.CreateInstance)
            {
                var service = sp.GetService<PidsElevatedLogServices>();
                AllIncidents = service.Get(x => x.Date >= fromDate && x.Date <= toDate && (x.DownTime > 0 || x.Message.Substring(0,7).Equals("OFFLINE", StringComparison.OrdinalIgnoreCase)));
            }
        }

        private PidHistoryViewModel GetHistoryForPid(Configuration pid, DateTime fromDate, DateTime toDate, bool showDayWeekMonth)
        {
            var pidIncidents = AllIncidents.Where(x => x.ConfigurationId == pid.Id);

            var pidHistory = new PidHistoryViewModel{PidId = pid.Id, PidDescription = string.Format("{0} - {1}", pid.StationName, pid.Description), IsEthernet = pid.NetworkName.Equals("Ethernet", StringComparison.OrdinalIgnoreCase)};

            if (showDayWeekMonth)
            {
                pidHistory.NumberOfIncidentsLastDay = GetNumberofIncidentsForPid(pidIncidents, DateTime.Today.AddDays(-1), DateTime.Today.AddDays(1));
                pidHistory.NumberOfIncidentsLastWeek = GetNumberofIncidentsForPid(pidIncidents, DateTime.Today.AddDays(-7), DateTime.Today.AddDays(1));
                pidHistory.NumberOfIncidentsLastMonth = GetNumberofIncidentsForPid(pidIncidents, DateTime.Today.AddDays(-30), DateTime.Today.AddDays(1));
                pidHistory.DownTimeLastDay = GetDowntimeMinutesForPid(pidIncidents, DateTime.Today.AddDays(-1), DateTime.Today.AddDays(1));
                pidHistory.DownTimeLastWeek = GetDowntimeMinutesForPid(pidIncidents, DateTime.Today.AddDays(-7), DateTime.Today.AddDays(1));
                pidHistory.DownTimeLastMonth = GetDowntimeMinutesForPid(pidIncidents, DateTime.Today.AddDays(-30), DateTime.Today.AddDays(1));
                pidHistory.AvailabilityLastDay = GetAvailability(pidHistory.DownTimeLastDay, DateTime.Today.AddDays(-1), DateTime.Today.AddDays(1));
                pidHistory.AvailabilityLastWeek = GetAvailability(pidHistory.DownTimeLastWeek, DateTime.Today.AddDays(-7), DateTime.Today.AddDays(1));
                pidHistory.AvailabilityLastMonth = GetAvailability(pidHistory.DownTimeLastMonth, DateTime.Today.AddDays(-30), DateTime.Today.AddDays(1));
            }
            else
            {
                pidHistory.NumberOfIncidentsForDuration = GetNumberofIncidentsForPid(pidIncidents, fromDate, toDate);
                pidHistory.DownTimeForDuration = GetDowntimeMinutesForPid(pidIncidents, fromDate, toDate);
                pidHistory.AvailabilityForDuration = GetAvailability(pidHistory.DownTimeForDuration, fromDate, toDate);
            }
            return pidHistory;
        }

        private int GetNumberofIncidentsForPid(IEnumerable<PidsElevatedLog> pidIncidents, DateTime fromDate, DateTime toDate)
        {
            int numberOfIncidents = pidIncidents.Where(x => x.Date >= fromDate && x.Date <= toDate).Count();
            return numberOfIncidents;
        }

        private int GetDowntimeMinutesForPid(IEnumerable<PidsElevatedLog> pidIncidents, DateTime fromDate, DateTime toDate)
        {
            /* TODO: 
             * Update this method to get add the latest outage duration if the PID is offline.
             * Check if the latest error starts with "OFFLINE".
             */
            int? outageMinutes;
            outageMinutes = pidIncidents.Where(x => x.Date >= fromDate && x.Date <= toDate && x.DownTime != null).Sum(y => y.DownTime);
            //Add the latest outage duration to outageMinutes here.
            return outageMinutes.HasValue ? outageMinutes.Value : 0;
        }

        private double GetAvailability(int downTime, DateTime fromDate, DateTime toDate)
        {
            double durationMinutes;
            if (toDate > DateTime.Today)
            {
                durationMinutes = DateTime.Now.Subtract(fromDate).TotalHours * 60;
            }
            else
            {
                durationMinutes = toDate.Subtract(fromDate).TotalHours * 60;
            }
            double availability = downTime > durationMinutes ? 0 : Math.Round((durationMinutes - downTime)/durationMinutes * 100, 2);
            return availability;
        }
    }
}
