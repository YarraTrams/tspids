﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Validation;
using System.Drawing;
using System.Linq;
using System.Web.Mvc;
using WebPids.BusinessLogic;
using WebPids.EntityFramework;
using Configuration = WebPids.EntityFramework.Configuration;

namespace WebPids.Web.Maintenance
{
    /// <summary>
    /// Bunch of local extension methods
    /// </summary>
    public static class ExtensionMethods
    {
        /// <summary>
        /// Get the health statuses 
        /// </summary>
        /// <param name="list"></param>
        /// <returns>enum as list</returns>
        public static IEnumerable<HealthStatus> GetHealthStatuses(this System.Collections.Generic.IList<Configuration> list)
        {
            return new List<HealthStatus>()
            {
               HealthStatus.WildCard,
               HealthStatus.Faulty,
               HealthStatus.Warning,
               HealthStatus.Healthy,
               HealthStatus.Disabled
            };
        }
        public static List<SelectListItem> GetRefreshRates(this System.Collections.Generic.IList<Configuration> configList)
        {
            List<SelectListItem> list = new List<SelectListItem>();

            list.Add(new SelectListItem 
            {
                Text = "15 seconds",
                Value = "15000",
                Selected = true
            });

            list.Add(new SelectListItem
            {
                Text = "30 seconds",
                Value = "30000",
            });

            list.Add(new SelectListItem
            {
                Text = "1 minute",
                Value = "60000",
            });

            list.Add(new SelectListItem
            {
                Text = "5 minutes",
                Value = "300000",
            });

            return list;
        }


        /// <summary>
        /// Get the orientation modes
        /// </summary>
        /// <param name="configuration">the class</param>
        /// <returns></returns>
        public static IEnumerable<Orientation> GetOrientationModes(this Configuration configuration)
        {
            return Enum.GetValues(typeof(Orientation)).Cast<Orientation>();
        }

        public static List<SelectListItem> GetNetworkNames(this Configuration configuration)
        {
            List<SelectListItem> networkNames = new List<SelectListItem>();

            networkNames.Add(new SelectListItem
            {
                Text = "3G",
                Value = "3G"
            });

            networkNames.Add(new SelectListItem
            {
                Text = "Ethernet",
                Value = "Ethernet",
                Selected = true
            });

            return networkNames;
        }

        /// <summary>
        /// Get the directions
        /// </summary>
        /// <param name="configuration">the config</param>
        /// <returns></returns>
        public static IEnumerable<Direction> GetDirections(this Configuration configuration)
        {
            var sp = ServiceProvider.CreateInstance;
            return sp.GetService<DirectionsServices>().GetAll();
        }

        public static IEnumerable<Direction> GetDirections(this Stop stop)
        {
            var sp = ServiceProvider.CreateInstance;
            return sp.GetService<DirectionsServices>().GetAll();
        }

        public static string GetDirectionName(this Stop stop)
        {
            var sp = ServiceProvider.CreateInstance;
            var t = sp.GetService<DirectionsServices>().GetAll().FirstOrDefault(n => n.Id == stop.Direction);
            return t != null ? t.Direction1 : string.Empty;
        }

        /// <summary>
        /// Get the display orders
        /// </summary>
        /// <param name="configuration">the config</param>
        /// <returns></returns>
        public static IEnumerable<DisplayOrder> GetDisplayOrders(this Configuration configuration)
        {
            var sp = ServiceProvider.CreateInstance;
            return sp.GetService<DisplayOrderServices>().GetAll();
        }

        /// <summary>
        /// Calls Create/Update/Delete methods and handles exceptions
        /// </summary>
        /// <param name="methodToCall">Method that should be called</param>
        /// <param name="methodParameter">Parameter that should be passed into the method</param>
        public static ActionResult CallCrudMethod<T>(this Controller controller, Action<T> methodToCall,T methodParameter)
        {
            try
            {
                methodToCall.Invoke(methodParameter);
            }
            catch (DbEntityValidationException excp)
            {
                var errorDetails = ErrorLogger.LogValidationError(excp);
                return new ContentResult() { Content = string.Format("Validation Error : {0}", errorDetails) };
            }
            catch (Exception excp)
            {
                var errorDetails = ErrorLogger.LogGenericError(excp);
                return new ContentResult() { Content = errorDetails };
            }
            return new ContentResult() { Content = "Success" };
        }

    }
}
