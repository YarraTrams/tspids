﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Web;
using WebPids.Common;

namespace WebPids.Web.Maintenance
{
    public static class ErrorLogger
    {
        public static string LogGenericError(Exception excp)
        {
            var errorDetails = string.Format("Station PIDs Configurator has thrown an exception: {0}", excp.ToString());
            WebPidsConsole.WriteEventLog(errorDetails, EventLogEntryType.Warning, Category.NotMonitored);
            WebPidsConsole.WriteLine(errorDetails, LogLevel.Error);
            return errorDetails;
        }

        public static string LogValidationError(DbEntityValidationException excp)
        {
            var sb = new StringBuilder();

            foreach (var failure in excp.EntityValidationErrors)
            {
                sb.AppendFormat("{0} failed validation. \n", failure.Entry.Entity.GetType());
                foreach (var error in failure.ValidationErrors)
                {
                    sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                    sb.AppendLine();
                }
            }

            WebPidsConsole.WriteEventLog(sb.ToString(), EventLogEntryType.Warning, Category.NotMonitored);
            WebPidsConsole.WriteLine(sb.ToString(), LogLevel.Warning);
            return sb.ToString();
        }
    }
}