﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPids.Web.Maintenance.ViewModels
{
    public class PidHistoryViewModel
    {
        public long PidId { get; set; }
        public string PidDescription { get; set; }
        public bool IsEthernet { get; set; }
        public int NumberOfIncidentsLastDay { get; set; }
        public int NumberOfIncidentsLastWeek { get; set; }
        public int NumberOfIncidentsLastMonth { get; set; }
        public int NumberOfIncidentsForDuration { get; set; }
        public int DownTimeLastDay { get; set; }
        public int DownTimeLastWeek { get; set; }
        public int DownTimeLastMonth { get; set; }
        public int DownTimeForDuration { get; set; }
        public double AvailabilityLastDay { get; set; }
        public double AvailabilityLastWeek { get; set; }
        public double AvailabilityLastMonth { get; set; }
        public double AvailabilityForDuration { get; set; }
    }
}