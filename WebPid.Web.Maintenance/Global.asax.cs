﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing; 
using log4net.Config;
using WebPids.BusinessLogic;
using WebPids.Common;


namespace WebPids.Web.Maintenance
{
    /// <summary>
    /// The app
    /// </summary>
    public class MvcApplication : System.Web.HttpApplication
    {
        /// <summary>
        /// The application start
        /// </summary>
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // Log4net
            XmlConfigurator.Configure();

            WebPidsConsole.WriteLine("Application_Start Ok", LogLevel.Info);
        }

        /// <summary>
        /// Application terminate
        /// </summary>
        protected void Application_End()
        {
            WebPidsConsole.WriteLine("Application_End Ok", LogLevel.Info);
        }
 
        /// <summary>
        /// The database name
        /// </summary>
        public static string DatabaseName
        {
            get
            {
                using (var sp = ServiceProvider.CreateInstance)
                {
                    return sp.GetService<ConfigurationServices>().DatabaseName;
                }
            }
        }

        /// <summary>
        /// Check if user is readonly
        /// </summary>
        public static bool IsUserReadOnly(string username)
        {
            using (var sp = ServiceProvider.CreateInstance)
            {
                var obj = sp.GetService<UserGroupServices>().GetLoggedinUserGroup(username);
                // User.Identity.Name
                if (ConfigurationManager.AppSettings["UseDomain"] == "no")
                    return false;

                return obj == null || obj.Readonly;
            }
        }
    }
}
