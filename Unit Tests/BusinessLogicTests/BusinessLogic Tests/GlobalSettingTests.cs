﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebPids.BusinessLogic;
using WebPids.EntityFramework;

namespace WebPids.UnitTests.BusinessLogic_Tests
{
     [TestClass]
    public class GlobalSettingTests
    {

        private GlobalSettingServices globalSettingServices { get; set; }
        private ServiceProvider serviceProvider = null;
       
        [TestInitialize]
        public void Setup()
        {
            serviceProvider = ServiceProvider.CreateInstance;
            globalSettingServices = serviceProvider.GetService<GlobalSettingServices>(); 
        }

        [TestCleanup]
        public void Cleanup()
        {
            serviceProvider.Dispose();
        }

        [TestMethod]
        public void GetAllGlobalSettings()
        {
            var data = globalSettingServices.GetAll();
            Assert.IsNotNull(data);
            Assert.IsTrue(data.Any());
        }


        [TestMethod]
        public void GetAGlobalSetting()
        {
            var data = globalSettingServices.Get(1);
            Assert.IsNotNull(data); 
        }

        [TestMethod]
        public void GetAnExtinctGlobalSetting()
        {
            var data = globalSettingServices.Get(1000);
            Assert.IsNull(data);
        }

        [TestMethod]
        public void GetAGlobalSettingByName()
        {
            var data = globalSettingServices.GetValByName("GenericMessage");
            Assert.IsTrue(data != string.Empty);
        }

        [TestMethod]
        public void GetExtinctGlobalSettingByName()
        {
            var data = globalSettingServices.GetValByName("SomeMessageThatDoesntExist");
            Assert.IsTrue(data == string.Empty);
        }

        [TestMethod]
        public void CRUDGlobalSettingTest()
        {
            // create a new object
            var data = new GlobalSetting()
            {
                Value = "Somevalue",
                Name = "Somename"
            };

            globalSettingServices.Add(data);
            var newid = data.Id;
            Assert.AreNotEqual(newid, 0);

            var obj = globalSettingServices.Get(newid);
            Assert.AreEqual(obj.Id, newid);

            // modify the object
            var update = new GlobalSetting()
            {
                Value = "Changedvalue",
                Id = newid,
                Name = obj.Name
            };

            globalSettingServices.Update(update);

            var getobj = globalSettingServices.Get(newid);
            Assert.AreEqual(getobj.Value, "Changedvalue");

            // delete the object
            globalSettingServices.Delete(getobj);
            getobj = globalSettingServices.Get(newid);
            Assert.IsNull(getobj);
        }

    }
}
