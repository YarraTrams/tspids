﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebPids.BusinessLogic;

namespace WebPids.UnitTests.BusinessLogic_Tests
{
    [TestClass]
    public class PidsElevatedLogTests
    {
        private PidsElevatedLogServices pidsElevatedLogServices { get; set; }
        private ServiceProvider serviceProvider = null;


        [TestInitialize]
        public void Setup()
        {
            serviceProvider = ServiceProvider.CreateInstance;
            pidsElevatedLogServices = serviceProvider.GetService<PidsElevatedLogServices>();
        }

        [TestCleanup]
        public void Cleanup()
        {
            serviceProvider.Dispose();
        }

        [TestMethod]
        public void GetAllLogs()
        {
            var data = pidsElevatedLogServices.GetAll();
            Assert.IsNotNull(data);
        }

        [TestMethod]
        public void Add()
        {
            //pidsElevatedLogServices.Add(1, PidsElevatedLogServices.ErrorLevel.Error, "Test message");
            var data = pidsElevatedLogServices.GetAll();
            Assert.IsTrue(data.Any());
        }

    }
}
