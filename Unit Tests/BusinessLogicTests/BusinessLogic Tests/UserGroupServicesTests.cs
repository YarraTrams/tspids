﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebPids.BusinessLogic;

namespace WebPids.UnitTests.BusinessLogic_Tests
{
    [TestClass]
    public class UserGroupServicesTests
    {
        private UserGroupServices userGroupServices { get; set; }
        private ServiceProvider serviceProvider = null;

        [TestInitialize]
        public void Setup()
        {
            serviceProvider = ServiceProvider.CreateInstance;
            userGroupServices = serviceProvider.GetService<UserGroupServices>();
        }


        [TestCleanup]
        public void Cleanup()
        {
            serviceProvider.Dispose();
        }

        [TestMethod]
        public void GetAllUserGroups()
        {
            var userGroups = userGroupServices.GetAll();
            Assert.IsNotNull(userGroups);
            Assert.IsTrue(userGroups.Any());
        }
    }
}
