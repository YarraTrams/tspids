﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebPids.BusinessLogic;

namespace WebPids.UnitTests.BusinessLogic_Tests
{
    [TestClass]
    public class PidsLogTests
    {
        private PidsLogServices pidsLogServices { get; set; }
        private ServiceProvider serviceProvider = null;


        [TestInitialize]
        public void Setup()
        {
            serviceProvider = ServiceProvider.CreateInstance;
            pidsLogServices = serviceProvider.GetService<PidsLogServices>();
        }

        [TestCleanup]
        public void Cleanup()
        {
            serviceProvider.Dispose();
        }

        [TestMethod]
        public void GetAllLogs()
        {
            var data = pidsLogServices.GetAll();
            Assert.IsNotNull(data);
        }

        [TestMethod]
        public void Add()
        {
            pidsLogServices.Add(1, "Test message");
            var data = pidsLogServices.GetAll();
            Assert.IsTrue(data.Any());
        }
    }
}
