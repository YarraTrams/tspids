﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebPids.BusinessLogic;

namespace WebPids.UnitTests.BusinessLogic_Tests
{
     [TestClass]
    public class DirectionsServicesTests
    {
        private ConfigurationServices configurationServices { get; set; }
        private DirectionsServices directionsServices { get; set; }
        private ServiceProvider serviceProvider = null;

        [TestInitialize]
        public void Setup()
        {
            serviceProvider = ServiceProvider.CreateInstance;
            configurationServices = serviceProvider.GetService<ConfigurationServices>();
            directionsServices = serviceProvider.GetService<DirectionsServices>();
        }

        [TestCleanup]
        public void Cleanup()
        {
            serviceProvider.Dispose();
        }

        [TestMethod]
        public void GetAllDirections()
        {
            var data = directionsServices.GetAll();
            Assert.IsNotNull(data);
            Assert.IsTrue(data.Any());
        }

        [TestMethod]
        public void GetNorth()
        {
            var data = directionsServices.GetDirection(1);
            Assert.IsNotNull(data);
            Assert.IsTrue(data.Direction1 == "North");
        }

        [TestMethod]
        public void GetDirectionForConfiguration()
        {
            var data = configurationServices.GetConfiguration(1);
            Assert.IsNotNull(data);
            Assert.IsNotNull(data.Direction);
        }
    }
}
