﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebPids.BusinessLogic;
using WebPids.Common;
using WebPids.EntityFramework;

namespace WebPids.UnitTests.BusinessLogic_Tests
{
    [TestClass]
    public class ConfigurationServicesTests
    {
        private ConfigurationServices configurationServices { get; set; }
        private StopServices stopServices { get; set; }
        private ServiceProvider serviceProvider = null;

        [TestInitialize]
        public void Setup()
        {
            serviceProvider = ServiceProvider.CreateInstance;
            configurationServices = serviceProvider.GetService<ConfigurationServices>();
            stopServices = serviceProvider.GetService<StopServices>();
        }

        [TestCleanup]
        public void Cleanup()
        {
            serviceProvider.Dispose();
        }

        [TestMethod]
        public void GetAllConfigurations()
        {
            var data = configurationServices.GetAll();
            Assert.IsNotNull(data);
            Assert.IsTrue(data.Any());
        }

        [TestMethod]
        public void GetExistingConfiguration()
        {
            var data = configurationServices.GetConfiguration(1);
            Assert.IsNotNull(data);
        }

        [TestMethod]
        public void GetNonExistingConfiguration()
        {
            var data = configurationServices.GetConfiguration(999);
            Assert.IsNull(data);
        }

        [TestMethod]
        public void CRUDConfigurationTest()
        {
            // Create a new config
            var config = new Configuration()
            {
                Description = "Testing description",
                NetworkName = "127.0.0.1",
                Enabled = false,
                NumOfLines = 3,
                PIDName = "Testing pid",
                MonitorHeartBeat = true,
                HeartbeatLastUpdatedTime = DateTime.Now,
                MonitorPageProcessing = true,
                PageProcessingTimeTaken = 2, 
                DirectionsId = 1, 
                Distance = 50,
                ErrorStatus = 0,
                DisplayOrderId = 1
            };

            configurationServices.Add(config);

            var newid = config.Id;
            Assert.AreNotEqual(newid , 0);

            var newconfig = configurationServices.GetConfiguration(newid);
            Assert.AreEqual(newconfig.Id , newid);

            // Modify the config
            var editconfig = new Configuration()
            {
                Id = config.Id,
                Description = config.Description,
                NetworkName = "localhost",
                Enabled = config.Enabled,
                NumOfLines = config.NumOfLines,
                PIDName = "Changed name",
                MonitorHeartBeat = true,
                HeartbeatLastUpdatedTime = DateTime.Now,
                MonitorPageProcessing = true,
                PageProcessingTimeTaken = 2,
                DirectionsId = 2,
                Distance = 10, 
                ErrorStatus = 2, 
                DisplayOrderId = 2
            };

            configurationServices.Update(editconfig);
            newconfig = configurationServices.GetConfiguration(newid);
            Assert.AreEqual(newconfig.PIDName , "Changed name");
            Assert.AreNotEqual(newconfig.NetworkName, "127.0.0.1");
            Assert.AreEqual(newconfig.NetworkName, "localhost");
            Assert.AreEqual(newconfig.ErrorStatus, true);
            Assert.AreEqual(newconfig.DisplayOrderId, 2);


            // Delete the config
            configurationServices.Delete(newconfig);
            newconfig = configurationServices.GetConfiguration(newid);
            Assert.IsNull(newconfig);

        }

        [TestMethod]
        public void GetPredictionsForStops()
        {
            var result = configurationServices.GetPidData(1);
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.PidsConfiguration);
            Assert.IsTrue(result.PredictionsResults.Any());
            Assert.IsFalse(result.PredictionsResults.FirstOrDefault().HasError);
            Assert.IsNull(result.PredictionsResults.FirstOrDefault().ErrorMessage);
            Assert.AreNotEqual(result.PredictionsResults.FirstOrDefault().ErrorMessage, string.Empty);
        }

        [TestMethod]
        public void GetPredictionsForInvalidStop()
        {
            var result = configurationServices.GetPidData(9999);
            Assert.IsNull(result);
        }

        [TestMethod]
        public void GetPredictionsForValidPidInvalidStops()
        {
            // Create a new config
            var config = new Configuration()
            {
                Description = "GetPredictionsForValidPidInvalidStops",
                Enabled = false,
                NumOfLines = 3,
                PIDName = "Testing Pid",
                MonitorHeartBeat = true,
                HeartbeatLastUpdatedTime = DateTime.Now,
                MonitorPageProcessing = true,
                PageProcessingTimeTaken = 2,
                DirectionsId = 1,
                Distance = 50,
                DisplayOrderId = 1
            };

            configurationServices.Add(config);

            var stop = new Stop() { Configuration = config, ConfigurationId = config.Id, TrackerId = 9999};
            stopServices.Add(stop);

            var result = configurationServices.GetPidData(config.Id);
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.PidsConfiguration);
            Assert.IsTrue(result.PredictionsResults.Any());
            Assert.IsTrue(result.PredictionsResults.FirstOrDefault().HasError);
            Assert.IsNotNull(result.PredictionsResults.FirstOrDefault().ErrorMessage);
            Assert.AreNotEqual(result.PredictionsResults.FirstOrDefault().ErrorMessage, string.Empty);

            configurationServices.Delete(config);
        }

        [TestMethod]
        public void TestUpdatePageProcessingTimeTaken()
        {
            // Create a new config
            var config = new Configuration()
            {
                Description = "UpdatePageProcessingTimeTaken",
                Enabled = false,
                NumOfLines = 3,
                PIDName = "Testing Pid",
                MonitorHeartBeat = true,
                HeartbeatLastUpdatedTime = DateTime.Now,
                MonitorPageProcessing = true,
                PageProcessingTimeTaken = 2,
                DirectionsId = 1,
                Distance = 50,
                DisplayOrderId = 1
            };

            configurationServices.Add(config);
            var timetaken = new TimeSpan(0, 0, 15);
            configurationServices.UpdatePageProcessingTimeTaken(config, timetaken);

            var gotconfig = configurationServices.GetConfiguration(config.Id);
            Assert.IsTrue(gotconfig.PageProcessingTimeTaken == timetaken.Milliseconds);

            var unprocessedPids = configurationServices.GetUnprocessedPids(new TimeSpan(0, 0, 2));
            Assert.IsTrue(unprocessedPids.Any());

            var unprocessedPids2 = configurationServices.GetUnprocessedPids(timetaken);
            Assert.IsFalse(unprocessedPids2.Any());
        }

        [TestMethod]
        public void TestUpdateHealthStatus()
        {
            // Create a new config
            var config = new Configuration()
            {
                Description = "UpdatePageProcessingTimeTaken",
                Enabled = false,
                NumOfLines = 3,
                PIDName = "Testing Pid",
                MonitorHeartBeat = true,
                HeartbeatLastUpdatedTime = DateTime.Now,
                MonitorPageProcessing = true,
                PageProcessingTimeTaken = 2,
                DirectionsId = 1,
                Distance = 50,
                DisplayOrderId = 1
            };

            configurationServices.Add(config);
            var orgdate = config.HeartbeatLastUpdatedTime;
            configurationServices.UpdateHealthStatus(config);
            Assert.IsTrue(config.HeartbeatLastUpdatedTime != orgdate);

            var pids = configurationServices.GetUnhealthyPids(TimeSpan.MinValue);
            Assert.IsTrue(pids.Any());
        }
    }
}
