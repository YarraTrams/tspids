﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebPids.BusinessLogic;

namespace WebPids.UnitTests.BusinessLogic_Tests
{
    [TestClass]
    public class DisplayOrderTests
    {

        private ConfigurationServices configurationServices { get; set; }
        private DisplayOrderServices displayOrderServices { get; set; }
        private ServiceProvider serviceProvider = null;

        [TestInitialize]
        public void Setup()
        {
            serviceProvider = ServiceProvider.CreateInstance;
            configurationServices = serviceProvider.GetService<ConfigurationServices>();
            displayOrderServices = serviceProvider.GetService<DisplayOrderServices>();
        }

        [TestCleanup]
        public void Cleanup()
        {
            serviceProvider.Dispose();
        }

        [TestMethod]
        public void GetAllDisplayOrders()
        {
            var data = displayOrderServices.GetAll();
            Assert.IsNotNull(data);
            Assert.IsTrue(data.Any());
        }

        [TestMethod]
        public void GetDisplayOrderForConfiguration()
        {
            var data = configurationServices.GetConfiguration(1);
            Assert.IsNotNull(data);
            Assert.IsNotNull(data.DisplayOrder);
        }
    }
}
