﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebPids.BusinessLogic;
using WebPids.EntityFramework;

namespace WebPids.UnitTests.BusinessLogic_Tests
{
    [TestClass]
    public class StopsTests
    {
        private ConfigurationServices configurationServices { get; set; }
        private StopServices stopServices { get; set; }
        private ServiceProvider serviceProvider = null;

        [TestInitialize]
        public void Setup()
        {
            serviceProvider = ServiceProvider.CreateInstance;
            configurationServices = serviceProvider.GetService<ConfigurationServices>();
            stopServices = serviceProvider.GetService<StopServices>();
        }

        [TestCleanup]
        public void Cleanup()
        {
            serviceProvider.Dispose();
        }

        [TestMethod]
        public void GetAllStops()
        {
            var stops = stopServices.GetAll();
            Assert.IsNotNull(stops);
            Assert.IsTrue(stops.Any());
        }

         [TestMethod]
        public void GetStop()
         {
             var stop = stopServices.GetStopById(1);
             Assert.IsNotNull(stop);
             Assert.AreNotEqual(stop.Id , 0);
             Assert.IsNotNull(stop.Configuration);
             Assert.AreNotEqual(stop.TrackerId , 0);
         }

        [TestMethod]
        public void GetNonexistingStop()
        {
            var stop = stopServices.GetStopById(999);
            Assert.IsNull(stop);
        }

        [TestMethod]
        public void CRUDStopTests()
        {
            // Create a new config
            var config = new Configuration()
            {
                Description = "Testing description",
                Enabled = false,
                NumOfLines = 3,
                PIDName = "Testing pid",
                MonitorHeartBeat = true,
                HeartbeatLastUpdatedTime = DateTime.Now,
                MonitorPageProcessing = true,
                PageProcessingTimeTaken = 2,
                DirectionsId = 1,
                Distance = 50,
                DisplayOrderId = 1
            };

            configurationServices.Add(config);
            Assert.IsTrue(config.Id != 0);

            // Add stops
            var stop1 = new Stop() {Configuration = config, ConfigurationId = config.Id, TrackerId = 9998};
            stopServices.Add(stop1);
            var stop2 = new Stop() { Configuration = config, ConfigurationId = config.Id, TrackerId = 9999};
            stopServices.Add(stop2);

            // Retrieve stops
            var stops = stopServices.GetStopsForConfiguration(config.Id);
            Assert.IsTrue(stops.Any());

            // Update a stop
            var modifiedStope = new Stop()
            {
                Id = stop1.Id,
                Configuration = stop1.Configuration,
                ConfigurationId = stop1.ConfigurationId,
                TrackerId = stop1.TrackerId,
            };
            stopServices.Update(modifiedStope);

            // extract the stop to check
            var changedStop = stopServices.GetStopById(stop1.Id);
            Assert.AreEqual(changedStop.TrackerId, stop1.TrackerId);

            // Remove second stop from the config
            stopServices.Delete(stop2);
            var deletedstop2 = stopServices.GetStopById(stop2.Id);
            Assert.IsNull(deletedstop2);

            var changedConfig = configurationServices.GetConfiguration(stop1.ConfigurationId);
            Assert.AreEqual(changedConfig.Stops.Count(), 1);

            // Remove the config
            configurationServices.Delete(config);
            var nullstop = stopServices.GetStopById(stop1.Id);
            Assert.IsNull(nullstop);
        }
    }
}
