﻿using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebPids.Common;

namespace WebPids.UnitTests.Common_Tests
{
    [TestClass]
    public class WebPidsConsoleTests
    {
        [TestMethod]
        public void WriteLineTests()
        {
            WebPidsConsole.WriteLine("Debug message");
            WebPidsConsole.WriteLine("Error message", LogLevel.Error);
            WebPidsConsole.WriteLine("Info message", LogLevel.Info);
            WebPidsConsole.WriteLine("Warning message", LogLevel.Warning);
        }

        [TestMethod]
        public void WriteLineTestsWithException()
        {
            WebPidsConsole.WriteLine(new Exception("some exception"), LogLevel.Error);
        }


        [TestMethod]
        public void WriteEventLogTest()
        {
            WebPidsConsole.WriteEventLog(
                "Message to test the event log", 
                EventLogEntryType.Information, 
                Category.NotMonitored
                );
        }

        [TestMethod]
        public void GetCurrentUserName()
        {
            var user = WebPidsConsole.CurrentUserName;
            Assert.IsTrue(user != string.Empty);
        }
    }
}
