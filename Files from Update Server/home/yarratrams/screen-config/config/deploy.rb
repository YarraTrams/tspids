# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'screen_config'

# This needs to be set to the SNV repository URL
# It must be accessible by both this VM and all the screens
set :repo_url, 'svn://10.0.205.22'

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/opt/yarratrams'

# Default value for :scm is :git
set :scm, :svn

# Default value for :format is :pretty
set :format, :pretty

# Default value for :log_level is :debug
set :log_level, :debug

# Default value for keep_releases is 5
set :keep_releases, 5

#set :pty, true
require 'sshkit/sudo'
class SSHKit::Sudo::InteractionHandler
  password_prompt_regexp /\[sudo\] password for yarratrams:/
  def on_data(command, stream_name, data, channel)
    if data =~ password_prompt
      channel.send_data("Password1\n")
    end
  end
end


namespace :deploy do

  desc "Reboot screen"
  task :reboot do
    on roles :config do
      execute :sudo, 'reboot'
    end
  end

  desc "Deploy and reboot"
  task :withreboot do
    on roles :config do
      invoke 'deploy'
      invoke 'deploy:reboot'
    end
  end

  desc "Config PID-specific parameters"
  task :configpid do
    on roles :config do |pid|
      execute :sed, "\"s/%PID_ID%/" + pid.properties.id + "/\" /opt/yarratrams/current/www/App/app.js.template > /opt/yarratrams/current/www/App/app.js"
      execute :sed, "\"s/%SCREEN_ORIENTATION%/" + pid.properties.orientation + "/\" /opt/yarratrams/current/kiosk-orientation.template > /opt/yarratrams/current/kiosk-orientation"
    end
  end

  desc "Refresh web browser"
  task :refreshbrowser do
    on roles :config do
      execute :bash, "/opt/yarratrams/current/browser-refresh.sh"
    end
  end

  desc "Restart web browser"
  task :restartbrowser do
    on roles :config do
      execute :bash, "/opt/yarratrams/current/browser-restart.sh"
    end
  end

  desc "Get SCROT"
  task :getscrot do
    on roles :config do
      execute "DISPLAY=:0 scrot /home/yarratrams/capiscrot.png"
      download! "/home/yarratrams/capiscrot.png", "/home/yarratrams/firmware/capiscrot.png"
      execute "rm -f /home/yarratrams/capiscrot.png"
    end
  end

end

after "deploy", "deploy:configpid"

