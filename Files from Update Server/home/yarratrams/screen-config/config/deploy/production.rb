# Screens to deploy to
# ====================

# Office DEMO PID 
server '10.10.25.5', user: 'yarratrams', roles: %w{config}, id: '67', orientation: ''

# Prarhan PID
# server '10.10.25.1', user: 'yarratrams', roles: %w{config}, id: '60', orientation: ''
# South Yarra PID
# server '10.10.24.2', user: 'yarratrams', roles: %w{config}, id: '22', orientation: ''
# Jewell Station
# server '10.10.24.3', user: 'yarratrams', roles: %w{config}, id: '58', orientation: ''
# Brunswick Station PID
# server '10.10.24.6', user: 'yarratrams', roles: %w{config}, id: '49', orientation: ''
# Glenferrie Station PID
# server '10.10.29.226', user: 'yarratrams', roles: %w(config), id: '14', orientation: ''
# Jolimont Station PID 35
# server '10.10.29.133', user: 'yarratrams', roles: %w{config}, id: '35', orientation: ''
# Jolimont  Station PID 34
# server '10.10.29.135', user: 'yarratrams', roles: %w{config}, id: '34', orientation: ''

# Box Hill PID 1
server '10.10.29.35', user: 'yarratrams', roles: %w{config}, id: '1', orientation: 'left'

# Box Hill PID 2
server '10.10.29.37', user: 'yarratrams', roles: %w{config}, id: '2', orientation: 'left'

# Caufield PID 3
server '10.10.29.69', user: 'yarratrams', roles: %w{config}, id: '3', orientation: ''

# Caufield PID 4
server '10.10.29.71', user: 'yarratrams', roles: %w{config}, id: '4', orientation: ''

# Essendon PID
server '10.10.29.197', user: 'yarratrams', roles: %w{config}, id: '5', orientation: ''

# Flagstaff PID 6
server '10.10.29.101', user: 'yarratrams', roles: %w{config}, id: '6', orientation: ''

# Flagstaff PID 7
server '10.10.29.103', user: 'yarratrams', roles: %w{config}, id: '7', orientation: ''

# Flinders Street PID 8
server '10.10.28.7', user: 'yarratrams', roles: %w{config}, id: '8', orientation: ''

# Flinders Street PID 9
server '10.10.28.9', user: 'yarratrams', roles: %w{config}, id: '9', orientation: ''

# Flinders Street PID 10
server '10.10.28.13', user: 'yarratrams', roles: %w{config}, id: '10', orientation: ''

# Flinders Street PID 11
server '10.10.28.15', user: 'yarratrams', roles: %w{config}, id: '11', orientation: ''

# Flinders Street PID 12
server '10.10.28.11', user: 'yarratrams', roles: %w{config}, id: '12', orientation: ''

# Footscray PID
server '10.10.24.31', user: 'yarratrams', roles: %w{config}, id: '13', orientation: ''

# Glenferrie Station PID
server '10.10.29.229', user: 'yarratrams', roles: %w(config), id: '14', orientation: ''

# Melbourne Central PID 
server '10.10.29.7', user: 'yarratrams', roles: %w{config}, id: '15', orientation: 'left'

# Melbourne Central PID 
server '10.10.29.9', user: 'yarratrams', roles: %w{config}, id: '16', orientation: 'left'

# Melbourne Central PID 
server '10.10.29.5', user: 'yarratrams', roles: %w{config}, id: '17', orientation: ''

# Parliament PID 
server '10.10.28.75', user: 'yarratrams', roles: %w{config}, id: '18', orientation: ''

# Parliament PID 
server '10.10.28.71', user: 'yarratrams', roles: %w{config}, id: '19', orientation: ''

# Parliament PID 
server '10.10.28.73', user: 'yarratrams', roles: %w{config}, id: '20', orientation: ''

# Parliament PID 
server '10.10.28.69', user: 'yarratrams', roles: %w{config}, id: '21', orientation: ''

# South Yarra PID
server '10.10.24.2', user: 'yarratrams', roles: %w{config}, id: '22', orientation: ''

# Southern Cross PID 23
server '10.10.28.133', user: 'yarratrams', roles: %w{config}, id: '23', orientation: ''

# Southern Cross PID 24
server '10.10.28.135', user: 'yarratrams', roles: %w{config}, id: '24', orientation: ''

# Southern Cross PID 25
server '10.10.28.137', user: 'yarratrams', roles: %w{config}, id: '25', orientation: 'left'

# Southern Cross PID 26
server '10.10.28.139', user: 'yarratrams', roles: %w{config}, id: '26', orientation: 'left'

# Balaclava PID
server '10.10.24.29', user: 'yarratrams', roles: %w{config}, id: '27', orientation: ''

# Camberwell PID
server '10.10.24.9', user: 'yarratrams', roles: %w{config}, id: '28', orientation: ''

# Clifton Hill PID
server '10.10.24.22', user: 'yarratrams', roles: %w{config}, id: '29', orientation: 'left'

# Clifton Hill PID
server '10.10.24.23', user: 'yarratrams', roles: %w{config}, id: '30', orientation: 'left'

# Elsternwick PID
server '10.10.24.30', user: 'yarratrams', roles: %w{config}, id: '31', orientation: 'left'

# Glenhuntly PID
server '10.10.24.7', user: 'yarratrams', roles: %w{config}, id: '32', orientation: 'left'

# Hawthorn PID
server '10.10.24.10', user: 'yarratrams', roles: %w{config}, id: '33', orientation: ''

# Jolimont  Station PID 34
server '10.10.29.135', user: 'yarratrams', roles: %w{config}, id: '34', orientation: ''

# Jolimont Station PID 35
server '10.10.29.133', user: 'yarratrams', roles: %w{config}, id: '35', orientation: ''

# Kooyong PID
server '10.10.24.27', user: 'yarratrams', roles: %w{config}, id: '36', orientation: ''

# Malvern PID
server '10.10.24.13', user: 'yarratrams', roles: %w{config}, id: '37', orientation: 'left'

# Newmarket PID
server '10.10.24.19', user: 'yarratrams', roles: %w{config}, id: '38', orientation: ''

# North Richmond PID 39
server '10.10.24.35', user: 'yarratrams', roles: %w{config}, id: '39', orientation: 'left'

# North Richmond PID 40
server '10.10.24.36', user: 'yarratrams', roles: %w{config}, id: '40', orientation: 'left'

# Northcote PID 41
server '10.10.24.24', user: 'yarratrams', roles: %w{config}, id: '41', orientation: ''

# Northcote PID 42
server '10.10.24.25', user: 'yarratrams', roles: %w{config}, id: '42', orientation: ''

# Richmond PID
server '10.10.30.5', user: 'yarratrams', roles: %w{config}, id: '43', orientation: 'left'

# Windsor PID 44
server '10.10.24.17', user: 'yarratrams', roles: %w{config}, id: '44', orientation: ''

# Windsor PID 45
server '10.10.24.16', user: 'yarratrams', roles: %w{config}, id: '45', orientation: ''

# Anstey PID
server '10.10.24.32', user: 'yarratrams', roles: %w{config}, id: '46', orientation: ''

# Armadale PID
server '10.10.24.14', user: 'yarratrams', roles: %w{config}, id: '47', orientation: 'left'

# Batman PID
server '10.10.24.33', user: 'yarratrams', roles: %w{config}, id: '48', orientation: ''

# Brunswick Station PID
server '10.10.24.6', user: 'yarratrams', roles: %w{config}, id: '49', orientation: ''

# Burnley PID 50
server '10.10.29.165', user: 'yarratrams', roles: %w{config}, id: '50', orientation: ''

# Burnley PID 51
server '10.10.29.167', user: 'yarratrams', roles: %w{config}, id: '51', orientation: ''

# Coburg PID
server '10.10.24.20', user: 'yarratrams', roles: %w{config}, id: '52', orientation: ''

# Croxton PID
server '10.10.24.26', user: 'yarratrams', roles: %w{config}, id: '53', orientation: ''

# East Richmond PID 54
server '10.10.24.12', user: 'yarratrams', roles: %w{config}, id: '54', orientation: ''

# East Richmond PID 55
server '10.10.24.11', user: 'yarratrams', roles: %w{config}, id: '55', orientation: ''

# Gardiner PID
server '10.10.24.38', user: 'yarratrams', roles: %w{config}, id: '56', orientation: ''

# Glen Iris PID
server '10.10.24.28', user: 'yarratrams', roles: %w{config}, id: '57', orientation: ''

#Jewell Station
server '10.10.24.3', user: 'yarratrams', roles: %w{config}, id: '58', orientation: ''

# Moreland PID
server '10.10.24.21', user: 'yarratrams', roles: %w{config}, id: '59', orientation: ''

# Prarhan PID
server '10.10.25.1', user: 'yarratrams', roles: %w{config}, id: '60', orientation: ''

# Ripponlea PID
server '10.10.24.18', user: 'yarratrams', roles: %w{config}, id: '61', orientation: 'left'

# Riversdale PID
server '10.10.24.8', user: 'yarratrams', roles: %w{config}, id: '62', orientation: ''

# Royal Park PID
server '10.10.24.34', user: 'yarratrams', roles: %w{config}, id: '63', orientation: 'left'

# Thornbury PID
server '10.10.24.39', user: 'yarratrams', roles: %w{config}, id: '64', orientation: ''

# Toorak PID
server '10.10.24.15', user: 'yarratrams', roles: %w{config}, id: '65', orientation: 'left'

# Westgarth PID
server '10.10.24.37', user: 'yarratrams', roles: %w{config}, id: '66', orientation: 'left'

# Flinders Street PID 68
server '10.10.28.5', user: 'yarratrams', roles: %w{config}, id: '68', orientation: 'left'



# Global options
# --------------
set :ssh_options, {
#   keys: %w(/home/rlisowski/.ssh/id_rsa),
  forward_agent: false,
  auth_methods: %w(password),
  password: 'Password1',
  user: 'yarratrams',
#  port: 8888
}
