﻿"use strict";

webPidsApp.controller("pidController", ["$scope", "$rootScope", "webPidsAppConfig", function ($scope, $rootScope, webPidsAppConfig) {
    if (!$scope.prediction || !$scope.index || !$scope.numoflines) {
        return;
    }

    initPages();

    $scope.stopname = $scope.prediction.StopName;
    $scope.citydirection = $scope.prediction.CityDirection;
    $scope.isinfreezone = $scope.prediction.IsInFreeZone;

    function getNumOfLines() {
        return $scope.numoflines > $scope.prediction.Predictions.length ? $scope.prediction.Predictions.length : $scope.numoflines;
    }

    function initPages() {
        // compute the pages
        $scope.numofpages = Math.ceil($scope.prediction.Predictions.length / getNumOfLines());
         
        $scope.currentpage = 1;
        $scope.pagedpredictions = $scope.prediction.Predictions.slice(0, getNumOfLines());
    }
    
}]);