﻿"use strict";

webPidsApp.controller("pidErrorController", ["$scope", "$rootScope", "$interval", "$timeout",  function ($scope, $rootScope, $interval, $timeout) {

    var canvas = document.getElementById("bodyCanvas");
    var context = canvas.getContext("2d");
    var footer = document.getElementById("footer");
    var contextfooter = footer.getContext("2d");
    var startProcessingError = false;


    var imageObj = new Image();
    var ttlogo = new Image();

    imageObj.onload = function() {
        drawImage();
    };


    ttlogo.onload = function() {
        drawLogo();
    };


    // resize the canvas to fill browser window dynamically
    window.addEventListener("resize", resizeCanvas, false);


    function resizeCanvas() {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight - 100;
        footer.width = window.innerWidth;
        footer.height = window.innerHeight - canvas.height;
        imageObj.src = "Images/exclamation.png";
        ttlogo.src = "Images/tramtracker.png";

        drawStuff();
    }

    function wrapText(context, text, x, y, maxWidth, lineHeight) {
        var words = text.split(' ');
        var line = '';

        for (var n = 0; n < words.length; n++) {
            var word = words[n];
            if (words[n] == '<nowrap>') {
                n++;
                word = '';
                while (n < words.length && words[n] != '</nowrap>') {
                    word += words[n] + ' ';
                    n++;
                }
                if (n < words.length) n++;
            }

            var testLine = line + word + ' ';
            var metrics = context.measureText(testLine);
            var testWidth = metrics.width;
            if (testWidth > maxWidth && n > 0) {
                context.fillText(line, x, y);
                line = word + ' ';
                y += lineHeight;
            }
            else {
                line = testLine;
            }
        }
        context.fillText(line, x, y);
    }


    function drawClock() {

        //Get Current Time
        var d = new Date();
        var str = prefixZero(d.getHours(), d.getMinutes(), d.getSeconds());
        var left = 40;
        var top = footer.height - 30;

        contextfooter.fillStyle = "black";
        contextfooter.fillRect(0, 0, 1000, 100);
        contextfooter.font = "60px Helvetica";
        contextfooter.fillStyle = "white";
        contextfooter.fillText(str, left, top);
    }

    function prefixZero(hour, min, sec) {
        var curTime;
        if (hour < 10)
            curTime = "0" + hour.toString();
        else
            curTime = hour.toString();

        if (min < 10)
            curTime += ":0" + min.toString();
        else
            curTime += ":" + min.toString();

        if (sec < 10)
            curTime += ":0" + sec.toString();
        else
            curTime += ":" + sec.toString();
        return curTime;
    }

    function drawImage() {
        context.drawImage(imageObj, 60, 60);
    }

    function drawLogo() {
        contextfooter.drawImage(ttlogo, footer.width - 300, 0);
    }

    function broadcastStartLoadPidDataTimerEvent() {
        $rootScope.$broadcast("loadPidsDataEventFinished");
    }

    function stopBroadcastStartLoadPidDataTimerEvent() {
        if ($scope.eventTimer != null) {
            $timeout.cancel($scope.eventTimer);
        }

        $scope.eventTimer = null;
    }

    function performProcessing() {
        drawClock();
        if (startProcessingError) {
            stopBroadcastStartLoadPidDataTimerEvent();
            $scope.eventTimer = $timeout(broadcastStartLoadPidDataTimerEvent, $scope.dataextractrate);
        }
    }

    $scope.$on("startProcessingErrorEvent", function (event, value) {
        startProcessingError = value;
        if(value)
            $rootScope.$broadcast("requestLoadPidsDataEvent");

    });


    // invoke the pid data extraction timer
    function startErrorTimer() {
        if ($scope.errorTimer == null) {
            $scope.errorTimer = $interval(performProcessing, 1000);
        }
    }

    // stop the data extraction timer
    function stopErrorTimer() {
        if ($scope.errorTimer != null) {
            $interval.cancel($scope.errorTimer);
        }
        $scope.errorTimer = null;
    }

    $scope.$on("$destroy", function (event) {
        try {
            stopBroadcastStartLoadPidDataTimerEvent();
            stopErrorTimer();
        } catch (e) {
            console.log(e.message);
        }
    });


    resizeCanvas(); 
    startErrorTimer();

    function drawStuff() {
        // do your drawing stuff here
        context.beginPath();
        context.fillStyle = "white";
        context.font = "bold 70px Arial";
        var maxWidth = canvas.width - 400;
        var lineHeight = 70;
        wrapText(context, "tramTRACKER is currently unavailable on this display.", 400, 100, maxWidth, lineHeight);
        context.font = "70px Arial";
        maxWidth = canvas.width - 60;
        wrapText(context, "For train, tram and bus information, call <nowrap> 1800 800 007 </nowrap>", 60, 500, maxWidth, lineHeight);

        context.fill();
        context.stroke();

        drawImage();

        // footer
        contextfooter.fillStyle = "#000000";
        contextfooter.fillRect(0, 0, footer.width, footer.height);
        drawLogo();
        drawClock();
    }
}]);