﻿"use strict";

webPidsApp.controller("pidcontainerController", ["$scope", "$rootScope", "$interval", "$timeout",  "webPidsService", "webPidsAppConfig", 
            function ($scope, $rootScope, $interval, $timeout, webPidsService, webPidsAppConfig) {

    var sync = true;
    var forceupdate = false;
    $scope.pidsData = null;

    var timestamp = Date.now();

   invokeFlushPidsData(false);

   // flush data from cache to binding variables
   function flushPidsData() {
        if ($scope.pidsData != null) {
            if (!$scope.pidsData.hasError) {
                // send data to client
                $scope.pidDescription = $scope.pidsData.description;
                $scope.orientationmode = $scope.pidsData.orientationMode;
                $scope.isgenericmessageenabled = $scope.pidsData.isGenericMessageEnabled;
                $scope.genericmessage = $scope.pidsData.genericMessage;
                if (forceupdate) {
                    $scope.predictions = $scope.pidsData.predictionsResults;
                    forceupdate = false;
                }
                $scope.numoflines = $scope.pidsData.numOfLines;
                if ($scope.predictions != null) {
                    $scope.readyForRefresh = new Array($scope.predictions[0].Predictions.length);
                    $scope.createdtime = $scope.predictions[0].CreatedTime;
                    $scope.clock = $scope.predictions[0].CreatedTime;
                    $scope.predictionslength = $scope.predictions.length;
                }

                $scope.messages = $scope.pidsData.messages;
                $rootScope.$broadcast("updateClockEvent", $scope.createdtime);
            }

            $scope.haserror = $scope.pidsData.hasError;
            $scope.errormessage = $scope.pidsData.errorMessage;

            $scope.$broadcast("startProcessingErrorEvent", $scope.pidsData.hasError);
        }
    }

    // get the data from web service into the cache
    function loadPidsData() {
        webPidsService.getPidsData()
            .then(function () {
                    // read and store it a safe place
                    $scope.pidsData = webPidsService.pidsData;

                    // if signalled we flush data from cache after successful completion of request
                    if (sync) {
                        sync = false; 
                        flushPidsData();
                    }

                    // record error
                    $scope.haserror = webPidsService.pidsData.hasError;
                    $scope.errormessage = webPidsService.pidsData.errorMessage;
                    $scope.$broadcast("startProcessingErrorEvent", $scope.pidsData.hasError);
                },
            function () {
                if (webPidsService.pidsData.hasError || webPidsService.hasError == undefined) {
                    $scope.pagedpredictions = [];
                    // Let's error report in case the server hasn't already done it
                    if (webPidsService.pidsData.errorMessage == undefined) {
                        webPidsService.pidsData.hasError = true;
                        webPidsService.pidsData.errorMessage = "Error with response from service";
                    }
                    else if (webPidsService.pidsData.errorMessage === "") {
                        webPidsService.pidsData.errorMessage = "Error extracting pid configuration for id " + webPidsAppConfig.pidid;
                    }

                    $scope.haserror = webPidsService.pidsData.hasError;
                    $scope.errormessage = webPidsService.pidsData.errorMessage;
                    
                    $scope.pidsData = null;

                    $scope.$broadcast("startProcessingErrorEvent", $scope.haserror);
                } 
            });

        timestamp = Date.now(); // only for debugging
    };

    // Async call to load data 
    function invokeFlushPidsData(flag) {
        stopFlushPidsDataTimer();

        forceupdate = true;
        if (flag === true) {
            $scope.flushPidsDataTimer = $timeout(flushPidsData, 1000);
        } else {
            $scope.flushPidsDataTimer = $timeout(loadPidsData, 1000);
        }
    }

    $scope.$on("requestLoadPidsDataEvent", function(event, data) {
        if ($scope.loadPidDataTimer != null && $scope.loadPidDataTimer != undefined)
            $interval.cancel($scope.loadPidDataTimer);
        $scope.loadPidDataTimer = $interval(loadPidsData, webPidsAppConfig.dataextractrate);
    });

    // stop the timer
    function stopFlushPidsDataTimer() {
        if ($scope.flushPidsDataTimer != null) {
            $timeout.cancel($scope.flushPidsDataTimer);
        }
        $scope.flushPidsDataTimer = null;
    }

    
    // signalled by caller for fresh data upload
    $scope.$on("loadPidsDataEventFinished", function (event, data) {
        // lets get new data from cache by resetting the objects
        forceupdate = true;
        invokeFlushPidsData(true);
        // get the ttid's to repaint themselves
        $rootScope.$broadcast("restartProcessingEvent");
    });
 

    // invoke the pid data extraction timer
    function startLoadPidDataTimer() {
        if ($scope.loadPidDataTimer == null) {
            sync = false;
            $scope.loadPidDataTimer = $interval(loadPidsData, webPidsAppConfig.dataextractrate);
        }
    }

    // signalled by caller to start data load 
    $scope.$on("startLoadPidDataTimerEvent", function (event, data) {
        startLoadPidDataTimer();
        $rootScope.$broadcast("restartProcessingEvent");
    });

    // stop the data extraction timer
    function stopLoadPidDataTimer() {
        if ($scope.loadPidDataTimer != null) {
            $interval.cancel($scope.loadPidDataTimer);
        }
        $scope.loadPidDataTimer = null;
    }

    // kill all open handles 
    $scope.$on("$destroy", function (event) {
        try {
            stopFlushPidsDataTimer();
            stopLoadPidDataTimer();
        } catch (e) {
            console.log(e.message);
        }
    });

}]);