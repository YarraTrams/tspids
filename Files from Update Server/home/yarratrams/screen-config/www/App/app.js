﻿var webPidsApp = angular.module("webPidsApp", []);

webPidsApp.constant("webPidsAppConfig", {
    hosturl: "http://localhost/WebPids.Web.Api/api/PidsData/",
    pidid: 23,
    pagetransitionrefreshrate: 6000,
    dataextractrate: 15000
    });
