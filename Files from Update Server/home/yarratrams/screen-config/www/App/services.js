﻿webPidsApp.factory("webPidsService", ["$http", "$q", "$interval", "webPidsAppConfig",
function ($http, $q, $interval, webPidsAppConfig) {
    var hosturl = webPidsAppConfig.hosturl;
    var pidsData = [];
    var failstarttime = null;

    // Get the Pids device information
    function getPidsData() {
        var getParameterByName = function(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? null : decodeURIComponent(results[1].replace(/\+/g, " "));
        };
        var pidId = getParameterByName("pidId") || webPidsAppConfig.pidid;

        var deferred = $q.defer();
        var controllerQuery = hosturl + "GetPidsData/?pidId=" + pidId;

        $http.get(controllerQuery)
            .then(function(result) {
                // Successful - send the payload to caller
                angular.copy(result.data, pidsData);
                failstarttime = null;
                deferred.resolve();
            },
            function(result) {
                if (failstarttime === null) {
                    failstarttime = Date.now(); // record the first failure
                } else if ((Date.now() - failstarttime) / 1000 >= 60) {
                    // show error screen after 60 seconds until service is restored
                    pidsData.hasError = true;
                    pidsData.errorMessage = "Error extracting pids data";
                    angular.copy(result.data, pidsData);
                    failstarttime = null; //reset
                }

                deferred.reject();
            });
        return deferred.promise;
    }
     

    //Expose methods and fields through revealing pattern
    return {
        pidsData: pidsData,
        getPidsData: getPidsData 
    }

}]); 
     