﻿webPidsApp.directive("pidbody", function () {
    return {
        restrict: "E",
        templateUrl: "App/PartialViews/pidbody.html",
        controller: "pidBodyController",
        scope: {
            haserror: "=",
            errormessage: "=",
            name: "=",
            description: "=",
            stopname: "=",
            citydirection: "=",
            isinfreezone: "=",
            prediction: "=",
            orientationmode: "=",
            pagedpredictions: "=",
            numofpages: "=",
            currentpage: "=",
            isready: "=",
            numoflines: "=",
            index: "=",
            predictionscount: "="
        }
    };
});