﻿webPidsApp.directive("piderror", function () {
    return {
        restrict: "E",
        templateUrl: "App/PartialViews/piderror.html",
        controller: "pidErrorController",
        scope: {
            errormessage: "="
        }
    };
});