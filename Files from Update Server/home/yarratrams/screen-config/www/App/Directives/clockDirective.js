﻿webPidsApp.directive("clock", function () {
    return {
        restrict: "E",
        replace: true,
        templateUrl: "App/PartialViews/clock.html",
        controller: "clockController",
        scope: {
        }
    };
});