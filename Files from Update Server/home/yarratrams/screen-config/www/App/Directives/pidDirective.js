﻿webPidsApp.directive("pid", function () {
    return {
        restrict: "E",
        templateUrl: "App/PartialViews/pid.html",
        controller: "pidController",
        scope: {
            haserror: "=",
            errormessage: "=",
            stopname: "=",
            citydirection: "=",
            isinfreezone: "=",
            prediction: "=",
            pagedpredictions: "=",
            orientationmode: "=",
            numofpages: "=",
            currentpage: "=",
            isready: "=",
            numoflines: "=",
            index: "@",
            predictionscount: "="
        }
    };
});