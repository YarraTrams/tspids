#!/bin/bash

# Load common config
. /opt/yarratrams/current/kiosk-config

# Cycle though all possible X displays
for x in `ls /tmp/.X11-unix/X*`
do
  y=$(basename $x)
  X_DISPLAY=$(echo ":${y#X}")
  export DISPLAY=$X_DISPLAY
  
  # Get window ID
  CHROME_ID=$(xdotool search --onlyvisible --name $EXEC_NAME)

  # Focus window and send the F5 keystroke
  xdotool windowactivate $CHROME_ID
  xdotool key --clearmodifiers F5
done
