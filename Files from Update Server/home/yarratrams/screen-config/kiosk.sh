#!/bin/bash

# Load common config
. /opt/yarratrams/current/kiosk-config

sleep 2

xset -dpms
xset s off
openbox-session &

sleep 2

# Create inital network-down file
touch $NETWORK_DOWN_FILE

while true
do
  # Clear browser cache 
  rm -rf ~/.{config,cache}/google-chrome/

  # Determine page to load (online/offline)
  if [ -e $NETWORK_DOWN_FILE ]
  then
    URL=$URL_OFFLINE
  else
    URL=$URL_ONLINE
  fi

  # Start browser with appropriate URL
  google-chrome --kiosk --no-first-run -allow-file-access-from-files --allow-file-access --allow-cross-origin-auth-prompt $URL
done
