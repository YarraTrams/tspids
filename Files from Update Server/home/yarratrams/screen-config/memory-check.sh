#!/bin/bash

# Load common config
. /opt/yarratrams/current/kiosk-config

while true; do
  sleep $MEM_CHECK_INTERVAL

  # Get current available memory
  MEM_FREE=$(awk '$1=="MemAvailable:"{print $2}' /proc/meminfo)

  if [ $MEM_FREE -le $MEM_CHECK_THRESHOLD ]
  then
    sudo reboot
  fi

done
