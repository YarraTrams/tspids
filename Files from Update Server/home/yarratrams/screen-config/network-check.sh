#!/bin/bash

# Load common config
. /opt/yarratrams/current/kiosk-config

# Initial delay to match X server startup
sleep 4

# Inital state is down
MISSES=3
STATE_CHANGE=0
while true
do
  sleep $PING_INTERVAL

  if ping -c 1 -w $PING_WAIT $PING_TARGET &>/dev/null
  then
    if [ $MISSES -ge $PING_MISSES ]
    then
      STATE_CHANGE=1
    fi
    MISSES=0
  else
    MISSES=$(expr $MISSES + 1)
    if [ $MISSES -eq $PING_MISSES ]
    then
      STATE_CHANGE=1
    fi
  fi

  if [ $MISSES -ge $PING_MISSES ]
  then
    touch $NETWORK_DOWN_FILE
  else
    rm -f $NETWORK_DOWN_FILE
  fi

  if [ $STATE_CHANGE -eq 1 ]
  then
    killall -9 $EXEC_NAME
    STATE_CHANGE=0
  fi
done
