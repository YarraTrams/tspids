#!/bin/bash

LOG_FILE=/home/yarratrams/deploy.log
HOST_FILE=/home/yarratrams/screen-config/config/deploy/production.rb
CONFIG_DIR=/home/yarratrams/screen-config

if [ $# -lt 1 ]
then
  echo -en "Usage: $0 [all | host [host [ ... ]]]\n\n"
  echo -en "\tall - process all hosts\n"
  echo -en "\thost list - process one or more hosts\n\n"
  exit
fi

if [ $1 = "all" ]
then
  echo -en "Loading host list... "
  IP_LIST=$(awk '$1=="server"{print $2}' $HOST_FILE | tr -cd '0-9.\n')
  echo -en "done\n"
else
  IP_LIST=$@
fi

cd $CONFIG_DIR

NUM_SUCCESSES=0
NUM_FAILS=0
FAILED_LIST=""
echo -en "Processing host list...\n"
for ip in $IP_LIST
do
  echo -en " + $ip\t: "
  export HOSTS="^$ip\$"
  export DATE=$(date +"%F %T %Z")
  cap production deploy:withreboot 2>&1 | sed "s/^/$DATE  /" >> $LOG_FILE
  EXIT_STATUS=${PIPESTATUS[0]}
  if [ $EXIT_STATUS -eq 0 ]
  then
    echo -en "SUCCESS\n"
    NUM_SUCCESSES=$(($NUM_SUCCESSES+1)) 
  else
    echo -en "FAILED\n"
    NUM_FAILS=$(($NUM_FAILS+1))
    FAILED_LIST="$FAILED_LIST $ip"
  fi
done

echo -en "\n$NUM_SUCCESSES updates succeeded.\n"
if [ $NUM_FAILS -ne 0 ]
then
  echo -en "$NUM_FAILS updates FAILED.\n"
fi
echo -en "\n"

if [ ! -z "$FAILED_LIST" ]
then
  echo -en "Failed to update:\n"
  for ip in $FAILED_LIST
  do
    echo -en " + $FAILED_LIST\n"
  done
  echo -en "\n"
fi

