#!/bin/bash

if [ $# -lt 1 ]
then
  echo -en "Usage: installscrot1 [host ip]\n\n"
  exit
fi

echo -en "\nCopying executable for SCROT to $1 ...\n\n"

scp installscrot2.sh screenshot2.sh $1:/home/yarratrams

cd /home/yarratrams/Scrot

scp libgif4_4.1.6-11_amd64.deb libid3tag0_0.15.1b-10ubuntu1_amd64.deb libimlib2_1.4.6-2_amd64.deb giblib1_1.2.4-9_amd64.deb scrot_0.8-13_amd64.deb $1:/home/yarratrams

ssh $1
