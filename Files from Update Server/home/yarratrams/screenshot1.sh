if [ $# -lt 1 ]
then
  echo -en "Usage: screenshot1 [host ip]\n\n"
  exit
fi

echo -en "\nCapturing screen on $1 ...\n\n"

ssh $1
