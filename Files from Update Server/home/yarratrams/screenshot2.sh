if [ $# -lt 1 ]
then
  echo -en "Usage: screenshot2 [image name]\n\n"
  exit
fi

filename=$1$(date +"_%Y%d%m%H%M.png")

echo -en "\nCapturing screen to $filename ...\n\n"

destination=10.0.205.22:/home/yarratrams/screenshots

export DISPLAY=:0

scrot $filename

scp $filename $destination

sudo mv $filename /home/screenshots

echo -en "\nScreen has been saved to $destination/$filename\n\n"
