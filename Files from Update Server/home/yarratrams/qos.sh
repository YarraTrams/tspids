#!/bin/sh

tc qdisc del dev eth0 root handle 1: htb default 10

tc qdisc add dev eth0 root handle 1: htb default 10

tc class add dev eth0 parent 1: classid 1:1 htb rate 240kbit

tc class add dev eth0 parent 1:1 classid 1:10 htb prio 0 rate 240kbit
tc qdisc add dev eth0 parent 1:10 handle 10: sfq perturb 10

