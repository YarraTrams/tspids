﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using WebPids.BusinessLogic.Models;
using WebPids.EntityFramework;
using WebPids.EntityFramework.DapperModels;
using WebPids.EntityFramework.DAL;

namespace WebPids.BusinessLogic
{
    public class TramTrackerService
    {
        /// <summary>
        /// Generate a list of predictions for a given list of tracker ids (stops)
        /// Each tracker Id will generate a PredictionResult object, which has 0-3 predictions in total
        /// after filtering
        /// </summary>
        /// <param name="stops">The stop / tracker id</param>
        /// <returns>A set of results, one for each stop, with the predictions for each result contained
        /// within the result object</returns>
        /// <exception cref="TramTrackerServiceException">List of stops cannot be null or empty</exception>
        public IEnumerable<PredictionsResult> GetPredictionsForStops(Configuration config, IEnumerable<Stop> stops)
        {
            if (stops == null || !stops.Any())
                throw new TramTrackerServiceException("List of stops cannot be null or empty");

            // Create instance of the API that talks to the tram tracker database
            TramTrackerAPI api = new TramTrackerAPI();

            // Conver the list of predictions into a set of results with predictions contained within
            List<PredictionsResult> results = new List<PredictionsResult>();
            
            // Iterate over the stops, and create a result for each stop
            foreach (Stop stop in stops)
            {
                // Fill the stop information
                WebPids.EntityFramework.DapperModels.StopInformation stopInfo = api.GetStopInformation(stop.TrackerId);
                PredictionsResult result = new PredictionsResult
                {
                    StopNumber = stopInfo.FlagStopNo.ToString(),
                    CityDirection = stopInfo.CityDirection,
                    HasError = false,
                    TimeRequested = DateTime.Now,
                    TimeResponded = DateTime.Now,
                    TrackerId = stop.TrackerId,
                    StopName = stopInfo.StopName,
                    DisplayTitle = !string.IsNullOrEmpty(stop.DisplayTitle) ? stop.DisplayTitle : config.PIDName,
                    Direction = stop.Direction != null ? ServiceProvider.CreateInstance.GetService<DirectionsServices>().GetAll().First(n => n.Id == stop.Direction).ImageName : config.Direction.ImageName,
                    Distance = stop.Distance ?? config.Distance,
                    Predictions = new List<Prediction>()
                };
                results.Add(result);

                // Get predictions for stop, and iterate over them for adding to business object after massaging
                IEnumerable<StopPrediction> rawPredictions = api.GetPredictionsForStop(stop);

                // Group the routes, to ensure we retrieve only the appropriate number of predictions for each route
                IEnumerable<IGrouping<int, StopPrediction>> groupedRoutes = rawPredictions.GroupBy(n => n.RouteNo).OrderBy(r => r.Key);//.Select( (key, item) => new {item.});

                // If 1, 2 routes allow 3 predictions each
                // If 3+ routes allow 1 prediction each
                int predictionsPerRoute = new []{1,2}.Contains(groupedRoutes.Count()) ? 3 : 1;

                foreach (IGrouping<int, StopPrediction> route in groupedRoutes)
                {
                    List<StopPrediction> stopPredictionsForRoute = route.OrderBy(r => r.Schedule).Take(predictionsPerRoute).ToList();

                    List<Prediction> routePredictions = new List<Prediction>();
                    foreach (StopPrediction pred in stopPredictionsForRoute)
                    {
                        Prediction p = new Prediction
                        {
                            Destination = pred.Destination.Length > 36 ? string.Format("{0} ...", pred.Destination.Substring(0, 36)) : pred.Destination,
                            DisplayAc = pred.DisplayAirCondition,
                            DisruptionMessage = pred.TTDMSMessage,
                            HasDisruption = !string.IsNullOrEmpty(pred.TTDMSMessage),
                            HasSpecialEvent = !string.IsNullOrEmpty(pred.SpecialEventMessage),
                            HeadBoardRouteNo = Convert.ToString(pred.HeadboardRouteNo),
                            RouteNumber = pred.RouteNo,
                            IsLowFloorTram = pred.LowFloor,
                            TramClassUrl = GetTramClassUrl(Convert.ToInt32(pred.VehicleNo)),
                            Display = pred.DisplayPrediction,
                            SpecialEventMessage = pred.SpecialEventMessage,
                            PredictedArrivalDateTime =
                                ((DateTime)pred.Schedule).AddSeconds(Convert.ToDouble(pred.Adjustment))
                        };
                        
                        routePredictions.Add(p);
                    }
                    result.Predictions.AddRange(routePredictions.OrderBy(x => x.PredictedArrivalDateTime));
                }
            }

            return results;
        }

        /// <summary>
        /// Get the tram class image url
        /// </summary>
        /// <param name="tramNo">the tram number</param>
        /// <returns>the well formed URL</returns>
        private static string GetTramClassUrl(int tramNo)
        {
            const string baseIconUrl = "Images/Trams/";

            // zeros are unknown
            if (tramNo <= 0)
                return string.Empty;

            // Z1 class trams
            if (tramNo >= 1 && tramNo <= 100)
                return baseIconUrl + "tram-z1.png";

            // Z2 class trams
            if (tramNo <= 115)
                return baseIconUrl + "tram-z3.png";

            // Z3 class trams
            if (tramNo <= 230)
                return baseIconUrl + "tram-z3.png";

            // A1 class trams
            if (tramNo <= 258)
                return baseIconUrl + "tram-a.png";

            // A2 class trams
            if (tramNo <= 300)
                return baseIconUrl + "tram-a.png";

            // W Class trams
            if (tramNo >= 681 && tramNo <= 1040)
                return baseIconUrl + "tram-w.png";

            // B1 class trams
            if (tramNo == 2001 || tramNo == 2002)
                return baseIconUrl + "tram-b.png";

            // B2 Class Trams
            if (tramNo >= 2003 && tramNo <= 2132)
                return baseIconUrl + "tram-b.png";

            // C Class Trams
            if (tramNo >= 3001 && tramNo <= 3036)
                return baseIconUrl + "tram-c.png";

            // D1 Class trams
            if (tramNo >= 3501 && tramNo <= 3600)
                return baseIconUrl + "tram-d1.png";

            // D2 Class trams
            if (tramNo >= 5001 && tramNo <= 5100)
                return baseIconUrl + "tram-d2.png";

            // Bumblebee trams
            if (tramNo >= 5101 && tramNo <= 5200)
                return baseIconUrl + "tram-c2.png";

            //E Class trams
            if (tramNo >= 6001 && tramNo <= 6050)
                return baseIconUrl + "tram-e.png";

            // bleh, return the default
            return string.Empty;
        }

    }
}
