﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using WebPids.EntityFramework;

namespace WebPids.BusinessLogic
{
    public class PidsElevatedLogServices : Services
    {
        [Flags]
        public enum ErrorLevel
        {   
            Error,
            Warning,
            Information
        }

        private readonly IRepository<PidsElevatedLog> repository;

        public PidsElevatedLogServices(IUnitOfWork unit)
            : base(unit)
        {
            repository = unitOfWork.GetRepository<PidsElevatedLog>();
        }

        /// <summary>
        /// Get all the pid errors
        /// </summary>
        /// <returns>the list of pid errors</returns>
        public IEnumerable<PidsElevatedLog> GetAll()
        {
            return repository.GetAll().OrderBy(a => a.Id).ToList();
        }

        /// <summary>
        /// Get all the pid errors
        /// </summary>
        /// <returns>The list of pid errors</returns>
        public IEnumerable<PidsElevatedLog> GetByPidId(long pidId)
        {
            return repository.Get(x => x.ConfigurationId == pidId, x => x.OrderByDescending(y => y.Id));
        }


        /// <summary>
        /// Returns pid errors filtered and ordered.
        /// </summary>
        /// <param name="filter">Filter Expression</param>
        /// <param name="orderBy">Order Expression</param>
        /// <param name="includeProperties">Included Properties</param>
        /// <returns>The list of pid errors</returns>
        public IEnumerable<PidsElevatedLog> Get(Expression<Func<PidsElevatedLog, bool>> filter = null,
                        Func<IQueryable<PidsElevatedLog>, IOrderedQueryable<PidsElevatedLog>> orderBy = null,
                        string includeProperties = "")
        {
            return repository.Get(filter, orderBy, includeProperties);
        }


        /// <summary>
        /// Add a new record
        /// </summary>
        /// <param name="configurationId"></param>
        /// <param name="level"></param>
        /// <param name="message"></param>
        public void Add(long configurationId, string level, string message, int? downTime = null)
        {
            // Write the elevated log to the database
            repository.BeginTransaction();
            PidsElevatedLog data = new PidsElevatedLog()
            {
                ConfigurationId = configurationId,
                Date = DateTime.Now,
                Level = level,
                Message = message,
                DownTime = downTime
            };

            repository.Add(data);
            repository.SaveAllContextChanges();
            repository.CommitTransaction();
        }
    }
}
