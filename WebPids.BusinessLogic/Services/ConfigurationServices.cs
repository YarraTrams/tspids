﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using WebPids.BusinessLogic.Models;
using WebPids.Common;
using WebPids.EntityFramework;

namespace WebPids.BusinessLogic
{
    public class ConfigurationServices : Services
    {
        private readonly IRepository<Configuration> repository;
        private readonly IRepository<Stop> stopRepository;
        private readonly IRepository<ConfigurationsHistory> configurationsHistoryRepository;
        private readonly IRepository<GlobalSetting> globalSettingRepository;
        private readonly IRepository<Direction> directionRepository;
        private readonly IRepository<DisplayOrder> displayOrderRepository;

        /// <summary>
        /// Min range
        /// </summary>
        private static readonly TimeSpan MinRange = new TimeSpan(0, 30, 0);
        /// <summary>
        /// Max range
        /// </summary>
        private static readonly TimeSpan MaxRange = new TimeSpan(0, 60, 0);

        public ConfigurationServices(IUnitOfWork unit)
            : base(unit)
        {
            repository = unitOfWork.GetRepository<Configuration>();
            repository.DefaultIncludes = "Stops,Direction,DisplayOrder";
            repository.UseDefaultIncludes = true;
            stopRepository = unitOfWork.GetRepository<Stop>();
            configurationsHistoryRepository = unitOfWork.GetRepository<ConfigurationsHistory>();
            globalSettingRepository = unitOfWork.GetRepository<GlobalSetting>();
            directionRepository = unitOfWork.GetRepository<Direction>();
            displayOrderRepository = unitOfWork.GetRepository<DisplayOrder>();
        }

        /// <summary>
        /// Get the specific record
        /// </summary>
        /// <param name="id">the id</param>
        /// <returns>the object</returns>
        public Configuration GetConfiguration(long id)
        {
            return repository.GetById(id);
        }

        /// <summary>
        /// Get the generic message enabled flag
        /// </summary>
        /// <returns></returns>
        private bool IsGenericMessageEnabled
        {
            get
            {
                GlobalSetting value = globalSettingRepository.GetAll().FirstOrDefault(a => a.Name.Equals("GenericMessageEnabled"));
                bool result = false;
                if (value != null)
                {
                    bool.TryParse(value.Value, out result);
                }

                return result;
            }
        }

        /// <summary>
        /// Get the generic message
        /// </summary>
        /// <returns></returns>
        private string GenericMessage
        {
            get
            {
                GlobalSetting value = globalSettingRepository.GetAll().FirstOrDefault(a => a.Name.Equals("GenericMessage"));
                return value != null ? value.Value : string.Empty;
            }
        }

        /// <summary>
        /// Get the Pids Data
        /// </summary>
        /// <param name="pidId">the id</param>
        /// <returns>the pids data object</returns>
        public virtual WebPidsData GetPidData(long pidId)
        {
            // Get the configuration info
            Configuration configuration = this.GetConfiguration(pidId);
            if (configuration == null)
                return null;

            // Get the stop ids
            ICollection<Stop> stops = configuration.Stops;

            // Get the data 
            //var oaConsumer = new OpenAccessConsumer();
            TramTrackerService tramTrackerConsumer = new TramTrackerService();
            WebPidsData result = new WebPidsData
            {
                PidsConfiguration = configuration,
                IsGenericMessageEnabled = IsGenericMessageEnabled,
                GenericMessage = GenericMessage,
                PredictionsResults = tramTrackerConsumer.GetPredictionsForStops(configuration, stops)
            };

            // Update the health status of the pid
            UpdateHealthStatus(configuration);

            // Post result
            return result;
        }

        /// <summary>
        /// Get all the pids
        /// </summary>
        /// <returns>the list of pids</returns>
        public IEnumerable<Configuration> GetAll()
        {
            return repository.GetAll();//.OrderBy(a => a.Id);
        }

        /// <summary>
        /// Get all PIDs with their health status  
        /// </summary>
        /// <returns>All PIDs with their health status</returns>
        public IEnumerable<Configuration> GetAllWithHealth()
        {
            var result = repository.GetAll();
            EvaluateHealthStatus(result);
            return result;
        }

        /// <summary>
        /// Get the Pids list based on their health status  
        /// </summary>
        /// <returns>problem pids will be first in line</returns>
        public IEnumerable<Configuration> GetAllByHealthStatus()
        {
            IOrderedQueryable<Configuration> result = repository.GetAll()
                .Where(a => a.MonitorHeartBeat)
                .OrderBy(a => a.Id);

            EvaluateHealthStatus(result);
            return result;
        }

        /// <summary>
        /// Returns Pids filtered and ordered.
        /// </summary>
        /// <param name="filter">Filter Expression</param>
        /// <param name="orderBy">Order Expression</param>
        /// <param name="includeProperties">Included Properties</param>
        /// <returns>The list of pid errors</returns>
        public IEnumerable<Configuration> Get(Expression<Func<Configuration, bool>> filter = null,
                        Func<IQueryable<Configuration>, IOrderedQueryable<Configuration>> orderBy = null,
                        string includeProperties = "")
        {
            return repository.Get(filter, orderBy, includeProperties);
        }


        /// <summary>
        /// Add a new record
        /// </summary>
        /// <param name="data">the config record</param>
        public void Add(Configuration data)
        {
            data.HeartbeatLastUpdatedTime = DateTime.Now;
            data.MonitorHeartBeat = true;
            repository.Add(data);
            repository.SaveAllContextChanges();

            CreateHistoryRecordForNew(data);
        }

        /// <summary>
        /// Update an existing record
        /// </summary>
        /// <param name="data">the object</param>
        public void Update(Configuration data)
        {
            Configuration org = repository.GetById(data.Id);
            org.PIDName = data.PIDName;
            org.Description = data.Description;
            org.NetworkName = data.NetworkName;
            org.OrientationMode = data.OrientationMode;
            org.Enabled = data.Enabled;
            org.MonitorHeartBeat = data.MonitorHeartBeat;
            if (data.HeartbeatLastUpdatedTime != DateTime.MinValue && data.HeartbeatLastUpdatedTime != DateTime.MaxValue)
            {
                org.HeartbeatLastUpdatedTime = data.HeartbeatLastUpdatedTime;
            }
            org.MonitorPageProcessing = data.MonitorPageProcessing;
            org.PageProcessingTimeTaken = data.PageProcessingTimeTaken;
            org.DirectionsId = data.DirectionsId;
            org.Distance = data.Distance;
            org.DisplayOrderId = data.DisplayOrderId;
            org.StationName = data.StationName;
            org.PIDAssetLocationNo = data.PIDAssetLocationNo;
            org.VicTrackId = data.VicTrackId;


            CreateHistoryRecordForUpdate(org);
            repository.SaveAllContextChanges();
        }

        /// <summary>
        /// Remove the configuration record 
        /// </summary>
        /// <param name="data">the object to remove</param>
        public void Delete(Configuration data)
        {
            Configuration obj = GetConfiguration(data.Id);
            if (obj.Stops.Any())
            {
                // remove the dependencies 
                foreach (Stop stop in obj.Stops.ToList())
                {
                    stopRepository.Delete(stop);
                }
            }

            repository.Delete(obj);
            CreateHistoryRecordForDelete(obj);
            repository.SaveAllContextChanges();
        }

        /// <summary>
        /// Update the PageProcessingTimeTaken for a pid
        /// </summary>
        /// <param name="data">the config to update</param>
        /// <param name="timeTaken"></param>
        public void UpdatePageProcessingTimeTaken(Configuration data, TimeSpan timeTaken)
        {
            Configuration obj = GetConfiguration(data.Id);
            obj.PageProcessingTimeTaken = Convert.ToInt64(timeTaken.TotalMilliseconds);
            obj.ErrorStatus = 0;    // reset the error flag
            repository.SaveAllContextChanges();
        }

        /// <summary>
        /// Get the Pids that have taken longer than the pageProcessingThreshold to process the pages
        /// </summary>
        /// <param name="pageProcessingThreshold">the value from caller</param>
        /// <returns>list of slow pids</returns>
        public IEnumerable<Configuration> GetUnprocessedPids(TimeSpan pageProcessingThreshold)
        {
            return GetAll().Where(a => a.MonitorPageProcessing && pageProcessingThreshold.TotalMilliseconds < a.PageProcessingTimeTaken);
        }

        /// <summary>
        /// Update the Health status for a pid
        /// </summary>
        /// <param name="data">the config to update</param>
        public void UpdateHealthStatus(Configuration data)
        {
            data.HeartbeatLastUpdatedTime = DateTime.Now;
            repository.SaveAllContextChanges();
        }

        /// <summary>
        /// Any pids that havent requested data from us can be deemed as unhealthy
        /// </summary>
        /// <param name="heartBeatThreshold">the threshold</param>
        /// <returns>list of unhealthy pids</returns>
        public IEnumerable<Configuration> GetUnhealthyPids(TimeSpan heartBeatThreshold)
        {
            DateTime now = DateTime.Now;
            return GetAll().Where(a => a.MonitorHeartBeat && now.Subtract(a.HeartbeatLastUpdatedTime).TotalMilliseconds > heartBeatThreshold.TotalMilliseconds).ToList();
        }

        /// <summary>
        /// Any pids that havent requested data for hours from us can be deemed as unhealthy
        /// </summary>
        /// <param name="heartBeatThresholdinHours">the threshold</param>
        /// <returns>list of unhealthy pids</returns>
        public IEnumerable<Configuration> GetUnhealthyPidsDownForXHours(TimeSpan heartBeatThresholdinHours)
        {
            DateTime now = DateTime.Now;
            return GetAll().Where(a => a.MonitorHeartBeat && now.Subtract(a.HeartbeatLastUpdatedTime).TotalHours > heartBeatThresholdinHours.Hours).ToList();
        }

        /// <summary>
        /// Set the error state
        /// </summary>
        /// <param name="configuration">the configuration</param>
        public void SetErrorStatus(Configuration configuration, int errorStatus)
        {
            configuration.ErrorStatus = errorStatus;
            repository.SaveAllContextChanges();
        }

        /// <summary>
        /// Add a history record for create process
        /// </summary>
        /// <param name="data">The configuration objec</param>
        private void CreateHistoryRecordForNew(Configuration data)
        {
            string msg = string.Format("Pid {0} - {1} created by {2}", data.Id, data.PIDName, WebPidsConsole.CurrentUserName);
            CreateHistoryRecord(data.Id, msg);
            configurationsHistoryRepository.SaveAllContextChanges();
        }

        /// <summary>
        /// Create a history record for updates
        /// </summary>
        /// <param name="data">the object</param>
        private void CreateHistoryRecordForUpdate(Configuration data)
        {
            string msg = string.Format("Pid {0} - {1} updated by {2}", data.Id, data.PIDName, WebPidsConsole.CurrentUserName);
            CreateHistoryRecord(data.Id, msg);
        }

        /// <summary>
        /// Create a history record for delete
        /// </summary>
        /// <param name="data">the object</param>
        private void CreateHistoryRecordForDelete(Configuration data)
        {
            string msg = string.Format("Pid {0} - {1} deleted by {2}", data.Id, data.PIDName, WebPidsConsole.CurrentUserName);
            CreateHistoryRecord(data.Id, msg);
        }

        /// <summary>
        /// Create a history record
        /// </summary>
        /// <param name="id">the id to use</param>
        /// <param name="msg">the msg</param>
        private void CreateHistoryRecord(long id, string msg)
        {
            ConfigurationsHistory history = new ConfigurationsHistory()
            {
                ConfigurationsId = id,
                Message = msg,
                UserName = Environment.UserName,
                CreatedDate = DateTime.Now
            };

            configurationsHistoryRepository.Add(history);
        }

        /// <summary>
        /// Parse each item and evaluate the health statuses
        /// </summary>
        /// <param name="items">The array</param>
        private void EvaluateHealthStatus(IEnumerable<Configuration> items)
        {
            DateTime now = DateTime.Now;
            foreach (Configuration item in items)
            {
                if (item.Enabled)
                {
                    if (item.ErrorStatus == 0)
                        item.HealthStatus = HealthStatus.Healthy;
                    else if (item.ErrorStatus == 1)
                        item.HealthStatus = HealthStatus.Warning;
                    else
                        item.HealthStatus = HealthStatus.Faulty;
                }
                else
                {
                    item.HealthStatus = HealthStatus.Disabled;
                }
            }
        }
    }
}
