﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebPids.EntityFramework;
using System.Configuration;

namespace WebPids.BusinessLogic
{
    public class UserGroupServices : Services
    {
         private readonly IRepository<UserGroup> repository;

         public UserGroupServices(IUnitOfWork unit)
            : base(unit)
        {
            repository = unitOfWork.GetRepository<UserGroup>();
        }

        /// <summary>
        /// Get the specific record
        /// </summary>
        /// <param name="id">the id</param>
        /// <returns>the object</returns>
         public UserGroup GetUserGroupById(long id)
        {
            return repository.GetById(id);
        }

        /// <summary>
        /// Get all the records
        /// </summary>
        /// <returns>the list of user groups</returns>
        public IEnumerable<UserGroup> GetAll()
        {
            return repository.GetAll();//.OrderBy(a => a.Id);
        }

        /// <summary>
        /// Get the logged in user group
        /// </summary>
        /// <param name="username">the user name</param>
        /// <returns>if the user is valid</returns>
        public UserGroup GetLoggedinUserGroup(string username)
        {
            IEnumerable<GroupPrincipal> adgroups = GetADGroups(username);
            IEnumerable<UserGroup> groups = GetAll();
            UserGroup obj = adgroups.Select(grp => groups.FirstOrDefault(a => a.Name == grp.Name)).FirstOrDefault(found => found != null);
            return obj;
        }

        /// <summary>
        /// Get the AD group for user
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        private IEnumerable<GroupPrincipal> GetADGroups(string userName)
        {
            List<GroupPrincipal> result = new List<GroupPrincipal>();

            if (ConfigurationManager.AppSettings["UseDomain"] == "no")
            {
                return result;
            }

            // establish domain context
            PrincipalContext domain = new PrincipalContext(ContextType.Domain, userName.GetDomain());

            // find your user
            UserPrincipal user = UserPrincipal.FindByIdentity(domain, userName.GetLogin());

            if (user == null)
                return result.AsEnumerable();

            // if found - grab its groups
            PrincipalSearchResult<Principal> groups = user.GetGroups();
            //var groups = user.GetAuthorizationGroups();

            // iterate over all groups
            result.AddRange(groups.OfType<GroupPrincipal>());

            return result.AsEnumerable();
        }
    }
}
