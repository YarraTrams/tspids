﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebPids.EntityFramework;

namespace WebPids.BusinessLogic
{
    public class ConfigurationsHistoryServices : Services
    {
        private readonly IRepository<ConfigurationsHistory> repository;
        private readonly IRepository<Configuration> configurationRepository;

        public ConfigurationsHistoryServices(IUnitOfWork unit)
            : base(unit)
        {
            repository = unitOfWork.GetRepository<ConfigurationsHistory>();
            //repository.DefaultIncludes = "Configuration";
            //repository.UseDefaultIncludes = true;
            configurationRepository = unitOfWork.GetRepository<Configuration>();
        }

        /// <summary>
        /// Get all items
        /// </summary>
        /// <returns>list of items</returns>
        public IEnumerable<ConfigurationsHistory> GetAll()
        {
            IOrderedEnumerable<ConfigurationsHistory> result = repository.Get().OrderByDescending(a => a.CreatedDate);
            foreach (ConfigurationsHistory item in result.Where(a => a.ConfigurationsId > 0))
            {
                item.Configuration = configurationRepository.GetById(item.ConfigurationsId);
            }

            return result;
        }

        /// <summary>
        /// Add a history entry
        /// </summary>
        /// <param name="data">the data to add</param>
        public void Add(ConfigurationsHistory data)
        {
            repository.Add(data);
            repository.SaveAllContextChanges();
        }
    }
}
