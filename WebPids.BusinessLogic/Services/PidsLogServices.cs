﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebPids.EntityFramework;

namespace WebPids.BusinessLogic 
{
    public class PidsLogServices : Services
    {
        private readonly IRepository<PidsLog> repository;

        public PidsLogServices(IUnitOfWork unit)
            : base(unit)
        {
            repository = unitOfWork.GetRepository<PidsLog>();
        }

        /// <summary>
        /// Get all the pids
        /// </summary>
        /// <returns>the list of pids</returns>
        public IEnumerable<PidsLog> GetAll()
        {
            return repository.GetAll().OrderBy(a => a.Id);
        }

        /// <summary>
        /// Add a new record
        /// </summary>
        /// <param name="configurationId"></param>
        /// <param name="message"></param>
        public void Add(short configurationId, string message)
        {
            repository.BeginTransaction();
            PidsLog data = new PidsLog()
            {
                ConfigurationId = configurationId,
                Date = DateTime.Now,
                Level = "INFORMATION",
                Message = message
            };

            repository.Add(data);
            repository.SaveAllContextChanges();
            repository.CommitTransaction();
        }
    }
}
