﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebPids.EntityFramework;

namespace WebPids.BusinessLogic
{
    public class DisplayOrderServices : Services
    {
         private readonly IRepository<DisplayOrder> repository;

         public DisplayOrderServices(IUnitOfWork unit)
             : base(unit)
        {
            repository = unitOfWork.GetRepository<DisplayOrder>();
            repository.DefaultIncludes = "Configurations";
            repository.UseDefaultIncludes = true;
        }

        /// <summary>
        /// Get the specific record
        /// </summary>
        /// <param name="id">the id</param>
        /// <returns>the object</returns>
         public DisplayOrder GetDisplayOrder(long id)
        {
            return repository.GetById(id);
        }

        /// <summary>
        /// Get all the items
        /// </summary>
        /// <returns>the list of pids</returns>
        public IEnumerable<DisplayOrder> GetAll()
        {
            return repository.GetAll().OrderBy(a => a.Id);
        }
    }
}
