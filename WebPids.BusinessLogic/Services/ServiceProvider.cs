﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebPids.EntityFramework;

namespace WebPids.BusinessLogic
{
    public class ServiceProvider : IDisposable
    {
        private readonly IUnitOfWork unitOfWork;

        private readonly Dictionary<Type, object> services = new Dictionary<Type, object>();


        public ServiceProvider(IUnitOfWork unit)
        {
            this.unitOfWork = unit;
        }

        public static ServiceProvider CreateInstance
        {
            get
            {
                UnitOfWork<WebPIDEntities> unit = new UnitOfWork<WebPIDEntities>();
                unit.IsLazyLoading = false;
                return new ServiceProvider(unit);
            }
        }

        /// <summary>
        /// Get a service by type.
        /// The provider creates only one instance per type.
        /// Subsequent requests for the same type will return the same instance.
        /// </summary>
        /// <typeparam name="TService">The type of service requested</typeparam>
        /// <returns>The service</returns>
        public TService GetService<TService>() where TService : Services
        {
            Type type = typeof(TService);
            if (this.services.ContainsKey(type))
            {
                return this.services[type] as TService;
            }

            TService service = Activator.CreateInstance(type, new object[] { this.unitOfWork }) as TService;

            // This allows access to the service provider in case a service needs other services.
            service.Setup(this);

            // Save it in dictionary to return upon next request
            services[type] = service;

            return service;
        }

        public void Dispose()
        {
            unitOfWork.Dispose();
        }
    }
}
