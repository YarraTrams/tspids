﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebPids.Common;
using WebPids.EntityFramework;

namespace WebPids.BusinessLogic
{
    public class StopServices : Services
    {
        private readonly IRepository<Stop> repository;
        private readonly IRepository<ConfigurationsHistory> configurationsHistoryRepository;

        public StopServices(IUnitOfWork unit)
            : base(unit)
        {
            repository = unitOfWork.GetRepository<Stop>();
            repository.DefaultIncludes = "Configuration";
            repository.UseDefaultIncludes = true;
            configurationsHistoryRepository = unitOfWork.GetRepository<ConfigurationsHistory>();
        }

        /// <summary>
        /// Get the specific record
        /// </summary>
        /// <param name="id">the id</param>
        /// <returns>the object</returns>
        public Stop GetStopById(long id)
        {
            return repository.GetById(id);
        }

        /// <summary>
        /// Get all the records
        /// </summary>
        /// <returns>the list of stops</returns>
        public IEnumerable<Stop> GetAll()
        {
            return repository.GetAll();//.OrderBy(a => a.Id);
        }

        /// <summary>
        /// Get all the records for a specific config
        /// </summary>
        /// <param name="configurationId">the config id</param>
        /// <returns>the list of stops</returns>
        public IEnumerable<Stop> GetStopsForConfiguration(long configurationId)
        {
            return repository.Get(a => a.ConfigurationId == configurationId);
        }

        /// <summary>
        /// Add a new stop
        /// </summary>
        /// <param name="stop">the stop object</param>
        public void Add(Stop stop)
        {
            // Perform integrity checks
            PerformIntegrityChecks(base.unitOfWork, stop);

            repository.Add(stop);
            CreateHistoryRecordForNew(stop);
            repository.SaveAllContextChanges();
        }

        /// <summary>
        /// Update the stop object
        /// </summary>
        /// <param name="stop">the stop object</param>
        public void Update(Stop stop)
        {
            Stop obj = GetStopById(stop.Id);
            obj.TrackerId = stop.TrackerId;
            obj.Direction = stop.Direction;
            obj.Distance = stop.Distance;
            obj.DisplayTitle = stop.DisplayTitle;

            stop.Configuration = obj.Configuration;

            // Perform integrity checks
            PerformIntegrityChecks(base.unitOfWork, obj);

            CreateHistoryRecordForUpdate(obj);
            repository.SaveAllContextChanges();
        }

        /// <summary>
        /// Delete the stop from the db
        /// </summary>
        /// <param name="stop">the stop</param>
        public void Delete(Stop stop)
        {
            CreateHistoryRecordForDelete(stop);
            repository.Delete(stop);
            repository.SaveAllContextChanges();
        }

        /// <summary>
        /// Perform integrity tests for the object
        /// </summary>
        /// <param name="unitOfWork">the UOF from caller</param>
        /// <param name="stop">the object</param>
        private static void PerformIntegrityChecks(IUnitOfWork unitOfWork, Stop stop)
        {
            ConfigurationServices configurationServices = new ConfigurationServices(unitOfWork);

            if (configurationServices.GetConfiguration(stop.ConfigurationId) == null)
            {
                throw new WebPidsException("Invalid configuration id allocated to the stop object", LogLevel.Info);
            }
        }

        /// <summary>
        /// Add a history record for create process
        /// </summary>
        /// <param name="data">The configuration objec</param>
        private void CreateHistoryRecordForNew(Stop data)
        {
            string msg = string.Format("Stop {0} created for Pid {1} - {2} by {3}", data.TrackerId,
                                    data.Configuration.Id, data.Configuration.PIDName, WebPidsConsole.CurrentUserName);
            CreateHistoryRecord(data.ConfigurationId, msg);
        }

        /// <summary>
        /// Create a history record for updates
        /// </summary>
        /// <param name="data">the object</param>
        private void CreateHistoryRecordForUpdate(Stop data)
        {
            string msg = string.Format("Stop {0} - {1} updated for Pid {2} - {3} by {4}",
                data.Id, data.TrackerId, data.Configuration.Id, data.Configuration.PIDName, WebPidsConsole.CurrentUserName);
            CreateHistoryRecord(data.ConfigurationId, msg);
        }

        /// <summary>
        /// Create a history record for delete
        /// </summary>
        /// <param name="data">the object</param>
        private void CreateHistoryRecordForDelete(Stop data)
        {
            string msg = string.Format("Stop {0} - {1} deleted for Pid {2} - {3} by {4}",
                data.Id, data.TrackerId, data.Configuration.Id, data.Configuration.PIDName, WebPidsConsole.CurrentUserName);
            CreateHistoryRecord(data.ConfigurationId, msg);
        }

        /// <summary>
        /// Create a history record
        /// </summary>
        /// <param name="id">the id to use</param>
        /// <param name="msg">the msg</param>
        private void CreateHistoryRecord(long id, string msg)
        {
            ConfigurationsHistory history = new ConfigurationsHistory()
            {
                ConfigurationsId = id,
                Message = msg,
                UserName = Environment.UserName,
                CreatedDate = DateTime.Now
            };

            configurationsHistoryRepository.Add(history);
        }

    }
}
