﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebPids.EntityFramework;

namespace WebPids.BusinessLogic
{
    public class Services
    {
        protected IUnitOfWork unitOfWork { get; set; }

        public Services(IUnitOfWork unit)
        {
            this.unitOfWork = unit;
        }

        /// <summary>
        /// Derived classes can override this to get access to other repositories, etc
        /// </summary>
        /// <param name="provider">The service provider</param>
        public virtual void Setup(ServiceProvider provider)
        {
        }

        /// <summary>
        /// Helper method to get the db name 
        /// </summary>
        public string DatabaseName
        {
            get { return this.unitOfWork.DatabaseName; }
        }
    }
}
