﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebPids.EntityFramework;

namespace WebPids.BusinessLogic
{
    public class DirectionsServices : Services
    {
        private readonly IRepository<Direction> repository;

        public DirectionsServices(IUnitOfWork unit) : base(unit)
        {
            repository = unitOfWork.GetRepository<Direction>();
            repository.DefaultIncludes = "Configurations";
            repository.UseDefaultIncludes = true;
        }

        /// <summary>
        /// Get the specific record
        /// </summary>
        /// <param name="id">the id</param>
        /// <returns>the object</returns>
        public Direction GetDirection(long id)
        {
            return repository.GetById(id);
        }

        /// <summary>
        /// Get all the directions
        /// </summary>
        /// <returns>the list of pids</returns>
        public IEnumerable<Direction> GetAll()
        {
            return repository.GetAll();//.OrderBy(a => a.Id);
        }
    }
}
