﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebPids.EntityFramework;

namespace WebPids.BusinessLogic 
{
    public class GlobalSettingServices : Services
    {
        private readonly IRepository<GlobalSetting> repository;


        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="unit"></param>
        public GlobalSettingServices(IUnitOfWork unit)
            : base(unit)
        {
            repository = unitOfWork.GetRepository<GlobalSetting>(); 
        }

        /// <summary>
        /// Get all items from db
        /// </summary>
        /// <returns></returns>
        public IEnumerable<GlobalSetting> GetAll()
        {
            return repository.Get();
        }

        /// <summary>
        /// Get a specific object
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public GlobalSetting Get(long id)
        {
            return repository.GetById(id);
        }

        /// <summary>
        /// Extract a value using the name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public string GetValByName(string name)
        {
            GlobalSetting obj = GetAll().FirstOrDefault(a => a.Name.Equals(name));
            return obj != null ? obj.Value : string.Empty;
        }

        /// <summary>
        /// Add a new item
        /// </summary>
        /// <param name="obj"></param>
        public void Add(GlobalSetting obj)
        {
            repository.Add(obj);
            repository.SaveAllContextChanges();
        }

        /// <summary>
        /// Update an existing item
        /// </summary>
        /// <param name="obj"></param>
        public void Update(GlobalSetting obj)
        {
            GlobalSetting item = Get(obj.Id);
            item.Value = obj.Value;
            item.Name = obj.Name;
            repository.SaveAllContextChanges();
        }

        /// <summary>
        /// Remove an item from db
        /// </summary>
        /// <param name="obj"></param>
        public void Delete(GlobalSetting obj)
        {
            GlobalSetting data = repository.GetById(obj.Id);
            repository.Delete(data);
            repository.SaveAllContextChanges();
        }
    }
}
