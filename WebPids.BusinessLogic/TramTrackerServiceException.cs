﻿using System;

namespace WebPids.BusinessLogic
{
    class TramTrackerServiceException : Exception
    {
        public TramTrackerServiceException(string message, Exception e)
            : base(message, e)
        {
        }

        public TramTrackerServiceException(string message)
            : base(message)
        {
        }
    }
}
