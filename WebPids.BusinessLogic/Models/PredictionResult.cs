﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WebPids.BusinessLogic.Models
{
    public class PredictionsResult
    {
        public PredictionsResult()
        {
            this.Predictions = new List<Prediction>();
        }

        /// <summary>
        /// The time the object was created 
        /// </summary>
        public DateTime CreatedTime
        {
            get { return DateTime.Now; }
        }

        /// <summary>
        /// The stop 
        /// </summary>
        public string StopNumber { get; set; }

        public long? Distance { get; set; }

        public string Direction { get; set; }

        public string DisplayTitle { get; set; }


        /// <summary>
        /// The tracker id 
        /// </summary>
        public long TrackerId { get; set; }

        /// <summary>
        /// The stop text (for internal reference only)
        /// </summary>
        public string StopText { get; set; }

        /// <summary>
        /// The city direction
        /// </summary>
        public string CityDirection { get; set; }

        /// <summary>
        /// Stop Name
        /// </summary>
        public string StopName { get; set; }

        /// <summary>
        /// List of predictions for the stop
        /// </summary>
        public List<Prediction> Predictions { get; set; }
        
        /// <summary>
        /// The error flag
        /// </summary>
        public bool HasError { get; set; }
        
        /// <summary>
        /// Error message text
        /// </summary>
        public string ErrorMessage { get; set; }
        
        /// <summary>
        /// The time this request was made
        /// </summary>
        public DateTime TimeRequested { get; set; }
    
        /// <summary>
        /// The time the service responded
        /// </summary>
        public DateTime TimeResponded { get; set; }

        /// <summary>
        /// Is it a free zone 
        /// </summary>
        public bool IsInFreeZone { get; set; }
    }
}