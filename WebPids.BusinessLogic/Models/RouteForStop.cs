﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebPids.BusinessLogic.Models
{
    public class RouteForStop
    {
        public RouteForStop()
        {
            this.RoutesForStopsInfo = new List<MainRoutesForStopInfo>();
        }

        /// <summary>
        /// The routes for stop info 
        /// </summary>
        public IEnumerable<MainRoutesForStopInfo> RoutesForStopsInfo {get; set; }

        /// <summary>
        /// The time this request was made
        /// </summary>
        public DateTime TimeRequested { get; set; }

        /// <summary>
        /// The time the service responded
        /// </summary>
        public DateTime TimeResponded { get; set; }
    }
}
