﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebPids.BusinessLogic.Models
{
    class StopInformation
    {
        /// <summary>
        /// The error flag
        /// </summary>
        public bool HasError { get; set; }

        /// <summary>
        /// Error message text
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// The time this request was made
        /// </summary>
        public DateTime TimeRequested { get; set; }

        /// <summary>
        /// The time the service responded
        /// </summary>
        public DateTime TimeResponded { get; set; }

        /// <summary>
        /// The direction of travel
        /// </summary>
        public string CityDirection  { get; set; }

        /// <summary>
        /// The stop no
        /// </summary>
        public string FlagStopNo   { get; set; }

        /// <summary>
        /// Has connecting buses?
        /// </summary>
        public bool HasConnectingBuses { get; set; }

        /// <summary>
        /// Has connecting trains
        /// </summary>
        public bool HasConnectingTrains { get; set; }

        /// <summary>
        /// Has connecting trams
        /// </summary>
        public bool HasConnectingTrams { get; set; }

        /// <summary>
        /// Is it a city stop
        /// </summary>
        public bool IsCityStop { get; set; }
    
        /// <summary>
        /// Is there access for disbled
        /// </summary>
        public bool IsEasyAccess { get; set; }
    
        /// <summary>
        /// Is it a part of the free zone
        /// </summary>
        public bool IsInFreeZone { get; set; }
    
        /// <summary>
        /// Is there a platform
        /// </summary>
        public bool IsPlatformStop { get; set; }
    
        /// <summary>
        /// Is there a shelter
        /// </summary>
        public bool IsShelter { get; set; }
    
        /// <summary>
        /// Is it a YT shelter
        /// </summary>
        public bool IsYTShelter { get; set; }
    
        /// <summary>
        /// The lat
        /// </summary>
        public double Latitude { get; set; }
    
        /// <summary>
        /// The long
        /// </summary>
        public double Longitude { get; set; }
   
        /// <summary>
        /// Stop length in meters
        /// </summary>
        public long StopLength { get; set; }
    
        /// <summary>
        /// Name
        /// </summary>
        public string StopName { get; set; }

        /// <summary>
        /// The zones
        /// </summary>
        public string Zones { get; set; } 
    }
}
