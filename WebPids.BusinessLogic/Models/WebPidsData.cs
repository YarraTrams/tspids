﻿using System.Collections.Generic;
using WebPids.EntityFramework;

namespace WebPids.BusinessLogic.Models
{
    /// <summary>
    /// The web PIDs data object
    /// </summary>
    public class WebPidsData
    {
        /// <summary>
        /// The configuration info
        /// </summary>
        public Configuration PidsConfiguration { get; set; }

        /// <summary>
        /// Is the generic message enabled
        /// </summary>
        public bool IsGenericMessageEnabled { get; set; }

        /// <summary>
        /// Generic message
        /// </summary>
        public string GenericMessage { get; set; }

        /// <summary>
        /// The prediction results for stops
        /// </summary>
        public IEnumerable<PredictionsResult> PredictionsResults { get; set; }
    }
}