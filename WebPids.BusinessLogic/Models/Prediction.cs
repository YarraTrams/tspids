﻿using System;
using System.Runtime.Remoting.Messaging;
using System.Text.RegularExpressions;
using WebPids.Common;

namespace WebPids.BusinessLogic.Models
{
    public class Prediction
    {
        public string Destination { get; set; }
        public bool DisplayAc { get; set; }
        public string DisruptionMessage { get; set; }
        public bool HasDisruption { get; set; }
        public bool HasSpecialEvent { get; set; }
        public string HeadBoardRouteNo { get; set; }
        public bool IsLowFloorTram { get; set; }
        public bool IsTtAvailable { get; set; }

        public bool Display { get; set; }
        public DateTime PredictedArrivalDateTime { get; set; }
        public string SpecialEventMessage { get; set; }
        public string TramClassUrl { get; set; }

        /// <summary>
        /// The predicted arrival date and time string as per business logic
        /// </summary>
        public string PredictedArrivalDateTimeString
        {
            get
            {
                return PredictedArrivalDateTime.ConstructArrivalDateTimeString(); 
            }
        }

        /// <summary>
        /// For detours like (3a, 16a) etc we strip the 'a' and return the route number
        /// </summary>
        public int RouteNumber { get; set; }
    }
}