﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using WebPids.Common;

namespace WebPids.BusinessLogic
{
    public static class ExtensionMethods
    {
        /// <summary>
        /// The flag determines if Pids Log is enabled in config
        /// </summary>
        private static readonly bool PidLogsEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["PidLogsEnabled"]);

        /// <summary>
        /// Get the domain name from user name
        /// </summary>
        /// <param name="username">the user name</param>
        /// <returns>the domain name</returns>
        public static string GetDomain(this string username)
        {
            var stop = username.IndexOf(@"\", StringComparison.Ordinal);
            return (stop > -1) ? username.Substring(0, stop) : string.Empty;
        }

        /// <summary>
        /// Get the user name without domain
        /// </summary>
        /// <param name="username">the full user name</param>
        /// <returns>the user name</returns>
        public static string GetLogin(this string username)
        {
            var stop = username.IndexOf(@"\", StringComparison.Ordinal);
            return (stop > -1) ? username.Substring(stop + 1, username.Length - stop - 1) : string.Empty;
        }

        /// <summary>
        /// Construct the date time string based on business logic
        /// </summary>
        /// <param name="arrivalDateTime"></param>
        /// <returns></returns>
        public static string ConstructArrivalDateTimeString(this DateTime arrivalDateTime)
        {
            var now = DateTime.Now;
            var diff = arrivalDateTime - now;

            if (diff.TotalMinutes < 1)
            {
                return "now";
            }
            else if (diff.TotalMinutes >= 1 && diff.TotalMinutes < 60)
            {
                return Convert.ToInt32(diff.TotalMinutes).ToString();
            }
            else
            {
                return arrivalDateTime.ToString("ddd hh:mm tt", CultureInfo.CreateSpecificCulture("en-US"));
            }
        }

        /// <summary>
        /// Add an elevated log
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configurationId"></param>
        /// <param name="entryType"></param>
        /// <param name="category"></param>
        /// <param name="message"></param>
        public static void Log(this PidsElevatedLogServices services, long configurationId, EventLogEntryType entryType, Category category, string message, int? downTime = null)
        {
            // Write elevated log to windows events if enabled in configuration file
            if (ConfigurationManager.AppSettings["WindowsEventLogWriting"] != "false")
            {
                try
                {
                    WebPidsConsole.WriteEventLog(message, entryType, category);
                }
                catch (Exception e)
                {

                    services.Add(configurationId, EventLogEntryType.Error.ToString(),
                        "Error writing to the windows event log:\n" + e.Message);
                }
            }

            // Write elevated log to database
            services.Add(configurationId, entryType.ToString(), message, downTime);
        }

        ///// <summary>
        ///// Add a information log
        ///// </summary>
        ///// <param name="services"></param>
        ///// <param name="configurationId"></param>
        ///// <param name="message"></param>
        //public static void Log(this PidsLogServices services, short configurationId, string message)
        //{
        //    if (PidLogsEnabled)
        //    {
        //        WebPidsConsole.WriteEventLog(message, EventLogEntryType.Information, Category.Service);
        //        services.Add(configurationId, message);
        //    }
        //}
    }
}
