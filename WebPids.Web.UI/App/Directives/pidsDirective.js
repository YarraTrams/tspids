﻿webPidsApp.directive("pids", function () {
    return {
        restrict: "E",
        templateUrl: "App/PartialViews/pids.html",
        controller: "pidsController",
        scope: {
            name: "=",
            predictions: "=",
            numoflines: "=",
            orientationmode: "=",
            haserror: "=",
            errormessage: "=",
            directionimage: "=",
            distance: "=",
            messages: "=",
            readyForRefresh: "=",
            isgenericmessageenabled: "=",
            genericmessage: "="
        }
    };
});