﻿webPidsApp.directive("pidheader", function () {
    return {
        restrict: "E",
        templateUrl: "App/PartialViews/pidheader.html",
        controller: "pidHeaderController",
        scope: {
            name: "=",
            distance: "=",
            directionimage: "="
        }
    };
});