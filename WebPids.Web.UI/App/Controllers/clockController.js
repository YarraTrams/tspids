﻿"use strict";

webPidsApp.controller("clockController", ["$scope", function ($scope) {
    $scope.clock = Date.now();

    $scope.$on("updateClockEvent", function (scope, data) {
        scope.currentScope.clock = data;
    });
}]);