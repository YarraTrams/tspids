﻿"use strict";
 
webPidsApp.controller("pidsController", ["$scope", "$rootScope", "$timeout", "webPidsService", "webPidsAppConfig", function ($scope, $rootScope, $timeout, webPidsService, webPidsAppConfig) {

    $scope.activeTTID1 = -1;
    var activeTTID2 = -1;
    var screentransitionrefreshrate = 500;

    $scope.firstDisplay = true;
    $scope.messageCounter = 0;
    $scope.finishedMarquee = function () {
        if ($scope.messages == null && $scope.genericmessage == null)
            return;

        var html = "";
        var genericmessage = $scope.genericmessage;
        var messages = $scope.messages;

        if (messages.length > 0) {
            var message = messages[$scope.messageCounter];
                html += "<div style=\"width:50px; float:left; height: 90px; line-height: 90px;\" class=\"" + (message.isdisruption ? "disruption" : "specialevent") + "\"></div>";
                html += "<div style=\"float:left; height: 90px; line-height: 90px;\" class=\"" + (message.isdisruption ? "disruption" : "specialevent") + "\"><img style=\"width:100px; padding-right:20px; margin-bottom:10px;\" class=\"marquee-image\" src=\"" + message.image + "\" />" + message.caption + "</div>";
                html += "<div style=\"width:50px; float:left; height: 90px; line-height: 90px;\" class=\"" + (message.isdisruption ? "disruption" : "specialevent") + "\"></div>";
        } else {
            html = "<div style=\"line-height: 90px;\" class=\"marquee generic\">" + genericmessage + "</div>";
        }
        $("#marquee").html(html);

        if (!$scope.firstDisplay)
            $("#marquee").marquee('destroy');
        else
            $scope.firstDisplay = false;

        $("#marquee").marquee({ duration: $scope.orientationmode === 1 ? 9800 : 12500 });
        $("#marquee").bind('finished', function () { $scope.finishedMarquee(); });

        $scope.messageCounter++;
        if ($scope.messageCounter >= messages.length)
            $scope.messageCounter = 0;
    }

    $scope.$on("$destroy", function () {
        try {
            stopScreenTransitionTimer();
        } catch (e) {
            console.log(e.data);
        }
    });
         
    startScreenTransitionTimer();

    $scope.getDirectionTitle = function(index) {
        if ($scope.predictions == undefined)
            return null;

        return $scope.predictions[index].CityDirection;
    }

    $scope.getPrediction = function (index) {
        if ($scope.predictions == undefined)
            return null;
        return $scope.predictions[index];
    }
 
    // signalled by caller to restart the screen transition 
    $scope.$on("restartProcessingEvent", function (event, data) {
        stopScreenTransitionTimer();
        startScreenTransitionTimer();
        if ($scope.firstDisplay) {
            $scope.finishedMarquee();
        }
    });
  
    // start the screen processing 
    function startScreenTransitionTimer() {
        if ($scope.transitionTimer == null) {
            $scope.transitionTimer = $timeout(restartProcessing, screentransitionrefreshrate);
        }
    }

    // stop the screen processing
    function stopScreenTransitionTimer() {
        if ($scope.transitionTimer != null) {
            $timeout.cancel($scope.transitionTimer);
        }
        $scope.transitionTimer = null;
    }
     
    // determine if the pid is visible 
    $scope.isUnderVisibleLimit = function (index) {
        if (index === $scope.activeTTID1 || index === activeTTID2)
            return true;
        else
            return false;
    };

    // get the isready attribute 
    function getControls() {
        return $scope.orientationmode === 1
            ? $(".isReadyForRefreshLandscape") 
            : $(".isReadyForRefreshPortrait");
    }

    // check if all pids have finished screen transition
    function allTTIDsReady() {
        var objs = getControls();
        for (var i = 0; i < objs.length; i++) {
            var obj = objs[i];
            if (obj.value === "false")
                return false;
        }

        return true;
    }
     
    // determine if its the only pid in forest
    function isOnlyTTId() {
        var objs = getControls();
        return objs.length === 1;
    }

    // finds the next pid thats ready for screen transition. 
    // if all are painted we simply return the first pid index
    function getNextAvailableTTID() {
        var objs = getControls();
        if (objs.length <= 2) {
            return 0;
        } else {
            for (var i = 0; i < objs.length; i++) {
                var obj = objs[i];
                if (obj.value === "false")  
                    return i;
            }
        }

        return 0;
    }

    // Check if the TTID at the given index is ready for new data
    function getReadyFlag(index) {
        var objs = getControls();
        if (index < objs.length) {
            var obj = objs[index];
            return (obj.value === "true");
        }

        return false;
    }

    // check if the pid in question is the last in the forest. this is used to determine the display style
    function isLasTTID(index) {
        var objs = getControls();
        return index === objs.length - 1;
    }

    // get the total number of pids in the forewst
    function getTotalTTIDs() {
        var objs = getControls();
        return objs.length;
    }

    // send screen transition request to the first two pids
    function restartProcessing() {
        $scope.$broadcast("startProcessingPages", 0);
        $scope.$broadcast("startProcessingPages", 1);
        $scope.activeTTID1 = 0;
        activeTTID2 = 1;
    }

    // inidicates if its the first pid
    $scope.isFirstPid = function (index) {
        if (index % 2 === 0)
            return true;
        return false;
    }

    $scope.isOnePid = function (index) {
        if ($scope.predictions.length - 1 === index)
            return true;
        return false;
    }

    // signalled by caller that its finished screen transition
    $scope.$on("finishedPageTransition", function (event, index) {
        if (getTotalTTIDs() > 2) {
            // if all pids have finished screen transition we request for new data and 
            // reset the loop
            if (allTTIDsReady()) {
                $rootScope.$broadcast("loadPidsDataEventFinished", $scope.createdtime);
                $scope.$broadcast("resetIsReady", false);
                // Lets update the UI with new data
                if (isOnlyTTId()) {
                    // update the only one
                    $scope.$broadcast("startProcessingPages", 0);
                    $scope.activeTTID1 = 0;
                    activeTTID2 = -1;
                }
            } else {
                // paint the next available pid
                var idx = parseInt(index);
                var lastAvindex = getNextAvailableTTID();
                if (idx % 2 === 0) {
                    // if its the last one in the loop just paint one 
                    if (isLasTTID(lastAvindex)) {
                        $scope.$broadcast("startProcessingPages", lastAvindex);
                        $scope.activeTTID1 = lastAvindex;
                        activeTTID2 = -1;
                    } else {
                        // the one of the left has finished processing.
                        // check if the one the right is also ready for an update
                        if (getReadyFlag(idx) && getReadyFlag(idx + 1)) {
                            $scope.$broadcast("startProcessingPages", lastAvindex);
                            $scope.$broadcast("startProcessingPages", lastAvindex + 1);
                            $scope.activeTTID1 = lastAvindex;
                            activeTTID2 = lastAvindex + 1;
                        }
                    }
                } else {
                    // the one on the right has finished processing. 
                    // check if the one on the left is also ready for an update
                    if (getReadyFlag(idx) && getReadyFlag(idx - 1)) {
                        $scope.$broadcast("startProcessingPages", lastAvindex);
                        $scope.$broadcast("startProcessingPages", lastAvindex + 1);
                        $scope.activeTTID1 = lastAvindex;
                        activeTTID2 = lastAvindex + 1;
                    }
                }
            }
        } else {
            if (allTTIDsReady()) {
                $rootScope.$broadcast("loadPidsDataEventFinished", $scope.createdtime);
                $scope.$broadcast("resetIsReady", false);
            }
        }
    });

    
}]);