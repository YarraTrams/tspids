﻿"use strict";

webPidsApp.controller("pidBodyController", ["$scope", "$rootScope", "$interval", "$timeout", "webPidsService", "webPidsAppConfig", function($scope, $rootScope, $interval, $timeout, webPidsService, webPidsAppConfig) {
    if (!$scope.prediction || !$scope.pagedpredictions) {
        return;
    }

    var predictions = $scope.prediction.Predictions;

    $scope.showprediction = true;
    $scope.extractdata = true;
    $scope.isready = false;

    $scope.$watch("pagedpredictions", function () {
        if ($scope.showprediction && (parseInt($scope.index)) % 2 === 0 && $scope.$parent.$parent.$parent.activeTTID1 === parseInt($scope.index)) {
            $rootScope.$broadcast("setHeaderDetails", $scope);
        }
    });

    $scope.predictiontimer = null;
    $scope.finishedPageTransitionTimer = null;

    // inform the parent that I have been initialized and ready to accept data
    $rootScope.$broadcast("startLoadPidDataTimerEvent");

    // Set class for body based on whether landscape, portrait, or single tracker id
    $scope.idForPidBodyArrivalsView = function (orientationmode) {
        if (orientationmode === 1)
            return 'arrivals-view';
        if ($scope.isTheOnlyPid())
            return 'arrivals-view-portrait-single';
        return 'arrivals-view-portrait';
    };

    // Get the direction for the given prediction panel
    $scope.getDirectionTitle = function (index) {
        return $scope.citydirection;
    }

    // stop the page transition timer
    function stopPredictionTimer() {
        if ($scope.predictiontimer != null) {
            $interval.cancel($scope.predictiontimer);
        }
        $scope.predictiontimer = null;
    }

    // kill all open handles 
    $scope.$on("$destroy", function(event) {
        try {
            stopPredictionTimer();
        } catch (e) {
            console.log(e.message);
        }
    });

    // using its index check if this is the only pid in the forest
    $scope.isTheOnlyPid = function() {
        var index = parseInt($scope.index);
        if ((index + 1) % 2 === 0) // pids at even index are ignored
            return false;
        var predictionscount = parseInt($scope.predictionscount);
        return (predictionscount - 1 === index);
    }

    // get the number of lines for display
    function getNumOfLines() {
        return $scope.numoflines > predictions.length ? predictions.length : $scope.numoflines;
    }

    function getBasePredictions(page) {
        if ($scope.currentpage >= $scope.numofpages)
            return predictions.slice(0, getNumOfLines());
        else {
            return predictions.slice(page * getNumOfLines(), (page * getNumOfLines()) + getNumOfLines());
        }

    }

    // get the data to display on the page from the list of predictions 
    function getPageOfPredictions(page) {
        var preds = getBasePredictions(page);
        var predsCopy = jQuery.extend(true, [], preds);

        while (predsCopy.length < 3) {
            predsCopy.push({ "Destination": "", "DisplayAc": false, "DisruptionMessage": null, "HasDisruption": false, "HasSpecialEvent": false, "HeadBoardRouteNo": "", "IsLowFloorTram": false, "IsTtAvailable": true, "PredictedArrivalDateTime": "", "SpecialEventMessage": "", "TramClassUrl": "Images/Trams/tram-b.png", "PredictedArrivalDateTimeString": "", "RouteNumber": 0 });
        }
        return predsCopy;
    };

    if ($scope.pagedpredictions.length < 3)
        $scope.pagedpredictions = getPageOfPredictions(1);

    // initialize the page 
    function initPages() {
        $scope.$broadcast("resetIsReady", false);
    };

    // signalled by caller for control to start displaying data on UI
    $scope.$on("startProcessingPages", function(event, index) {
        var scopeindex = parseInt($scope.index);
        if (index === scopeindex) {
            startPredictionTimer();
            initPages();
            stopPageTransitionEvent();



        }
    });

    // async call informing parent the page transition is done and we need new data
    function finishedPageTransition() {
        $rootScope.$broadcast("finishedPageTransition", $scope.index);
    }

    // async call to start the page transition
    function startPageTransitionEvent() {
        $scope.finishedPageTransitionTimer = $timeout(finishedPageTransition, 100);  
    }

    // stop the page transition timer
    function stopPageTransitionEvent() {
        if ($scope.finishedPageTransitionTimer != null) {
            $timeout.cancel($scope.finishedPageTransitionTimer);
        }
        $scope.finishedPageTransitionTimer = null;
    }

    // signalled by caller that data has been refreshed and painting can resume
    $scope.$on("resetIsReady", function(event, value) {
        $scope.isready = value;
        if (value === false) {
            if (predictions.length === 0) {
                $scope.numofpages = 0;
                $scope.currentpage = 0;
                $scope.pagedpredictions = getPageOfPredictions(0);
            } else {
                $scope.numofpages = Math.ceil(predictions.length / getNumOfLines());
                $scope.currentpage = 1;
                $scope.pagedpredictions = getPageOfPredictions(0);
            }
        }
    });

    // lets mark the UI as ready to accept new data
    function markAsReady() {
        $scope.$broadcast("resetIsReady", true);
        stopPredictionTimer();
        startPageTransitionEvent();
    }

    // time callback to paint the page
    function displayNextPageOfPredictions() {
        if ($scope.currentpage === 0 && $scope.numofpages === 0) {
            markAsReady();  // reached here because the prediction engine has sent no data
        }
        else {
            // recycle only if we have one page or we have reached the end of the loop
            if ($scope.numofpages === 1 || ($scope.currentpage >= $scope.numofpages)) {
                markAsReady();
            } else {
                $scope.showprediction = true;
                $scope.pagedpredictions = getPageOfPredictions($scope.currentpage);
                $scope.currentpage++;
            }
        }
    };

    // start the page transition timer
    function startPredictionTimer() {
        if ($scope.predictiontimer == null) {
            $scope.predictiontimer = $interval(displayNextPageOfPredictions, webPidsAppConfig.pagetransitionrefreshrate);
        }
    }
}]);

