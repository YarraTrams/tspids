﻿"use strict";

webPidsApp.controller("pidHeaderController", ["$scope", "$rootScope", function ($scope, $rootScope) {
    // check if the free zone icon should be visible
    $scope.isFreeZoneLogoVisible = function () {
        return $scope.isinfreezone;
    };

    $scope.$on("setHeaderDetails", function (event, data) {
        if (data != null) {
            $scope.distance = data.prediction.Distance;
            $scope.name = data.prediction.DisplayTitle;
            $scope.directionimage = "Images/" + data.prediction.Direction;
        }
    });

}]);