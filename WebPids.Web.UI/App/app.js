﻿var webPidsApp = angular.module("webPidsApp", []);

webPidsApp.constant("webPidsAppConfig", {
    hosturl: "http://10.0.202.110/StationPIDsApi/api/PidsData/",
    pidid: 11,
    pagetransitionrefreshrate: 6000,
    dataextractrate: 15000
});
