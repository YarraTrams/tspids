USE [WebPID_ForUnitTesting]
GO
SET IDENTITY_INSERT [dbo].[DisplayStyles] ON 

GO
INSERT [dbo].[DisplayStyles] ([Id], [StyleId], [Name], [Description]) VALUES (1, 1, N'Single PIDS displayed in centre of screen (1 TTID)', N'Tram stop servicing a tram route heading in a single direction')
GO
INSERT [dbo].[DisplayStyles] ([Id], [StyleId], [Name], [Description]) VALUES (2, 2, N'Two PIDS displayed side-by-side (2 TTID''s)', N'One tram stop servicing one tram route heading in two directions.  The PID will display two tramTracker ID''s')
GO
INSERT [dbo].[DisplayStyles] ([Id], [StyleId], [Name], [Description]) VALUES (3, 3, N'Two stops Three direction s (3 TTID''s)', N'Two PIDS displayed side-by-side.  Screen to transition (animation) to display  single PID in centre of screen.')
GO
INSERT [dbo].[DisplayStyles] ([Id], [StyleId], [Name], [Description]) VALUES (4, 4, N'Two stops Four direction s (4 TTID''s)', N'Two PIDS displayed side-by-side.  Screen to transition (animation) to display  two PIDs.')
GO
SET IDENTITY_INSERT [dbo].[DisplayStyles] OFF
GO
SET IDENTITY_INSERT [dbo].[Configurations] ON 

GO
INSERT [dbo].[Configurations] ([Id], [DisplayStyleId], [PIDName], [Description], [NumOfLines], [RouteNo], [LowFloorOnly], [Enabled]) VALUES (1, 2, N'Dorcas St', N'Eastern Rd - to and from city', 3, 0, 0, 1)
GO
SET IDENTITY_INSERT [dbo].[Configurations] OFF
GO
SET IDENTITY_INSERT [dbo].[Stops] ON 

GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId]) VALUES (1, 1, 1232)
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId]) VALUES (2, 1, 2232)
GO
SET IDENTITY_INSERT [dbo].[Stops] OFF
GO
SET IDENTITY_INSERT [dbo].[UserGroups] ON 

GO
INSERT [dbo].[UserGroups] ([Id], [Name], [Readonly], [SortOrder], [Enabled]) VALUES (2, N'SG-DEV-TramTracker Master Data -Readonly ', 1, 1, 1)
GO
INSERT [dbo].[UserGroups] ([Id], [Name], [Readonly], [SortOrder], [Enabled]) VALUES (4, N'SG-DEV-TramTracker Master Data -Update ', 0, 2, 1)
GO
SET IDENTITY_INSERT [dbo].[UserGroups] OFF
GO
