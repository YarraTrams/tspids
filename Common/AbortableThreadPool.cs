﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace WebPids.Common
{
    /// <summary>
    /// Work item container
    /// </summary>
    public sealed class WorkItem
    {
        private readonly WaitCallback _callback;
        private readonly object _state;
        private readonly ExecutionContext _ctx;

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="wc"></param>
        /// <param name="state"></param>
        /// <param name="ctx"></param>
        internal WorkItem(WaitCallback wc, object state, ExecutionContext ctx)
        {
            _callback = wc;
            _state = state;
            _ctx = ctx;
        }

        /// <summary>
        /// Callback handle
        /// </summary>
        internal WaitCallback Callback
        {
            get { return _callback; }
        }

        /// <summary>
        /// thread state handle
        /// </summary>
        internal object State
        {
            get { return _state; }
        }

        /// <summary>
        /// The execution context
        /// </summary>
        internal ExecutionContext Context
        {
            get { return _ctx; }
        }
    }

    /// <summary>
    /// Work item status
    /// </summary>
    [Flags]
    public enum WorkItemStatus
    {
        Completed,
        Queued,
        Executing,
        Aborted
    }

    /// <summary>
    /// The abortable thread pool class
    /// </summary>
    public static class AbortableThreadPool
    {
        /// <summary>
        /// List of all callbacks
        /// </summary>
        private readonly static LinkedList<WorkItem> _callbacks = new LinkedList<WorkItem>();
        /// <summary>
        /// Dictionary of all threads
        /// </summary>
        private readonly static Dictionary<WorkItem, Thread> _threads = new Dictionary<WorkItem, Thread>();

        /// <summary>
        /// Start a thread pool
        /// </summary>
        /// <param name="callback">the callback</param>
        /// <param name="state">the object from caller</param>
        /// <returns></returns>
        public static WorkItem QueueUserWorkItem(WaitCallback callback, object state)
        {
            if (callback == null) 
                throw new ArgumentNullException("callback");

            var item = new WorkItem(callback, state, ExecutionContext.Capture());
            lock (_callbacks) 
                _callbacks.AddLast(item);

            ThreadPool.QueueUserWorkItem(new WaitCallback(HandleItem));
            return item;
        }

        /// <summary>
        /// handler for the thread
        /// </summary>
        /// <param name="ignored"></param>
        private static void HandleItem(object ignored)
        {
            WorkItem item = null;
            try
            {
                lock (_callbacks)
                {
                    if (_callbacks.Count > 0)
                    {
                        item = _callbacks.First.Value;
                        _callbacks.RemoveFirst();
                    }
                    if (item == null) return;
                    _threads.Add(item, Thread.CurrentThread);
                }
                ExecutionContext.Run(item.Context, delegate { item.Callback(item.State); }, null);
            }
            finally
            {
                lock (_callbacks)
                {
                    if (item != null) _threads.Remove(item);
                }
            }
        }

        /// <summary>
        /// what happens when user cancels operation
        /// </summary>
        /// <param name="item"></param>
        /// <param name="allowAbort"></param>
        /// <returns></returns>
        public static WorkItemStatus Cancel(WorkItem item, bool allowAbort = false)
        {
            if (item == null) throw new ArgumentNullException("item");
            lock (_callbacks)
            {
                LinkedListNode<WorkItem> node = _callbacks.Find(item);
                if (node != null)
                {
                    _callbacks.Remove(node);
                    return WorkItemStatus.Queued;
                }
                else if (_threads.ContainsKey(item))
                {
                    if (allowAbort)
                    {
                        _threads[item].Abort();
                        _threads.Remove(item);
                        return WorkItemStatus.Aborted;
                    }
                    else return WorkItemStatus.Executing;
                }
                else return WorkItemStatus.Completed;
            }
        }
    }
}
