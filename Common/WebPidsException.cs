﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebPids.Common
{
    public sealed class WebPidsException : ApplicationException
    {
        private WebPidsException(string message) : base(message)
        {
            WebPidsConsole.WriteLine(message);
        }

        public WebPidsException(string message, LogLevel logLevel) : this(message)
        {
            WebPidsConsole.WriteLine(message, logLevel);
            if (logLevel == LogLevel.Error)
            {
                WebPidsConsole.WriteEventLog(message, EventLogEntryType.Information, Category.NotMonitored);
            }
        }
    }
}
