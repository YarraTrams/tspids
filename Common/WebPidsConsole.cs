﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Security.Principal;

namespace WebPids.Common
{
    /// <summary>
    /// Logging level
    /// </summary>
    [Flags]
    public enum LogLevel
    {
        /// <summary>
        /// Debug level 
        /// </summary>
        Debug = 0,
        /// <summary>
        /// Information level
        /// </summary>
        Info,
        /// <summary>
        /// Warnings only
        /// </summary>
        Warning,
        /// <summary>
        /// Fatal errors/exceptions only
        /// </summary>
        Error
    }

    /// <summary>
    /// Category of event log
    /// </summary>
    [Flags]
    public enum Category
    {
        NotMonitored = 0,
        PidIsBackOnline,            // A TSPID has come back Online after being marked as offline.
        PidIsInWarningState,        // A TSPID is in the Warning State after being inactive for a set duration.
        PidIsOffline,               // A TSPID PID is Offline.
        PidHardwareWarning,         // A TSPID Hardware Threshold has been Breached.
        PidHardwareError,           // TSPID Hardware is Down.
        HeartbeatServiceError,      // Error in Heartbeat Service.
        PredictionWebServiceError,  // Prediction Web API has thrown an exception while trying to process a PID request.
        DisabledPidRequested,       // Prediction Web API has received a request for a PID that is disabled.
        ConfiguratorError,          // TSPIDS Configurator Web Site has thrown an exception.
        PidDownThresholdBreached,    // More than a predefined number of PIDs have gone down.
        PidIsOfflineforXHours       // A TSPID PID is Offline for Hours.
    }

    /// <summary>
    /// The console class
    /// </summary>
    public class WebPidsConsole
    {
        // Event source name
        public const string EventSource = "Web PIDs";

        // Dummy value to be used in place of PID Id.
        public const int DummyId = -9999;

        // Create a logger for use in this class
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Log a line
        /// </summary>
        /// <param name="message">the string</param>
        /// <param name="logLevel">log level</param>
        public static void WriteLine(String message, LogLevel logLevel = LogLevel.Debug)
        {
            switch (logLevel)
            {
                case LogLevel.Debug:
                    log.Debug(message);
                    break;
                case LogLevel.Error:
                    log.Error(message);
                    break;
                case LogLevel.Warning:
                    log.Warn(message);
                    break;
                case LogLevel.Info:
                    log.Info(message);
                    break;
            }
        }

        /// <summary>
        /// Write the exception to the log
        /// </summary>
        /// <param name="exception">the exception</param>
        /// <param name="logLevel">log level</param>
        public static void WriteLine(Exception exception, LogLevel logLevel = LogLevel.Debug)
        {
#if DEBUG
            WriteLine(exception.ToString(), logLevel);
#else
			WriteLine(exception.Message, logLevel);
#endif
        }

        /// <summary>
        /// Write to Windows Event log
        /// </summary>
        /// <param name="message"></param>
        /// <param name="logLevel"></param>
        /// <param name="category"></param>
        public static void WriteEventLog(string message,
            EventLogEntryType logLevel, Category category)
        {
            var log = "Application";
            var eventid = Convert.ToInt32(ConfigurationManager.AppSettings["EventlogId"]);
            if (!EventLog.SourceExists(EventSource))
            {
                EventLog.CreateEventSource(EventSource, log);
            }

            EventLog.WriteEntry(EventSource, message, logLevel, eventid, (short)category, null);
        }

        /// <summary>
        /// Get the current logged in user using HTTP
        /// </summary>
        static public string CurrentUserName
        {
            get
            {
                var windowsIdentity = WindowsIdentity.GetCurrent();
                if (windowsIdentity != null)
                    return System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.User != null
                        ? System.Web.HttpContext.Current.User.Identity.Name
                        : windowsIdentity.Name;
                return string.Empty;
            }
        }
    }
}
