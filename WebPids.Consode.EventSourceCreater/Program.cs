﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebPids.Common;

namespace WebPids.Consode.EventSourceCreater
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create the source, if it does not already exist. 
            var src = "Web PIDs";
            var log = "Application";
            if (!EventLog.SourceExists(src))
            {
                //An event log source should not be created and immediately used. 
                //There is a latency time to enable the source, it should be created 
                //prior to executing the application that uses the source. 
                //Execute this sample a second time to use the new source.
                EventLog.CreateEventSource(WebPidsConsole.EventSource, log);
                Console.WriteLine(string.Format("Event source \"{0}\" was successfully created.", src));
                Console.WriteLine("Exiting, execute the application a second time to use the source.");
                Console.Read();
                // The source is created.  Exit the application to allow it to be registered. 
                return;
            }

            // Create an EventLog instance and assign its source.
            EventLog eventLog = new EventLog();
            eventLog.Source = WebPidsConsole.EventSource;

            // Write an informational entry to the event log.    
            eventLog.WriteEntry(string.Format("Testing event source \"{0}\".", WebPidsConsole.EventSource));
            Console.WriteLine(string.Format("Event source \"{0}\" has been created and tested.", src));
            Console.Read();
        }
    }
}
