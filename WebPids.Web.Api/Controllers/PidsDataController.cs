﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using WebPids.BusinessLogic;
using WebPids.BusinessLogic.Models;
using WebPids.Common;

namespace WebPids.Web.Api.Controllers
{
    public class PidsDataController : ApiController
    {

        /// <summary>
        /// Simple method to ping the API and valid that it is alive
        /// </summary>
        /// <returns>JSON validation message of activity</returns>
        /// <example>http://webaddress/api/PidsData/Ping</example>
        [System.Web.Http.HttpGet]
        public dynamic Ping()
        {
            return JObject.Parse(@"{""status"":""active""}");
        }

        /// <summary>
        /// Get the pids data
        /// </summary>
        /// <param name="pidId">the id</param>
        /// <returns>the payload</returns>
        /// <example>http://webaddress/api/PidsData/GetPidsData?pidId=1</example>
        [System.Web.Http.HttpGet]
        public dynamic GetPidsData(long pidId)
        {
            try
            {               
                using (var sp = ServiceProvider.CreateInstance)
                {
                    var startTime = DateTime.Now;

                    var configurationServices = sp.GetService<ConfigurationServices>();
                    var pidsLogServices = sp.GetService<PidsLogServices>();
                    var pidsElevatedLogServices = sp.GetService<PidsElevatedLogServices>();
                    var globalSettingServices = sp.GetService<GlobalSettingServices>();
                    var defaultDisruptionMessage = globalSettingServices.GetValByName("DefaultDisruptionMessage");

                    var configuration = configurationServices.GetConfiguration(pidId);
                    var heartBeatLastUpdatedTime = configuration.HeartbeatLastUpdatedTime;
                    var piddata = configurationServices.GetPidData(pidId);
                    if (piddata == null)
                    {
                        // error reporting
                        var msg = string.Format("Error extracting PID configuration for Id {0}", pidId);
                        WebPidsConsole.WriteLine(msg, LogLevel.Error);
                        pidsElevatedLogServices.Log(pidId, EventLogEntryType.Error, Category.PredictionWebServiceError, msg);
                        
                        var errobj = CreateErrorObject(pidId, msg); 
                        return errobj;
                    }

                    if (piddata.PidsConfiguration.Enabled)
                    {
                        // flush out all the messages 
                        var hasError = piddata.PredictionsResults.Any() && piddata.PredictionsResults.First().HasError;
                        var errorMessage = hasError ? piddata.PredictionsResults.First().ErrorMessage : string.Empty;
                        var messages = CollectMessages(piddata.PredictionsResults, defaultDisruptionMessage);

                        // Set message if service is not available
                        var firstPredictionResult = piddata.PredictionsResults.FirstOrDefault();
                        if (firstPredictionResult != null)
                        {
                            var firstPrediction = firstPredictionResult.Predictions.FirstOrDefault();
                            if (firstPrediction != null && firstPrediction.HeadBoardRouteNo.Equals("0", StringComparison.OrdinalIgnoreCase))
                            {
                                var services = sp.GetService<GlobalSettingServices>();
                                firstPrediction.DisruptionMessage = services.GetValByName("ServiceUnavailableMessage");
                            }
                        }

                        // create a consolidated object
                        var newobj = new
                        {
                            pidId,
                            pidName = piddata.PidsConfiguration.PIDName,
                            description = piddata.PidsConfiguration.Description,
                            networkName = piddata.PidsConfiguration.NetworkName,
                            orientationMode = piddata.PidsConfiguration.OrientationMode,
                            numOfLines = piddata.PidsConfiguration.NumOfLines,
                            imageName = piddata.PidsConfiguration.Direction.ImageName,
                            distance = piddata.PidsConfiguration.Distance,
                            isGenericMessageEnabled  = piddata.IsGenericMessageEnabled,
                            genericMessage = piddata.GenericMessage,
                            predictionsResults = piddata.PredictionsResults,
                            messages,
                            hasError,
                            errorMessage
                        };

                        // Report event only if the pid is in error state
                        if (configuration.ErrorStatus == 2 || configuration.ErrorStatus == 3)
                        {
                            var pidDownTime = (int) DateTime.Now.Subtract(heartBeatLastUpdatedTime).TotalMinutes;
                            var msg = string.Format("ONLINE - PID Id {0} - Asset Location Number {1} - at {2} station - {3} has responded after being down for {4} minutes from {5} to {6}.", 
                                configuration.Id, configuration.PIDAssetLocationNo, configuration.StationName, configuration.PIDName, pidDownTime, heartBeatLastUpdatedTime, DateTime.Now);
                            pidsElevatedLogServices.Log(configuration.Id, EventLogEntryType.Warning, Category.PidIsBackOnline, msg, pidDownTime);
                        }

                        pidsLogServices.Add((short)configuration.Id, String.Format("Heartbeat for PID Id {0}", configuration.Id));

                        // Update the processing time taken
                        var span = DateTime.Now.Subtract(startTime);
                        configurationServices.UpdatePageProcessingTimeTaken(configuration, span);

                        return newobj;
                    }

                    // PID is disabled.
                    var warning = string.Format("Disabled PID requested. PID Id: {0}", pidId);
                    var disabledobj = CreateErrorObject(pidId, warning);
                    return disabledobj;
                }
            }
            catch (Exception exception)
            {
                // Exception handling (hide actual exception from end user)
                var msg = string.Format("Exception {0} precessing PID request for Id {1} ", exception, pidId);
                WebPidsConsole.WriteLine(msg, LogLevel.Error);
                using (var sp = ServiceProvider.CreateInstance)
                {
                    var pidsElevatedLogServices = sp.GetService<PidsElevatedLogServices>();
                    pidsElevatedLogServices.Log(pidId, EventLogEntryType.Error, Category.PredictionWebServiceError, msg);
                }

                var errobj = CreateErrorObject(pidId, string.Format("Error precessing PID request for Id {0}", pidId));
                return errobj;
            }
        }

        /// <summary>
        /// Create the error object
        /// </summary>
        /// <param name="pidId">the pid id</param>
        /// <param name="errorMessage">error message to report</param>
        /// <returns>the error object</returns>
        private static dynamic CreateErrorObject(long pidId, string errorMessage)
        {
            return new
            {
                pidId,
                hasError = true,
                errorMessage
            };
        }

        /// <summary>
        /// Collect all disruption and special event messages into one container
        /// </summary>
        /// <param name="items">the prediction results</param>
        /// <param name="defaultDisruptionMessage">default disruption message</param>
        /// <returns>the messages container</returns>
        private static IEnumerable<dynamic> CollectMessages(IEnumerable<PredictionsResult> items, string defaultDisruptionMessage)
        {
            var resultsDistruption = new List<dynamic>();
            var resultsSpecialMessage = new List<dynamic>();

            // Though the following code takes all predictions into account in its first iteration and later filter for unique, it 
            // is better than assuming the first record in a prediction array 
            // i.e. items.Select(item => item.Predictions.FirstOrDefault()).Where(prediction => prediction != null)
            // will be repeated. This way we have taken care for future business logic changes
            foreach (var prediction in items.SelectMany(item => item.Predictions))
            {
                if (prediction.HasDisruption)
                {
                    var data = new
                    {
                        isdisruption = true,
                        image = "Images/disruption-marquee.png",
                        caption = string.IsNullOrEmpty(prediction.DisruptionMessage)
                                        ? string.Format(defaultDisruptionMessage, prediction.RouteNumber)   // message from db
                                        : prediction.DisruptionMessage
                    };

                    resultsDistruption.Add(data);
                }

                if (prediction.HasSpecialEvent)
                {
                    var data = new
                    {
                        isdisruption = false,
                        image = "Images/special-event-marquee.png",
                        caption = prediction.SpecialEventMessage
                    };

                    resultsSpecialMessage.Add(data);
                }
            }


            //  Filter out duplicates
            resultsDistruption.AddRange(resultsSpecialMessage);
            return resultsDistruption.Distinct();
        }
    }
}
