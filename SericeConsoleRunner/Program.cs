﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using WebPids.BusinessLogic;
using WebPids.WebPidsService;

namespace WebPids.WebPidsService
{
    static class Program
    {
        private static IContainer _container = null;

        static void Main(string[] args)
        {
            _container = Configure();
            // Log4net
            //XmlConfigurator.Configure();
            var program = new BaseProgram(Configure());

            var hb = new HeartbeatService(program);
            hb.Process(null);
        }

        private static IContainer Configure()
        {
            var builder = new ContainerBuilder();

            // Register the service provider
            builder.RegisterInstance(ServiceProvider.CreateInstance);

            return builder.Build();
        }

        public static ServiceProvider ServiceProvider
        {
            get { return _container.Resolve<ServiceProvider>(); }
        }
    }
}
