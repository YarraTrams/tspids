﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebPids.BusinessLogic;
using WebPids.Common;

namespace EventLogWriter.Controllers
{
    public class EventController : ApiController
    {
        /// <summary>
        /// Generate an error event indicating a PID has had a hardware level trigger
        /// that is being reported as an error
        /// This error will be written as a windows event log
        /// </summary>
        /// <param name="pidId">The Id of the TSPID reporting the error</param>
        /// <param name="msg">The body of the error message</param>
        [System.Web.Http.HttpGet]
        public void PutErrorEvent(int pidId, string msg)
        {
            PutEvent(pidId, msg, EventLogEntryType.Error, Category.PidHardwareError);
        }

        [System.Web.Http.HttpGet]
        public void PutWarningEvent(int pidId, string msg)
        {
            PutEvent(pidId, msg, EventLogEntryType.Warning, Category.PidHardwareWarning);
        }

        private void PutEvent(int pidId, string msg, EventLogEntryType eventType, Category category)
        {
            try
            {
                // Only write windows log if this instance of the event log service is enabled
                if (System.Configuration.ConfigurationManager.AppSettings["Enabled"] == "true")
                {
                    using (ServiceProvider sp = ServiceProvider.CreateInstance)
                    {
                        sp.GetService<PidsElevatedLogServices>()
                            .Log(pidId, eventType, category, msg);
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error creating windows event log for pid[" + pidId + "]", e);
            }
            finally
            {

            }   
        }
    }
}
