﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebPids.BusinessLogic;
using WebPids.Common;
using Configuration = WebPids.EntityFramework.Configuration;

namespace WebPids.WebPidsService
{
    /// <summary>
    /// Monitors the heart rate of all PID devices
    /// </summary>
    public partial class HeartbeatService : ServiceBase
    {
        public BaseProgram Program = null;
        public HeartbeatService(BaseProgram  program)
        {
            Program = program;
            InitializeComponent();
        }

        #region Properties

        /// <summary>
        /// The business layer
        /// </summary>
        private ConfigurationServices ConfigurationServices
        {
            get { return Program.ServiceProvider.GetService<ConfigurationServices>(); }
        }

        /// <summary>
        /// The elevated log services
        /// </summary>
        private PidsElevatedLogServices PidsElevatedLogServices
        {
            get { return Program.ServiceProvider.GetService<PidsElevatedLogServices>(); }
        }
         
        #endregion

        #region Attributes
        /// <summary>
        /// Heartbeat delay for thread to wait (from config)
        /// </summary>
        private readonly static TimeSpan HeartBeatDelay = new TimeSpan(0, 0, 0, 0, Convert.ToInt32(ConfigurationManager.AppSettings["HeartBeatDelay"]));
        /// <summary>
        /// Threshold for filtering data (from config)
        /// </summary>
        private readonly static TimeSpan HeartBeatThreshold = new TimeSpan(0, 0, 0, 0, Convert.ToInt32(ConfigurationManager.AppSettings["HeartBeatThreshold"]));
        /// <summary>
        /// Threshold for filtering data for dead pids (from config)
        /// </summary>
        private readonly static TimeSpan HeartBeatErrorThreshold = new TimeSpan(0, 0, 0, 0, Convert.ToInt32(ConfigurationManager.AppSettings["HeartBeatErrorThreshold"]));
        /// <summary>
        /// Threshold for filtering data for dead pids (from config)
        /// </summary>
        private readonly static TimeSpan HeartBeatErrorThresholdForXHours = new TimeSpan(0, Convert.ToInt32(ConfigurationManager.AppSettings["HeartBeatErrorThresholdforXHours"]), 0, 0, 0);
        /// <summary> 
        /// The number of PIDs that should go offline before a critical alert is raised.
        /// </summary>
        private readonly static int PidDownThreshold = Convert.ToInt32(ConfigurationManager.AppSettings["PidDownThreshold"]);
        /// <summary>
        /// Monitor if the PidDownThreshold has been breached.
        /// </summary>
        private static bool pidDownThresholdBreached = false;
        /// <summary>
        /// The lock object
        /// </summary>
        private readonly object LockObject = new object();
        /// <summary>
        /// The thread handle
        /// </summary>
        private WorkItem WorkItem { get; set; }
        #endregion

        /// <summary>
        /// Start the service
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart(string[] args)
        {
            PerformStartUpTests();
            WorkItem = AbortableThreadPool.QueueUserWorkItem(Process, null);
            base.OnStart(args);
        }

        private void PerformStartUpTests()
        {
            //Test database connectivity - Log4net
            try
            {
                WebPidsConsole.WriteLine("HeartbeatService started", LogLevel.Info);
            }
            catch (Exception ex)
            {
                WebPidsConsole.WriteEventLog(string.Format("Heartbeat Service Error while Log4net was trying to access the database:\n {0}", ex.Message), EventLogEntryType.Error, Category.NotMonitored);
                return;
            }

            //Test database connectivity - Entity Framework
            try
            {
                PidsElevatedLogServices.Add(WebPidsConsole.DummyId, EventLogEntryType.Information.ToString(), "Heartbeat Service Starting. Testing entity framework database connection.");
            }
            catch(Exception ex)
            {
                WebPidsConsole.WriteEventLog(string.Format("Heartbeat Service Error while entity framework was trying to access database:\n {0}", ex.Message), EventLogEntryType.Error, Category.NotMonitored);
                return;
            }

            //Test event log access
            try
            {
                WebPidsConsole.WriteEventLog("Heartbeat Service is started and connected to the database.", EventLogEntryType.Information, Category.NotMonitored);
            }
            catch (Exception ex)
            {
                WebPidsConsole.WriteLine(string.Format("Heartbeat Service Error while writing to the windows event log:\n", ex.Message), LogLevel.Error);
            }
        }

        /// <summary>
        /// Stop the service 
        /// </summary>
        protected override void OnStop()
        {
            var status = AbortableThreadPool.Cancel(WorkItem);
            base.OnStop();
            WebPidsConsole.WriteLine("HeartbeatService stopped", LogLevel.Info);
        }

        /// <summary>
        /// The processing loop
        /// </summary>
        /// <param name="data"></param>
        public void Process(object data)
        {
            while (true)
            {
                lock (LockObject)
                {
                    try
                    {
                        //Get all dead PIDs.
                        var allDeadPids = ConfigurationServices.GetUnhealthyPids(HeartBeatErrorThreshold).Where(a => a.Enabled);
                        var numberOfDeadPids = allDeadPids.Count();

                        if (!pidDownThresholdBreached)
                        {
                            if (numberOfDeadPids >= PidDownThreshold)
                            {
                                var msg = string.Format("CRITICAL ERROR - More than {0} PIDs are offline. The actual number of PIDs that are offline is {1}.", PidDownThreshold, numberOfDeadPids);
                                PidsElevatedLogServices.Log(WebPidsConsole.DummyId, EventLogEntryType.Error, Category.PidDownThresholdBreached, msg);
                                pidDownThresholdBreached = true;
                            }
                        }
                        else
                        {
                            if (numberOfDeadPids < PidDownThreshold)
                            {
                                var msg = string.Format("CRITICAL ERROR IS DOWNGRADED - Less than {0} PIDs are offline now. The actual number of PIDs that are offline now is {1}.", PidDownThreshold, numberOfDeadPids);
                                PidsElevatedLogServices.Log(WebPidsConsole.DummyId, EventLogEntryType.Information, Category.PidDownThresholdBreached, msg);
                                pidDownThresholdBreached = false;
                            }
                        }

                        //Get all dead PIDs that has been down for hours.
                        var allDeadPidsForHours = ConfigurationServices.GetUnhealthyPidsDownForXHours(HeartBeatErrorThresholdForXHours).Where(a => a.Enabled && a.ErrorStatus != 3);

                        if (allDeadPidsForHours.Count() != 0)
                        {
                            foreach (var item in allDeadPidsForHours)
                            {
                                var msg =
                                    string.Format(
                                        "ERR0R: OFFLINE for more than {4} hours - PID Id: {0} \nEvent: PID down \nMessage: PID Id {0} - Asset Location Number {1} - at {2} station has not responded since {3} and is considered down.",
                                        item.Id, item.PIDAssetLocationNo, item.StationName, item.HeartbeatLastUpdatedTime, HeartBeatErrorThresholdForXHours.Hours);

                                PidsElevatedLogServices.Log(item.Id, EventLogEntryType.Error, Category.PidIsOfflineforXHours, msg);
                                ConfigurationServices.SetErrorStatus(item, 3);

                            }

                        }
                        

                        //Get PIDs that have died recently.
                        var newDeadPids = allDeadPids.Where(a => (a.ErrorStatus != 2 && a.ErrorStatus != 3));
                        var configurations = newDeadPids as IList<Configuration> ?? newDeadPids.ToList();

                        foreach (var item in configurations)
                        {
                            var msg =
                                string.Format(
                                    "OFFLINE - PID Id: {0} \nEvent: PID down \nMessage: PID Id {0} - Asset Location Number {1} - at {2} station has not responded since {3} and is considered down.",
                                    item.Id, item.PIDAssetLocationNo, item.StationName, item.HeartbeatLastUpdatedTime);

                            PidsElevatedLogServices.Log(item.Id, EventLogEntryType.Error, Category.PidIsOffline, msg);

                            ConfigurationServices.SetErrorStatus(item, 2);
                        }

                        // report warnings for pids that have not responded for a short while
                        var unprocessedPids =
                            ConfigurationServices.GetUnhealthyPids(HeartBeatThreshold)
                                .Where(a => (a.ErrorStatus != 2 && a.ErrorStatus != 3  && a.ErrorStatus != 1) && a.Enabled);
                        foreach (var item in unprocessedPids)
                        {
                            var msg =
                                string.Format(
                                    "DELAYED - PID Id: {0} \nEvent: PID response delayed \nMessage: Pid id {0} - Asset Location Number {1} - at {2} station has not responded since {3}.",
                                    item.Id, item.PIDAssetLocationNo, item.StationName, item.HeartbeatLastUpdatedTime);

                            PidsElevatedLogServices.Log(item.Id, EventLogEntryType.Warning, Category.PidIsInWarningState,
                                msg);

                            ConfigurationServices.SetErrorStatus(item, 1);
                        }

                        // Wait until the next parse
                        Thread.Sleep(HeartBeatDelay);           }
                    catch (ThreadAbortException excp)
                    {
                        // Ignore the exception and continue
                        WebPidsConsole.WriteLine(excp);
                    }
                    catch (Exception excp)
                    {
                        // Report the problem
                        PidsElevatedLogServices.Log(WebPidsConsole.DummyId, EventLogEntryType.Error, Category.HeartbeatServiceError, string.Format("HeartbeatService Exception : {0}", excp.Message));
                    }

                }
            }
        }
    }
}
