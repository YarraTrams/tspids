﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using log4net.Config;
using WebPids.BusinessLogic;
using WebPids.Common;

namespace WebPids.WebPidsService
{
    public class BaseProgram
    {
        private IContainer container = null;

        public BaseProgram(IContainer c)
        {
            container = c;
        }

        public ServiceProvider ServiceProvider
        {
            get { return container.Resolve<ServiceProvider>(); }
        }
    }

    static class Program
    {
        /// <summary>
        /// The IoC container
        /// </summary>

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            var program = new BaseProgram(Configure());
            // Log4net
            XmlConfigurator.Configure();

            #if DEBUG
                        Debugger.Launch();
            #endif 

            var servicesToRun  = new ServiceBase[] 
            { 
                new HeartbeatService(program)
            };

            ServiceBase.Run(servicesToRun);
        }

        /// <summary>
        /// Configure autofac
        /// </summary>
        /// <returns>the container</returns>
        private static IContainer Configure()
        {
            var builder = new ContainerBuilder();

            // Register the service provider
            builder.RegisterInstance(ServiceProvider.CreateInstance);

            return builder.Build();
        }
    }
}
