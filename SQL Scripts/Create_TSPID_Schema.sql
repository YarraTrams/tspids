ALTER TABLE [dbo].[TSPIDS_Stops] DROP CONSTRAINT [FK_TSPIDS_Stops_TSPIDS_Directions]
GO
ALTER TABLE [dbo].[TSPIDS_Stops] DROP CONSTRAINT [FK_Stops_Configurations]
GO
ALTER TABLE [dbo].[TSPIDS_PID] DROP CONSTRAINT [FK_Configurations_DisplayOrder]
GO
ALTER TABLE [dbo].[TSPIDS_PID] DROP CONSTRAINT [FK_Configurations_Directions]
GO
/****** Object:  Index [IX_TSPIDS_PidsLog]    Script Date: 6/08/2015 1:19:45 PM ******/
DROP INDEX [IX_TSPIDS_PidsLog] ON [dbo].[TSPIDS_PidsLog]
GO
/****** Object:  Index [IX_ELMAH_Error_App_Time_Seq]    Script Date: 6/08/2015 1:19:45 PM ******/
DROP INDEX [IX_ELMAH_Error_App_Time_Seq] ON [dbo].[TSPIDS_ELMAH_Error]
GO
/****** Object:  Table [dbo].[TSPIDS_UserGroups]    Script Date: 6/08/2015 1:19:45 PM ******/
DROP TABLE [dbo].[TSPIDS_UserGroups]
GO
/****** Object:  Table [dbo].[TSPIDS_Stops]    Script Date: 6/08/2015 1:19:45 PM ******/
DROP TABLE [dbo].[TSPIDS_Stops]
GO
/****** Object:  Table [dbo].[TSPIDS_PidsLogHist]    Script Date: 6/08/2015 1:19:45 PM ******/
DROP TABLE [dbo].[TSPIDS_PidsLogHist]
GO
/****** Object:  Table [dbo].[TSPIDS_PidsLog]    Script Date: 6/08/2015 1:19:45 PM ******/
DROP TABLE [dbo].[TSPIDS_PidsLog]
GO
/****** Object:  Table [dbo].[TSPIDS_PidsElevatedLog]    Script Date: 6/08/2015 1:19:45 PM ******/
DROP TABLE [dbo].[TSPIDS_PidsElevatedLog]
GO
/****** Object:  Table [dbo].[TSPIDS_PID]    Script Date: 6/08/2015 1:19:45 PM ******/
DROP TABLE [dbo].[TSPIDS_PID]
GO
/****** Object:  Table [dbo].[TSPIDS_LogService]    Script Date: 6/08/2015 1:19:45 PM ******/
DROP TABLE [dbo].[TSPIDS_LogService]
GO
/****** Object:  Table [dbo].[TSPIDS_Log]    Script Date: 6/08/2015 1:19:45 PM ******/
DROP TABLE [dbo].[TSPIDS_Log]
GO
/****** Object:  Table [dbo].[TSPIDS_GlobalSettings]    Script Date: 6/08/2015 1:19:45 PM ******/
DROP TABLE [dbo].[TSPIDS_GlobalSettings]
GO
/****** Object:  Table [dbo].[TSPIDS_ELMAH_Error]    Script Date: 6/08/2015 1:19:45 PM ******/
DROP TABLE [dbo].[TSPIDS_ELMAH_Error]
GO
/****** Object:  Table [dbo].[TSPIDS_DisplayOrder]    Script Date: 6/08/2015 1:19:45 PM ******/
DROP TABLE [dbo].[TSPIDS_DisplayOrder]
GO
/****** Object:  Table [dbo].[TSPIDS_Directions]    Script Date: 6/08/2015 1:19:45 PM ******/
DROP TABLE [dbo].[TSPIDS_Directions]
GO
/****** Object:  Table [dbo].[TSPIDS_ConfigurationsHistory]    Script Date: 6/08/2015 1:19:45 PM ******/
DROP TABLE [dbo].[TSPIDS_ConfigurationsHistory]
GO
/****** Object:  Table [dbo].[TSPIDS_ConfigurationsHistory]    Script Date: 6/08/2015 1:19:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TSPIDS_ConfigurationsHistory](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ConfigurationsId] [bigint] NOT NULL,
	[Message] [varchar](5000) NULL,
	[UserName] [varchar](5000) NOT NULL,
	[CreatedDate] [datetime] NOT NULL DEFAULT (getdate()),
 CONSTRAINT [PK_ConfigurationsHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TSPIDS_Directions]    Script Date: 6/08/2015 1:19:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TSPIDS_Directions](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Direction] [varchar](5000) NOT NULL,
	[ImageName] [varchar](5000) NOT NULL,
 CONSTRAINT [PK_Directions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TSPIDS_DisplayOrder]    Script Date: 6/08/2015 1:19:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TSPIDS_DisplayOrder](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](200) NOT NULL,
 CONSTRAINT [PK_DisplayOrder] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TSPIDS_ELMAH_Error]    Script Date: 6/08/2015 1:19:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TSPIDS_ELMAH_Error](
	[ErrorId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_ELMAH_Error_ErrorId]  DEFAULT (newid()),
	[Application] [nvarchar](60) NOT NULL,
	[Host] [nvarchar](50) NOT NULL,
	[Type] [nvarchar](100) NOT NULL,
	[Source] [nvarchar](60) NOT NULL,
	[Message] [nvarchar](500) NOT NULL,
	[User] [nvarchar](50) NOT NULL,
	[StatusCode] [int] NOT NULL,
	[TimeUtc] [datetime] NOT NULL,
	[Sequence] [int] IDENTITY(1,1) NOT NULL,
	[AllXml] [ntext] NOT NULL,
 CONSTRAINT [PK_ELMAH_Error] PRIMARY KEY NONCLUSTERED 
(
	[ErrorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TSPIDS_GlobalSettings]    Script Date: 6/08/2015 1:19:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TSPIDS_GlobalSettings](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](5000) NOT NULL,
	[Value] [varchar](5000) NOT NULL,
 CONSTRAINT [PK_GlobalSettings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TSPIDS_Log]    Script Date: 6/08/2015 1:19:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TSPIDS_Log](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Thread] [varchar](255) NOT NULL,
	[Level] [varchar](50) NOT NULL,
	[Logger] [varchar](255) NOT NULL,
	[Message] [varchar](4000) NOT NULL,
	[Exception] [varchar](2000) NULL,
 CONSTRAINT [PK_Log] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TSPIDS_LogService]    Script Date: 6/08/2015 1:19:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TSPIDS_LogService](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Thread] [varchar](255) NOT NULL,
	[Level] [varchar](50) NOT NULL,
	[Logger] [varchar](255) NOT NULL,
	[Message] [varchar](4000) NOT NULL,
	[Exception] [varchar](2000) NULL,
 CONSTRAINT [PK_LogService] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TSPIDS_PID]    Script Date: 6/08/2015 1:19:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TSPIDS_PID](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[OrientationMode] [int] NOT NULL,
	[PIDName] [varchar](200) NOT NULL,
	[Description] [varchar](5000) NULL,
	[NumOfLines] [int] NOT NULL CONSTRAINT [DF_Configurations_NumOfLines]  DEFAULT ((3)),
	[DirectionsId] [bigint] NOT NULL,
	[Distance] [int] NOT NULL,
	[Enabled] [bit] NOT NULL,
	[MonitorHeartBeat] [bit] NOT NULL CONSTRAINT [DF__Configura__Monit__1367E606]  DEFAULT ((1)),
	[HeartbeatLastUpdatedTime] [datetime] NOT NULL CONSTRAINT [DF__Configura__Heart__145C0A3F]  DEFAULT (getdate()),
	[MonitorPageProcessing] [bit] NOT NULL CONSTRAINT [DF__Configura__Monit__15502E78]  DEFAULT ((1)),
	[PageProcessingTimeTaken] [bigint] NOT NULL CONSTRAINT [DF__Configura__PageP__164452B1]  DEFAULT ((0)),
	[NetworkName] [varchar](5000) NULL,
	[ErrorStatus] [int] NULL,
	[DisplayOrderId] [bigint] NOT NULL,
	[ConfigurationURL] [nvarchar](200) NULL,
 CONSTRAINT [PK_Configurations] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TSPIDS_PidsElevatedLog]    Script Date: 6/08/2015 1:19:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TSPIDS_PidsElevatedLog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL,
	[ConfigurationId] [bigint] NOT NULL,
	[Level] [varchar](50) NOT NULL,
	[Message] [varchar](5000) NOT NULL,
 CONSTRAINT [PK_PidsElevatedLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TSPIDS_PidsLog]    Script Date: 6/08/2015 1:19:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TSPIDS_PidsLog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL,
	[ConfigurationId] [smallint] NOT NULL,
	[Level] [varchar](50) NOT NULL,
	[Message] [varchar](5000) NOT NULL,
 CONSTRAINT [PK_TSPIDS_PidsLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TSPIDS_PidsLogHist]    Script Date: 6/08/2015 1:19:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TSPIDS_PidsLogHist](
	[LogDateHour] [datetime] NOT NULL,
	[ConfigurationId] [smallint] NOT NULL,
	[AvgResponseTimeInSec] [int] NOT NULL,
 CONSTRAINT [PK_PidsLogHist_LogHour_ConfId] PRIMARY KEY CLUSTERED 
(
	[LogDateHour] ASC,
	[ConfigurationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TSPIDS_Stops]    Script Date: 6/08/2015 1:19:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TSPIDS_Stops](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ConfigurationId] [bigint] NOT NULL,
	[TrackerId] [bigint] NOT NULL,
	[DisplayTitle] [varchar](100) NULL,
	[Direction] [bigint] NULL,
	[Distance] [int] NULL,
 CONSTRAINT [PK_Stops] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TSPIDS_UserGroups]    Script Date: 6/08/2015 1:19:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TSPIDS_UserGroups](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](5000) NOT NULL,
	[Readonly] [bit] NOT NULL,
	[SortOrder] [int] NOT NULL,
	[Enabled] [bit] NOT NULL,
 CONSTRAINT [PK_Groups] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_ELMAH_Error_App_Time_Seq]    Script Date: 6/08/2015 1:19:45 PM ******/
CREATE NONCLUSTERED INDEX [IX_ELMAH_Error_App_Time_Seq] ON [dbo].[TSPIDS_ELMAH_Error]
(
	[Application] ASC,
	[TimeUtc] DESC,
	[Sequence] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TSPIDS_PidsLog]    Script Date: 6/08/2015 1:19:45 PM ******/
CREATE NONCLUSTERED INDEX [IX_TSPIDS_PidsLog] ON [dbo].[TSPIDS_PidsLog]
(
	[Date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TSPIDS_PID]  WITH CHECK ADD  CONSTRAINT [FK_Configurations_Directions] FOREIGN KEY([DirectionsId])
REFERENCES [dbo].[TSPIDS_Directions] ([Id])
GO
ALTER TABLE [dbo].[TSPIDS_PID] CHECK CONSTRAINT [FK_Configurations_Directions]
GO
ALTER TABLE [dbo].[TSPIDS_PID]  WITH CHECK ADD  CONSTRAINT [FK_Configurations_DisplayOrder] FOREIGN KEY([DisplayOrderId])
REFERENCES [dbo].[TSPIDS_DisplayOrder] ([Id])
GO
ALTER TABLE [dbo].[TSPIDS_PID] CHECK CONSTRAINT [FK_Configurations_DisplayOrder]
GO
ALTER TABLE [dbo].[TSPIDS_Stops]  WITH CHECK ADD  CONSTRAINT [FK_Stops_Configurations] FOREIGN KEY([ConfigurationId])
REFERENCES [dbo].[TSPIDS_PID] ([Id])
GO
ALTER TABLE [dbo].[TSPIDS_Stops] CHECK CONSTRAINT [FK_Stops_Configurations]
GO
ALTER TABLE [dbo].[TSPIDS_Stops]  WITH CHECK ADD  CONSTRAINT [FK_TSPIDS_Stops_TSPIDS_Directions] FOREIGN KEY([Direction])
REFERENCES [dbo].[TSPIDS_Directions] ([Id])
GO
ALTER TABLE [dbo].[TSPIDS_Stops] CHECK CONSTRAINT [FK_TSPIDS_Stops_TSPIDS_Directions]
GO