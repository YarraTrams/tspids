DELETE FROM [dbo].[TSPIDS_UserGroups]
GO
DELETE FROM [dbo].[TSPIDS_GlobalSettings]
GO
DELETE FROM [dbo].[TSPIDS_ConfigurationsHistory]
GO
DELETE FROM [dbo].[TSPIDS_Stops]
GO
DELETE FROM [dbo].[TSPIDS_PID]
GO
DELETE FROM [dbo].[TSPIDS_DisplayOrder]
GO
DELETE FROM [dbo].[TSPIDS_Directions]
GO
SET IDENTITY_INSERT [dbo].[TSPIDS_Directions] ON 

GO
INSERT [dbo].[TSPIDS_Directions] ([Id], [Direction], [ImageName]) VALUES (1, N'North', N'north.png')
GO
INSERT [dbo].[TSPIDS_Directions] ([Id], [Direction], [ImageName]) VALUES (2, N'South', N'south.png')
GO
INSERT [dbo].[TSPIDS_Directions] ([Id], [Direction], [ImageName]) VALUES (3, N'East', N'east.png')
GO
INSERT [dbo].[TSPIDS_Directions] ([Id], [Direction], [ImageName]) VALUES (4, N'West', N'west.png')
GO
INSERT [dbo].[TSPIDS_Directions] ([Id], [Direction], [ImageName]) VALUES (5, N'North-east', N'ne.png')
GO
INSERT [dbo].[TSPIDS_Directions] ([Id], [Direction], [ImageName]) VALUES (6, N'North-west', N'nw.png')
GO
INSERT [dbo].[TSPIDS_Directions] ([Id], [Direction], [ImageName]) VALUES (7, N'South-east', N'se.png')
GO
INSERT [dbo].[TSPIDS_Directions] ([Id], [Direction], [ImageName]) VALUES (8, N'South-west', N'sw.png')
GO
SET IDENTITY_INSERT [dbo].[TSPIDS_Directions] OFF
GO
SET IDENTITY_INSERT [dbo].[TSPIDS_DisplayOrder] ON 

GO
INSERT [dbo].[TSPIDS_DisplayOrder] ([Id], [Name]) VALUES (1, N'Route Number in ascending order')
GO
INSERT [dbo].[TSPIDS_DisplayOrder] ([Id], [Name]) VALUES (2, N'Arrival Time in ascending order')
GO
SET IDENTITY_INSERT [dbo].[TSPIDS_DisplayOrder] OFF
GO
SET IDENTITY_INSERT [dbo].[TSPIDS_PID] ON 

GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (1, 2, N'Whitehorse Rd', N'Box Hill Central/Whitehorse Rd (Box Hill)', 3, 3, 250, 1, 1, CAST(N'2015-08-09 10:05:01.040' AS DateTime), 1, 1854, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=1')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (2, 2, N'Whitehorse Rd', N'Box Hill Central/Whitehorse Rd (Box Hill)', 3, 4, 250, 1, 1, CAST(N'2015-08-08 15:46:47.977' AS DateTime), 1, 3270, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=2')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (3, 1, N'Derby Rd', N'Caulfield Railway Station/Derby Rd (Caulfield East)', 3, 3, 100, 1, 1, CAST(N'2015-08-08 15:47:07.953' AS DateTime), 1, 9110, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=3')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (4, 1, N'Derby Rd', N'Caulfield Railway Station/Derby Rd (Caulfield East)', 3, 4, 90, 1, 1, CAST(N'2015-08-08 15:48:19.693' AS DateTime), 1, 9023, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=4')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (5, 1, N'Mount Alexander Rd', N'Fletcher St/Napier St (Essendon)', 3, 3, 190, 1, 1, CAST(N'2015-08-08 15:56:57.437' AS DateTime), 1, 7386, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=5')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (6, 1, N'William St', N'Flagstaff Railway Station/William St (Melbourne City)', 3, 1, 40, 1, 1, CAST(N'2015-07-08 09:50:50.000' AS DateTime), 1, 34, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=6')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (7, 1, N'La Trobe St', N'Flagstaff Railway Station/William St (Melbourne City)', 3, 1, 40, 1, 1, CAST(N'2015-08-06 14:58:59.000' AS DateTime), 1, 628, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=7')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (8, 1, N'St Kilda Rd', N'Flinders Street Railway Station/Swanston St (Melbourne City)', 3, 1, 20, 1, 1, CAST(N'2015-08-07 15:23:44.380' AS DateTime), 1, 103, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=8')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (9, 1, N'Flinders St', N'Swanston St/Flinders St (Melbourne City)', 3, 4, 40, 1, 1, CAST(N'2015-06-08 15:00:25.000' AS DateTime), 1, 73, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=9')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (10, 1, N'Elizabeth St', N'Flinders Street Railway Station/Elizabeth St (Melbourne City)', 3, 1, 20, 1, 1, CAST(N'2015-08-08 15:59:59.590' AS DateTime), 1, 384, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=10')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (11, 1, N'Flinders St', N'Elizabeth St/ Flinders St Intersections', 3, 1, 20, 1, 1, CAST(N'2015-08-07 14:39:22.267' AS DateTime), 1, 132, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=11')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (12, 1, N'Flinders St', N'Elizabeth St/Flinders St (Melbourne City - clock exit)', 3, 4, 20, 1, 1, CAST(N'2015-08-07 10:08:43.027' AS DateTime), 1, 104, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=12')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (13, 1, N'Leeds St', N'Footscray/Leeds St (Footscray)', 3, 1, 50, 1, 1, CAST(N'2015-08-06 15:05:56.927' AS DateTime), 1, 32, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=13')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (14, 1, N'Glenferrie Rd', N'Damien''s home stop', 3, 4, 80, 1, 1, CAST(N'2015-08-07 15:10:50.287' AS DateTime), 1, 42, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=14')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (15, 2, N'Swanston St', N'La Trobe St/Swanston St (Melbourne City)', 3, 1, 200, 1, 1, CAST(N'2015-08-06 15:06:44.787' AS DateTime), 1, 86, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=15')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (16, 1, N'La Trobe St', N'La Trobe St/Swanston St (Melbourne City)', 3, 1, 200, 1, 1, CAST(N'2015-08-06 15:07:07.510' AS DateTime), 1, 65, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=16')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (17, 1, N'Elizabeth St', N'Elizabeth St/La Trobe St (Melbourne City)', 3, 4, 50, 1, 1, CAST(N'2015-08-06 15:07:24.003' AS DateTime), 1, 40, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=17')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (18, 1, N'Nicholson St', N'Albert St/Nicholson St (Fitzroy)', 3, 5, 60, 1, 1, CAST(N'2015-08-07 15:14:36.093' AS DateTime), 1, 37, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=18')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (19, 1, N'Collins St', N'Parliament/Collins St (Melbourne City)', 3, 4, 170, 1, 1, CAST(N'2015-08-07 10:22:26.143' AS DateTime), 1, 531, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=19')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (20, 1, N'Bourke St', N'Parliament/Collins St (Melbourne City)', 3, 3, 120, 1, 1, CAST(N'2015-08-06 15:09:35.423' AS DateTime), 1, 88, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=20')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (21, 1, N'Macarthur St', N'Parliament Railway Station/Macarthur St (East Melbourne)', 3, 6, 100, 1, 1, CAST(N'2015-08-06 16:18:15.483' AS DateTime), 1, 75, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=21')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (22, 1, N'Chapel St', N'Toorak Rd/Chapel St (South Yarra)', 3, 4, 10, 1, 1, CAST(N'2015-08-07 10:49:44.233' AS DateTime), 1, 398, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=22')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (23, 2, N'Collins St', N'Spencer St/Collins St (Melbourne City)', 3, 1, 60, 1, 1, CAST(N'2015-08-09 10:10:41.637' AS DateTime), 1, 3930, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=23')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (24, 2, N'Spencer St', N'Spencer St/Collins St (Melbourne City)', 3, 1, 50, 1, 1, CAST(N'2015-08-08 16:00:33.107' AS DateTime), 1, 3367, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=24')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (25, 1, N'Bourke St', N'Spencer St/Bourke St (Melbourne City)', 3, 1, 50, 1, 1, CAST(N'2015-08-07 15:24:08.763' AS DateTime), 1, 37, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=25')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (26, 1, N'Collins St', N'Batmans Hill/Collins St (Docklands)', 3, 1, 20, 1, 1, CAST(N'2015-08-06 15:21:48.550' AS DateTime), 1, 34, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=26')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (27, 1, N'Carlisle St', N'Nelson St/Carlisle St (Balaclava)', 3, 1, 50, 1, 1, CAST(N'2015-08-09 10:03:24.910' AS DateTime), 1, 1129, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=27')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (28, 1, N'Burke Rd', N'Camberwell Station/Burke Rd (Hawthorn East)', 3, 4, 160, 1, 1, CAST(N'2015-08-06 15:22:42.393' AS DateTime), 1, 33, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=28')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (29, 2, N'Queens Pde', N'Clifton Hill Interchange/Queens Pde (Clifton Hill)', 3, 6, 450, 1, 1, CAST(N'2015-08-07 16:00:26.573' AS DateTime), 1, 37, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=29')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (30, 2, N'Queens Pde', N'Clifton Hill Interchange/Queens Pde (Clifton Hill)', 3, 4, 470, 1, 1, CAST(N'2015-08-06 15:23:24.447' AS DateTime), 1, 32, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=30')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (31, 1, N'Glenhuntly Rd', N'Elsternwick Railway Station/Glenhuntly Rd (Elsternwick)', 3, 1, 10, 1, 1, CAST(N'2015-08-06 15:23:41.923' AS DateTime), 1, 107, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=31')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (32, 2, N'Glenhuntly Rd', N'Glenhuntly Railway Station/Glenhuntly Rd (Glen Huntly)', 3, 4, 20, 1, 1, CAST(N'2015-08-06 15:23:55.113' AS DateTime), 1, 41, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=32')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (33, 1, N'Burwood Rd', N'Hawthorn Railway Station/Burwood Rd (Hawthorn)', 3, 3, 150, 1, 1, CAST(N'2015-08-06 15:24:09.697' AS DateTime), 1, 40, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=33')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (34, 1, N'Wellington Pde', N'Powlett St/Wellington Pde (East Melbourne)', 3, 4, 40, 1, 1, CAST(N'2015-08-06 15:24:34.303' AS DateTime), 1, 70, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=34')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (35, 1, N'Wellington Pde', N'Clarendon St/Wellington Pde (East Melbourne)', 3, 3, 120, 1, 1, CAST(N'2015-08-06 15:24:37.620' AS DateTime), 1, 36, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=35')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (36, 1, N'Glenferrie Rd', N'Kooyong Railway Station/Glenferrie Rd (Kooyong)', 3, 4, 40, 1, 1, CAST(N'2015-08-06 15:24:52.623' AS DateTime), 1, 35, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=36')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (37, 2, N'Glenferrie Rd', N'Dandenong Rd/Glenferrie Rd (Malvern)', 3, 3, 140, 1, 1, CAST(N'2015-08-07 10:58:33.083' AS DateTime), 1, 232, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=37')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (38, 1, N'Racecourse Rd', N'Newmarket Plaza/Racecourse Rd (Flemington)', 3, 4, 160, 1, 1, CAST(N'2015-08-06 15:26:11.500' AS DateTime), 1, 34, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=38')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (39, 1, N'Victoria St', N'North Richmond Railway Station/Victoria St (Richmond)', 3, 1, 40, 1, 1, CAST(N'2015-08-07 15:24:33.563' AS DateTime), 1, 131, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=39')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (40, 1, N'Victoria St', N'North Richmond Railway Station/Victoria St (Richmond)', 3, 1, 40, 1, 1, CAST(N'2015-08-07 15:25:10.123' AS DateTime), 1, 41, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=40')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (41, 1, N'High St', N'Separation St/High St (Northcote)', 3, 4, 410, 1, 1, CAST(N'2015-08-06 15:27:25.897' AS DateTime), 1, 39, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=41')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (42, 1, N'High St', N'Arthurton Rd/St Georges Rd (Northcote)', 3, 3, 410, 1, 1, CAST(N'2015-08-06 15:27:46.990' AS DateTime), 1, 865, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=42')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (43, 2, N'Swan St', N'Cremorne St/Swan St (Richmond)', 3, 1, 55, 1, 1, CAST(N'2015-08-09 10:16:50.787' AS DateTime), 1, 76, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=43')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (44, 1, N'Dandenong Rd', N'Chapel St/Dandenong Rd (Windsor)', 3, 1, 50, 1, 1, CAST(N'2015-08-06 15:31:19.437' AS DateTime), 1, 102, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=44')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (45, 1, N'Chapel St', N'Windsor Railway Station/Chapel St (Windsor)', 3, 3, 50, 1, 1, CAST(N'2015-08-06 15:31:43.623' AS DateTime), 1, 181, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=45')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (46, 1, N'Sydney Rd', N'Albion St/Sydney Rd (Brunswick)', 3, 4, 260, 1, 1, CAST(N'2015-08-06 15:31:53.750' AS DateTime), 1, 44, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=46')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (47, 2, N'High St', N'Armadale Station/High St (Armadale)', 3, 3, 140, 1, 1, CAST(N'2015-08-06 15:32:04.543' AS DateTime), 1, 51, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=47')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (48, 1, N'Sydney Rd', N'Gaffney St/Sydney Rd (Coburg North)', 3, 3, 350, 1, 1, CAST(N'2015-08-06 15:32:52.590' AS DateTime), 1, 3775, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=48')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (49, 1, N'Sydney Rd', N'Albert St/Sydney Rd (Brunswick)', 3, 3, 300, 1, 1, CAST(N'2015-08-06 15:32:53.987' AS DateTime), 1, 758, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=49')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (50, 1, N'Swan St', N'Burnley St/Swan St (Burnley)', 3, 4, 100, 1, 1, CAST(N'2015-08-06 15:33:06.987' AS DateTime), 1, 518, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=50')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (51, 1, N'Swan St', N'Burnley St/Swan St (Burnley)', 3, 4, 100, 1, 1, CAST(N'2015-08-06 15:33:18.900' AS DateTime), 1, 839, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=51')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (52, 1, N'Sydney Rd', N'Coburg Market/Sydney Rd (Coburg)', 3, 3, 390, 1, 1, CAST(N'2015-08-06 15:33:29.437' AS DateTime), 1, 1449, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=52')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (53, 1, N'High St', N'Dennis St/High St (Northcote)', 3, 3, 230, 1, 1, CAST(N'2015-08-06 15:34:28.033' AS DateTime), 1, 177, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=53')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (54, 1, N'Swan St', N'Church St/Swan St (Richmond)', 3, 5, 100, 1, 1, CAST(N'2015-08-06 15:35:01.977' AS DateTime), 1, 551, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=54')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (55, 1, N'Swan St', N'East Richmond Railway Station/Church St (Richmond)', 3, 4, 100, 1, 1, CAST(N'2015-08-06 17:34:22.783' AS DateTime), 1, 469, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=55')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (56, 1, N'Gardiner Station', N'Malvern Rd/Burke Rd (Glen Iris)', 3, 1, 0, 1, 1, CAST(N'2015-08-06 15:36:40.903' AS DateTime), 1, 71, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=56')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (57, 1, N'High St', N'Glen Iris/High St (Glen Iris)', 3, 5, 160, 1, 1, CAST(N'2015-08-06 15:37:31.263' AS DateTime), 1, 62, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=57')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (58, 1, N'Sydney Rd', N'Barkly Square/Sydney Rd (Brunswick)', 3, 3, 220, 1, 1, CAST(N'2015-08-06 15:37:52.560' AS DateTime), 1, 34, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=58')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (59, 1, N'Sydney Rd', N'Moreland Rd/Sydney Rd (Coburg)', 3, 3, 100, 1, 1, CAST(N'2015-08-07 15:25:41.050' AS DateTime), 1, 80, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=59')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (60, 1, N'High St', N'Latrobe St/High St (Windsor)', 3, 3, 150, 1, 1, CAST(N'2015-08-06 15:38:42.857' AS DateTime), 1, 41, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=60')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (61, 1, N'Brighton Rd', N'Glen Eira Rd/Brighton Rd (Elwood)', 3, 3, 400, 1, 1, CAST(N'2015-08-06 15:38:57.093' AS DateTime), 1, 37, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=61')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (62, 1, N'Riversdale Rd', N'Spencer Rd/Riversdale Rd (Camberwell)', 3, 3, 170, 1, 1, CAST(N'2015-08-06 15:39:11.307' AS DateTime), 1, 108, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=62')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (63, 1, N'Royal Park', N'Royal Park Railway Station/Royal Park (Parkville)', 3, 3, 150, 1, 1, CAST(N'2015-08-06 15:39:24.327' AS DateTime), 1, 38, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=63')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (64, 1, N'St Georges Rd', N'Hutton St/St Georges Rd (Thornbury)', 3, 3, 160, 1, 1, CAST(N'2015-08-07 17:28:56.060' AS DateTime), 1, 53, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=64')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (65, 2, N'Malvern Rd', N'Orrong Rd/Malvern Rd (Toorak)', 3, 4, 300, 1, 1, CAST(N'2015-08-06 15:40:32.693' AS DateTime), 1, 65, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=65')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (66, 1, N'High St', N'Westgarth St/High St (Northcote)', 3, 4, 270, 1, 1, CAST(N'2015-08-06 15:40:42.777' AS DateTime), 1, 564, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=66')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (67, 1, N'Application team ', N'112 Eastern Road, South Melbourne', 3, 1, 1000, 1, 1, CAST(N'2015-08-06 15:42:13.563' AS DateTime), 1, 48, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=67')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (68, 1, N'St Kilda Rd', N'Flinders Street Railway Station/Swanston St (Melbourne City) - Fed Square Exit - Princes Bridge', 3, 1, 20, 1, 1, CAST(N'2015-06-08 15:05:27.000' AS DateTime), 1, 91, NULL, 2, 1, N'http://dev-ttr-app-01/WebPids.Web.Api/api/PidsData/GetPIDsData/?pidId=68')
GO
INSERT [dbo].[TSPIDS_PID] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId], [ConfigurationURL]) VALUES (69, 1, N'damien''s PID', NULL, 3, 3, 1000, 1, 1, CAST(N'2015-08-07 14:39:51.957' AS DateTime), 0, 53, NULL, 2, 1, NULL)
GO
SET IDENTITY_INSERT [dbo].[TSPIDS_PID] OFF
GO
SET IDENTITY_INSERT [dbo].[TSPIDS_Stops] ON 

GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2829, 1, 1758, N'On Whitehorse Rd', 1, 250)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2830, 2, 1758, N'On Whitehorse Rd', 4, 250)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2831, 3, 1089, N'On Derby Rd', 3, 100)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2832, 3, 2089, N'On Derby Rd', 3, 100)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2833, 4, 1089, N'On Derby Rd', 4, 90)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2834, 4, 2089, N'On Derby Rd', 4, 90)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2835, 5, 1302, N'On Mount Alexander Rd', 3, 190)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2836, 5, 2302, N'On Mount Alexander Rd', 3, 190)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2837, 6, 3064, N'On William St', 1, 40)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2838, 6, 3166, N'On WilliamSt', 1, 40)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2841, 8, 3113, N'On St Kilda Rd', 1, 20)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2842, 9, 3605, N'On Flinders St', 4, 40)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2843, 9, 3705, N'On Flinders St', 4, 40)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2844, 10, 3801, N'On Elizabeth St', 1, 20)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2845, 12, 3604, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2846, 12, 3704, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2847, 13, 4364, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2848, 14, 1474, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2849, 14, 2474, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2850, 15, 3008, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2851, 15, 3108, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2856, 18, 3210, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2857, 18, 3310, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2858, 19, 3409, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2859, 19, 3509, NULL, 1, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2862, 21, 3410, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2863, 21, 3510, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2866, 23, 3401, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2867, 23, 3501, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2870, 25, 3201, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2871, 25, 3301, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2872, 26, 3400, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2873, 26, 3500, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2874, 27, 1073, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2875, 27, 2073, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2876, 28, 1664, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2877, 28, 2664, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2878, 29, 1825, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2879, 29, 2825, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2880, 30, 1825, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2881, 30, 2825, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2882, 31, 1044, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2883, 31, 2044, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2884, 32, 1061, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2885, 32, 2060, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2886, 33, 1926, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2887, 33, 2926, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2888, 34, 3612, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2889, 34, 3712, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2890, 35, 3611, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2891, 35, 3711, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2892, 36, 1465, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2893, 36, 2465, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2896, 38, 1326, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2897, 38, 2326, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2898, 39, 1719, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2899, 39, 2719, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2900, 40, 1719, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2901, 40, 2719, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2902, 41, 1369, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2903, 41, 2369, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2904, 42, 1369, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2905, 42, 2369, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2906, 43, 1761, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2907, 43, 2761, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2908, 44, 1102, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2909, 44, 2102, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2910, 45, 4043, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2911, 45, 4143, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2912, 46, 1426, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2913, 46, 2426, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2914, 47, 1162, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2915, 47, 2162, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2916, 48, 1437, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2917, 48, 2437, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2918, 49, 1422, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2919, 49, 2422, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2920, 50, 1767, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2921, 50, 2767, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2922, 51, 1767, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2923, 51, 2767, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2924, 52, 1433, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2925, 52, 2433, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2926, 53, 1371, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2927, 53, 2371, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2928, 54, 1764, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2929, 54, 2764, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2930, 55, 1764, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2931, 56, 1651, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2932, 56, 2651, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2933, 57, 1175, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2934, 58, 1420, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2935, 58, 2420, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2938, 60, 1152, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2939, 60, 2152, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2940, 61, 1039, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2941, 61, 2039, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2942, 62, 1778, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2943, 62, 2778, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2944, 63, 1256, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2945, 63, 2256, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2946, 64, 1373, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2947, 64, 2373, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2948, 65, 1637, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2949, 65, 2637, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2950, 66, 1827, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2951, 66, 2827, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2958, 8, 3013, N'On St Kilda Rd', 1, 20)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2959, 11, 3605, N'On Flinders St', 1, 20)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2960, 11, 3705, N'On Flinders  St', 1, 20)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2961, 11, 3801, N'On Elizabeth St', 1, 20)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2964, 17, 3805, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2965, 17, 3905, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2966, 20, 3309, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2967, 20, 3209, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2968, 22, 4050, N'On Chapel St', 4, 10)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2969, 22, 4150, N'On Chapel St', 4, 10)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2970, 67, 1232, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2977, 53, 1835, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2978, 53, 2835, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2979, 54, 4156, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2980, 54, 4057, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2981, 55, 2764, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2982, 55, 4057, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2983, 55, 4156, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2984, 64, 1840, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2985, 64, 2840, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2986, 67, 2024, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2991, 41, 1833, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2992, 41, 2833, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2993, 42, 1833, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2994, 42, 2833, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2996, 68, 3013, N'On St Kilda Rd', 1, 20)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (2997, 68, 3113, N'On St Kilda Rd', 1, 20)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (3000, 24, 3254, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (3001, 24, 3354, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (3002, 7, 3553, N'On La Trobe St', 1, 40)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (3003, 7, 3653, N'On La Trobe St', 1, 40)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (3004, 59, 1428, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (3005, 59, 2428, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (3006, 59, 1551, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (3009, 16, 3556, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (3010, 16, 3656, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (13033, 22, 1565, N'On Toorak Rd', 4, 10)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (13034, 22, 2565, N'On Toorak Rd', 4, 10)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (13035, 37, 1453, N'On Glenferrie Rd', 3, 140)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (13036, 37, 2453, N'On Glenferrie Rd', 3, 140)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (13037, 37, 1140, N'On Wattletree Rd', 3, 140)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (13038, 37, 2140, N'On Wattletree Rd', 3, 140)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (13039, 37, 1976, N'On Dandenong Rd', 3, 140)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (13040, 37, 2115, N'On Dandenong Rd', 3, 140)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (13041, 14, 1232, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (13042, 69, 1232, NULL, NULL, NULL)
GO
INSERT [dbo].[TSPIDS_Stops] ([Id], [ConfigurationId], [TrackerId], [DisplayTitle], [Direction], [Distance]) VALUES (13043, 69, 2232, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[TSPIDS_Stops] OFF
GO
SET IDENTITY_INSERT [dbo].[TSPIDS_ConfigurationsHistory] ON 

GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (1, 26, N'Pid 26 - Collins St updated by ROBBIE\Rob', N'Rob', CAST(N'2015-08-03 08:23:43.097' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (2, 1, N'Stop 2829 - 1758 updated for Pid 1 - Whitehorse Rd by ROBBIE\Rob', N'Rob', CAST(N'2015-08-05 12:25:38.897' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (3, 1, N'Stop 2829 - 1758 updated for Pid 1 - Whitehorse Rd by ROBBIE\Rob', N'Rob', CAST(N'2015-08-05 12:25:45.693' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (4, 1, N'Stop 2829 - 1758 updated for Pid 1 - Whitehorse Rd by ROBBIE\Rob', N'Rob', CAST(N'2015-08-05 12:25:52.923' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (5, 1, N'Stop 2829 - 1758 updated for Pid 1 - Whitehorse Rd by ROBBIE\Rob', N'Rob', CAST(N'2015-08-05 12:34:40.500' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (6, 1, N'Stop 2829 - 1758 updated for Pid 1 - Whitehorse Rd by ROBBIE\Rob', N'Rob', CAST(N'2015-08-05 12:40:13.040' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (7, 1, N'Stop 2829 - 1758 updated for Pid 1 - Whitehorse Rd by ROBBIE\Rob', N'Rob', CAST(N'2015-08-05 12:40:23.037' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (8, 1, N'Stop 2829 - 1758 updated for Pid 1 - Whitehorse Rd by ROBBIE\Rob', N'Rob', CAST(N'2015-08-05 12:40:27.850' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (9, 1, N'Stop 2829 - 1758 updated for Pid 1 - Whitehorse Rd by ROBBIE\Rob', N'Rob', CAST(N'2015-08-05 12:40:36.060' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (10, 11, N'Stop 2961 - 3801 updated for Pid 11 - Flinders St by ROBBIE\Rob', N'Rob', CAST(N'2015-08-05 12:58:20.327' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (11, 37, N'Stop 13029 - 1453 updated for Pid 37 - Glenferrie Rd by ROBBIE\Rob', N'Rob', CAST(N'2015-08-06 09:12:18.307' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (12, 37, N'Stop 13031 - 1976 updated for Pid 37 - Glenferrie Rd by ROBBIE\Rob', N'Rob', CAST(N'2015-08-06 09:12:33.447' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (13, 7, N'Pid 7 - La Trobe St updated by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 08:55:40.717' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (14, 11, N'Stop 2960 - 3704 updated for Pid 11 - Flinders St by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 08:56:56.910' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (15, 11, N'Stop 2960 - 3704 updated for Pid 11 - Flinders St by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 08:57:22.997' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (16, 11, N'Stop 2959 - 3604 updated for Pid 11 - Flinders St by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 08:57:52.907' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (17, 11, N'Pid 11 - Flinders St updated by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 08:57:55.867' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (18, 11, N'Stop 2959 - 3604 updated for Pid 11 - Flinders St by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 08:59:16.903' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (19, 19, N'Stop 2859 - 3509 updated for Pid 19 - Collins St by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:18:21.907' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (20, 19, N'Pid 19 - Collins St updated by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:18:28.947' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (21, 11, N'Stop 2959 - 3605 updated for Pid 11 - Flinders St by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:21:59.273' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (22, 11, N'Stop 2960 - 3705 updated for Pid 11 - Flinders St by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:22:21.910' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (23, 11, N'Pid 11 - Flinders St updated by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:22:24.577' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (24, 11, N'Stop 2959 - 3605 updated for Pid 11 - Flinders St by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:24:08.377' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (25, 11, N'Stop 2960 - 3705 updated for Pid 11 - Flinders St by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:24:23.107' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (26, 11, N'Stop 2961 - 3801 updated for Pid 11 - Flinders St by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:24:41.497' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (27, 11, N'Pid 11 - Flinders St updated by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:24:43.807' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (28, 22, N'Stop 2864 - 1565 deleted for Pid 22 - Chapel St by TRAMCO\Rob.Clayton', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:29:41.697' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (29, 22, N'Stop 2865 - 2565 deleted for Pid 22 - Chapel St by TRAMCO\Rob.Clayton', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:29:45.433' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (30, 22, N'Stop 1565 created for Pid 22 - Chapel St by TRAMCO\Rob.Clayton', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:30:05.873' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (31, 22, N'Stop 2565 created for Pid 22 - Chapel St by TRAMCO\Rob.Clayton', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:30:16.577' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (32, 11, N'Stop 2961 - 3801 updated for Pid 11 - Flinders St by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:33:53.400' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (33, 11, N'Stop 2959 - 3605 updated for Pid 11 - Flinders St by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:34:04.957' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (34, 11, N'Pid 11 - Flinders St updated by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:34:11.253' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (35, 11, N'Stop 2959 - 3605 updated for Pid 11 - Flinders St by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:34:51.940' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (36, 11, N'Stop 2960 - 3705 updated for Pid 11 - Flinders St by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:35:02.930' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (37, 11, N'Stop 2961 - 3801 updated for Pid 11 - Flinders St by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:35:12.887' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (38, 11, N'Pid 11 - Flinders St updated by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:35:15.153' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (39, 1, N'Stop 2829 - 1758 updated for Pid 1 - Whitehorse Rd by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:36:37.837' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (40, 22, N'Stop 2968 - 4050 updated for Pid 22 - Chapel St by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:38:22.890' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (41, 22, N'Stop 2969 - 4150 updated for Pid 22 - Chapel St by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:38:41.033' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (42, 22, N'Stop 13033 - 1565 updated for Pid 22 - Chapel St by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:39:10.967' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (43, 22, N'Stop 13034 - 2565 updated for Pid 22 - Chapel St by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:39:32.570' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (44, 22, N'Pid 22 - Chapel St updated by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:39:34.803' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (45, 22, N'Stop 13033 - 1565 updated for Pid 22 - Chapel St by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:40:36.677' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (46, 22, N'Stop 13034 - 2565 updated for Pid 22 - Chapel St by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:40:47.267' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (47, 22, N'Pid 22 - Chapel St updated by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:40:49.723' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (48, 37, N'Stop 13027 - 1140 deleted for Pid 37 - Glenferrie Rd by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:42:05.847' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (49, 37, N'Stop 13028 - 2140 deleted for Pid 37 - Glenferrie Rd by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:42:09.407' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (50, 37, N'Stop 13029 - 1453 deleted for Pid 37 - Glenferrie Rd by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:42:12.347' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (51, 37, N'Stop 13030 - 2453 deleted for Pid 37 - Glenferrie Rd by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:42:14.810' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (52, 37, N'Stop 13031 - 1976 deleted for Pid 37 - Glenferrie Rd by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:42:17.293' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (53, 37, N'Stop 13032 - 2115 deleted for Pid 37 - Glenferrie Rd by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:42:24.917' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (54, 37, N'Stop 1453 created for Pid 37 - Glenferrie Rd by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:44:12.673' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (55, 37, N'Stop 13035 - 1453 updated for Pid 37 - Glenferrie Rd by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:44:41.220' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (56, 37, N'Stop 2453 created for Pid 37 - Glenferrie Rd by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:45:13.920' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (57, 37, N'Stop 13036 - 2453 updated for Pid 37 - Glenferrie Rd by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:45:24.077' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (58, 37, N'Stop 1140 created for Pid 37 - Glenferrie Rd by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:45:37.667' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (59, 37, N'Stop 2140 created for Pid 37 - Glenferrie Rd by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:45:47.540' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (60, 37, N'Stop 1976 created for Pid 37 - Glenferrie Rd by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:46:00.540' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (61, 37, N'Stop 2115 created for Pid 37 - Glenferrie Rd by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:46:08.160' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (62, 37, N'Stop 13037 - 1140 updated for Pid 37 - Glenferrie Rd by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:46:38.047' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (63, 37, N'Stop 13038 - 2140 updated for Pid 37 - Glenferrie Rd by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:47:01.503' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (64, 37, N'Stop 13039 - 1976 updated for Pid 37 - Glenferrie Rd by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:47:29.850' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (65, 37, N'Stop 13040 - 2115 updated for Pid 37 - Glenferrie Rd by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:47:56.040' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (66, 37, N'Pid 37 - Glenferrie Rd updated by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 10:48:07.287' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (67, 2, N'Stop 2830 - 1758 updated for Pid 2 - Whitehorse Rd by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 11:02:37.390' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (68, 3, N'Stop 2831 - 1089 updated for Pid 3 - Derby Rd by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 11:04:14.423' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (69, 3, N'Stop 2832 - 2089 updated for Pid 3 - Derby Rd by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 11:04:31.847' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (70, 3, N'Pid 3 - Derby Rd updated by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 11:04:33.287' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (71, 4, N'Stop 2833 - 1089 updated for Pid 4 - Derby Rd by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 11:05:05.653' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (72, 4, N'Stop 2834 - 2089 updated for Pid 4 - Derby Rd by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 11:05:22.277' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (73, 4, N'Pid 4 - Derby Rd updated by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 11:05:24.097' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (74, 5, N'Stop 2835 - 1302 updated for Pid 5 - Mount Alexander Rd by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 11:06:06.350' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (75, 5, N'Stop 2836 - 2302 updated for Pid 5 - Mount Alexander Rd by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 11:06:25.913' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (76, 5, N'Pid 5 - Mount Alexander Rd updated by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 11:06:29.267' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (77, 6, N'Stop 2837 - 3064 updated for Pid 6 - William St by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 11:06:59.417' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (78, 6, N'Stop 2838 - 3166 updated for Pid 6 - William St by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 11:07:12.553' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (79, 6, N'Pid 6 - William St updated by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 11:07:15.107' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (80, 7, N'Stop 3002 - 3553 updated for Pid 7 - La Trobe St by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 11:07:49.033' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (81, 7, N'Stop 3003 - 3653 updated for Pid 7 - La Trobe St by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 11:08:01.430' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (82, 7, N'Pid 7 - La Trobe St updated by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 11:08:04.040' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (83, 8, N'Stop 2841 - 3113 updated for Pid 8 - St Kilda Rd by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 11:08:26.410' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (84, 8, N'Stop 2958 - 3013 updated for Pid 8 - St Kilda Rd by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 11:08:44.823' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (85, 8, N'Pid 8 - St Kilda Rd updated by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 11:08:46.907' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (86, 9, N'Stop 2842 - 3605 updated for Pid 9 - Flinders St by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 11:09:16.723' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (87, 9, N'Stop 2843 - 3705 updated for Pid 9 - Flinders St by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 11:09:43.103' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (88, 9, N'Pid 9 - Flinders St updated by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 11:09:45.050' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (89, 68, N'Stop 2996 - 3013 updated for Pid 68 - St Kilda Rd by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 11:10:30.690' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (90, 68, N'Stop 2997 - 3113 updated for Pid 68 - St Kilda Rd by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 11:10:46.047' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (91, 68, N'Pid 68 - St Kilda Rd updated by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 11:10:49.203' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (92, 10, N'Stop 2844 - 3801 updated for Pid 10 - Elizabeth St by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 11:11:36.030' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (93, 10, N'Pid 10 - Elizabeth St updated by TRAMCO\Diana.Barnes', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 11:11:38.957' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (94, 14, N'Stop 1232 created for Pid 14 - Glenferrie Rd by TRAMCO\Rob.Clayton', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 11:42:24.117' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (95, 14, N'Pid 14 - Glenferrie Rd updated by TRAMCO\Rob.Clayton', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 11:43:27.863' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (96, 69, N'Pid 69 - damien''s PID created by TRAMCO\Rob.Clayton', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 11:48:02.490' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (97, 69, N'Stop 1232 created for Pid 69 - damien''s PID by TRAMCO\Rob.Clayton', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 11:48:17.160' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (98, 69, N'Stop 2232 created for Pid 69 - damien''s PID by TRAMCO\Rob.Clayton', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 11:48:23.137' AS DateTime))
GO
INSERT [dbo].[TSPIDS_ConfigurationsHistory] ([Id], [ConfigurationsId], [Message], [UserName], [CreatedDate]) VALUES (99, 69, N'Pid 69 - damien''s PID updated by TRAMCO\Rob.Clayton', N'DEV-TTR-APP-01$', CAST(N'2015-08-07 11:48:31.553' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[TSPIDS_ConfigurationsHistory] OFF
GO
SET IDENTITY_INSERT [dbo].[TSPIDS_GlobalSettings] ON 

GO
INSERT [dbo].[TSPIDS_GlobalSettings] ([Id], [Name], [Value]) VALUES (1, N'GenericMessageEnabled', N'False')
GO
INSERT [dbo].[TSPIDS_GlobalSettings] ([Id], [Name], [Value]) VALUES (2, N'GenericMessage', N'1Real-time tram information on your mobile. Get tramTRACKER app for iPhone or Android. Follow @yarratrams on Twitter.')
GO
SET IDENTITY_INSERT [dbo].[TSPIDS_GlobalSettings] OFF
GO
SET IDENTITY_INSERT [dbo].[TSPIDS_UserGroups] ON 

GO
INSERT [dbo].[TSPIDS_UserGroups] ([Id], [Name], [Readonly], [SortOrder], [Enabled]) VALUES (2, N'SG-DEV-TramTracker Master Data -Readonly', 1, 1, 1)
GO
INSERT [dbo].[TSPIDS_UserGroups] ([Id], [Name], [Readonly], [SortOrder], [Enabled]) VALUES (4, N'SG-DEV-TramTracker Master Data -Update', 0, 2, 1)
GO
SET IDENTITY_INSERT [dbo].[TSPIDS_UserGroups] OFF
GO
