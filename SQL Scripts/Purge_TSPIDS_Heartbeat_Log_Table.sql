--
-- Delete all entries from the TSPID heardbeat log that are greater than six months old
--
--

delete from TSPIDS_PidsLog where [Date] < GETDATE() - (30*6)