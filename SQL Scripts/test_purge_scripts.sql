-- Test of purging of raised TSPID error entries


begin transaction

update TSPIDS_PidsLog set date = convert(datetime, '2015-06-01') where id > 2171
update TSPIDS_PidsLog set date = convert(datetime, '2015-01-01') where id > 4171

--select * from TSPIDS_PidsLog

-- Delete all entries from the TSPID heardbeat log that are greater than six months old
delete from TSPIDS_PidsLog where [Date] < GETDATE() - (30*6)

select * from TSPIDS_PidsLog

rollback


