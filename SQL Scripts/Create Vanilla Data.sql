USE [WebPID_ForUnitTesting]
GO
 

SET IDENTITY_INSERT [dbo].[Directions] ON 

GO
INSERT INTO [dbo].[Directions] ([Id], [Direction],[ImageName]) VALUES (1, 'North', 'north.png')
GO
INSERT INTO [dbo].[Directions] ([Id], [Direction],[ImageName]) VALUES (2, 'South', 'south.png')
GO
INSERT INTO [dbo].[Directions] ([Id], [Direction],[ImageName]) VALUES (3, 'East', 'east.png')
GO
INSERT INTO [dbo].[Directions] ([Id], [Direction],[ImageName]) VALUES (4, 'West', 'west.png')
GO
INSERT INTO [dbo].[Directions] ([Id], [Direction],[ImageName]) VALUES (5, 'North-east', 'ne.png')
GO
INSERT INTO [dbo].[Directions] ([Id], [Direction],[ImageName]) VALUES (6, 'North-west', 'nw.png')
GO
INSERT INTO [dbo].[Directions] ([Id], [Direction],[ImageName]) VALUES (7, 'South-east', 'se.png')
GO
INSERT INTO [dbo].[Directions] ([Id], [Direction],[ImageName]) VALUES (8, 'South-west', 'sw.png')
GO
SET IDENTITY_INSERT [dbo].[Directions] OFF 

GO

TRUNCATE TABLE [dbo].[GlobalSettings]
GO 

SET IDENTITY_INSERT [dbo].[GlobalSettings] ON 
GO 

INSERT [dbo].[GlobalSettings] ([Id], [Name], [Value]) 
VALUES (1, 'GenericMessageEnabled', 'True');
GO

INSERT [dbo].[GlobalSettings] ([Id], [Name], [Value]) 
VALUES (2, 'GenericMessage', 'Real-time tram information on your mobile. Get tramTRACKER app for iPhone or Android. Follow @yarratrams on Twitter.');
GO

INSERT [dbo].[GlobalSettings] ([Id], [Name], [Value]) 
VALUES (3, 'DefaultDisruptionMessage', 'Route {0} is currently disrupted and delays may occur');
GO

SET IDENTITY_INSERT [dbo].[GlobalSettings] OFF 
GO 

TRUNCATE TABLE [dbo].[DisplayOrder]
GO 

SET IDENTITY_INSERT [dbo].[DisplayOrder] ON 
GO 

INSERT [dbo].[DisplayOrder] ([Id], [Name]) 
VALUES (1, 'Route Number in ascending order');
GO

INSERT [dbo].[DisplayOrder] ([Id], [Name]) 
VALUES (2, 'Arrival Time in ascending order');
GO


SET IDENTITY_INSERT [dbo].[DisplayOrder] OFF 
GO 


SET IDENTITY_INSERT [dbo].[Configurations] ON 

GO
INSERT [dbo].[Configurations] ([Id], [PIDName], [Description], [NetworkName], [NumOfLines], [Enabled], [OrientationMode], [DirectionsId], [Distance], [ErrorStatus], [DisplayOrderId]) 
VALUES (1, 'Dorcas St', 'Eastern Rd - to and from city', 'localhost', 3, 1, 1,1, 50, 0, 1)
GO
SET IDENTITY_INSERT [dbo].[Configurations] OFF
GO
SET IDENTITY_INSERT [dbo].[Stops] ON 

GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (1, 1, 1232, 'Up')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2, 1, 2232, 'Dow')
GO
SET IDENTITY_INSERT [dbo].[Stops] OFF
GO
SET IDENTITY_INSERT [dbo].[UserGroups] ON 

GO
INSERT [dbo].[UserGroups] ([Id], [Name], [Readonly], [SortOrder], [Enabled]) VALUES (2, 'SG-DEV-TramTracker Master Data -Readonly', 1, 1, 1)
GO
INSERT [dbo].[UserGroups] ([Id], [Name], [Readonly], [SortOrder], [Enabled]) VALUES (4, 'SG-DEV-TramTracker Master Data -Update', 0, 2, 1)
GO
SET IDENTITY_INSERT [dbo].[UserGroups] OFF
GO
