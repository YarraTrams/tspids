--
-- Delete all entries from the TSPID elevated log that are greater than a year old
--
--

delete from TSPIDS_PidsElevatedLog where [Date] < GETDATE() - (30*12)
