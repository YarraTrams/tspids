--USE [WebPid_MB]
--GO

SET IDENTITY_INSERT [dbo].[Directions] ON 

GO
INSERT [dbo].[Directions] ([Id], [Direction], [ImageName]) VALUES (1, N'North', N'north.png')
GO
INSERT [dbo].[Directions] ([Id], [Direction], [ImageName]) VALUES (2, N'South', N'south.png')
GO
INSERT [dbo].[Directions] ([Id], [Direction], [ImageName]) VALUES (3, N'East', N'east.png')
GO
INSERT [dbo].[Directions] ([Id], [Direction], [ImageName]) VALUES (4, N'West', N'west.png')
GO
INSERT [dbo].[Directions] ([Id], [Direction], [ImageName]) VALUES (5, N'North-east', N'ne.png')
GO
INSERT [dbo].[Directions] ([Id], [Direction], [ImageName]) VALUES (6, N'North-west', N'nw.png')
GO
INSERT [dbo].[Directions] ([Id], [Direction], [ImageName]) VALUES (7, N'South-east', N'se.png')
GO
INSERT [dbo].[Directions] ([Id], [Direction], [ImageName]) VALUES (8, N'South-west', N'sw.png')
GO
SET IDENTITY_INSERT [dbo].[Directions] OFF
GO
SET IDENTITY_INSERT [dbo].[DisplayOrder] ON 

GO
INSERT [dbo].[DisplayOrder] ([Id], [Name]) VALUES (1, N'Route Number in ascending order')
GO
INSERT [dbo].[DisplayOrder] ([Id], [Name]) VALUES (2, N'Arrival Time in ascending order')
GO
SET IDENTITY_INSERT [dbo].[DisplayOrder] OFF
GO
SET IDENTITY_INSERT [dbo].[Configurations] ON 

GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (1, 1, N'Box Hill Station', N'Box Hill Central/Whitehorse Rd (Box Hill)', 3, 3, 300, 1, 1, CAST(N'2015-07-20 14:26:53.650' AS DateTime), 1, 11538748, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (2, 1, N'Box Hill Station', N'Box Hill Central/Whitehorse Rd (Box Hill)', 3, 3, 300, 1, 1, CAST(N'2015-07-20 14:26:53.677' AS DateTime), 1, 9121232, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (3, 1, N'Caulfield Station', N'Caulfield Railway Station/Derby Rd (Caulfield East)', 3, 1, 160, 1, 1, CAST(N'2015-07-20 14:26:54.780' AS DateTime), 1, 19137606, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (4, 1, N'Caulfield Station', N'Caulfield Railway Station/Derby Rd (Caulfield East)', 3, 1, 160, 1, 1, CAST(N'2015-07-20 14:26:54.747' AS DateTime), 1, 16702399, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (5, 1, N'Essendon Station', N'Fletcher St/Napier St (Essendon)', 3, 3, 190, 1, 1, CAST(N'2015-07-20 14:26:54.687' AS DateTime), 1, 15861973, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (6, 1, N'Flagstaff Station', N'Flagstaff Railway Station/William St (Melbourne City)', 3, 1, 1, 1, 1, CAST(N'2015-07-20 14:26:55.293' AS DateTime), 1, 16283336, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (7, 1, N'Flagstaff Station', N'Flagstaff Railway Station/William St (Melbourne City)', 3, 3, 1, 1, 1, CAST(N'2015-07-20 14:26:55.290' AS DateTime), 1, 16203237, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (8, 1, N'Flinders St Station', N'Flinders Street Railway Station/Swanston St (Melbourne City)', 3, 1, 1, 1, 1, CAST(N'2015-07-20 14:26:48.120' AS DateTime), 1, 292093474, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (9, 1, N'Flinders St Station', N'Swanston St/Flinders St (Melbourne City)', 3, 1, 1, 1, 1, CAST(N'2015-07-20 14:26:42.013' AS DateTime), 1, 230765117, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (10, 2, N'Flinders St Station', N'Flinders Street Railway Station/Elizabeth St (Melbourne City)', 3, 1, 1, 1, 1, CAST(N'2015-07-20 14:26:41.577' AS DateTime), 1, 226255376, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (11, 1, N'Flinders St Station', N'Elizabeth St/ Flinders St Intersections', 3, 1, 1, 1, 1, CAST(N'2015-07-21 14:48:02.897' AS DateTime), 1, 15687248, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (12, 2, N'Flinders St Station', N'Elizabeth St/Flinders St (Melbourne City - clock exit)', 3, 1, 1, 1, 1, CAST(N'2015-07-20 14:26:42.013' AS DateTime), 1, 230534082, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (13, 1, N'Footscray Station', N'Footscray/Leeds St (Footscray)', 3, 1, 50, 1, 1, CAST(N'2015-07-20 14:26:40.687' AS DateTime), 1, 217197482, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (14, 1, N'Glenferrie Station', N'Glenferrie Railway Station/Glenferrie Rd (Hawthorn)', 3, 1, 1, 1, 1, CAST(N'2015-07-20 14:26:41.607' AS DateTime), 1, 225631133, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (15, 1, N'Melbourne Central Station', N'La Trobe St/Swanston St (Melbourne City)', 3, 1, 1, 1, 1, CAST(N'2015-07-20 14:26:48.997' AS DateTime), 1, 82985095, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (16, 1, N'Melbourne Central Station', N'La Trobe St/Swanston St (Melbourne City)', 3, 1, 1, 1, 1, CAST(N'2015-07-20 14:26:43.237' AS DateTime), 1, 18656994, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (17, 1, N'Melbourne Central Station', N'Elizabeth St/La Trobe St (Melbourne City)', 3, 2, 1, 1, 1, CAST(N'2015-07-20 14:26:44.170' AS DateTime), 1, 25801063, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (18, 1, N'Parliament Station', N'Albert St/Nicholson St (Fitzroy)', 3, 1, 1, 1, 1, CAST(N'2015-07-20 14:26:43.713' AS DateTime), 1, 20855890, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (19, 1, N'Parliament Station', N'Parliament/Collins St (Melbourne City)', 3, 1, 1, 1, 1, CAST(N'2015-07-20 14:26:46.237' AS DateTime), 1, 42158863, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (20, 1, N'Parliament Station', N'Parliament/Collins St (Melbourne City)', 3, 1, 1, 1, 1, CAST(N'2015-07-20 14:26:43.870' AS DateTime), 1, 18504635, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (21, 1, N'Parliament Station', N'Parliament Railway Station/Macarthur St (East Melbourne)', 3, 1, 1, 1, 1, CAST(N'2015-07-20 14:26:45.620' AS DateTime), 1, 31581165, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (22, 1, N'South Yarra Station', N'Toorak Rd/Chapel St (South Yarra)', 3, 6, 170, 1, 1, CAST(N'2015-07-20 14:26:45.230' AS DateTime), 1, 24702749, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (23, 1, N'Southern Cross Station', N'Spencer St/Collins St (Melbourne City)', 3, 1, 1, 1, 1, CAST(N'2015-07-21 14:48:07.517' AS DateTime), 1, 16163232, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (24, 1, N'Southern Cross Station', N'Spencer St/Collins St (Melbourne City)', 3, 1, 1, 1, 1, CAST(N'2015-07-20 14:26:47.457' AS DateTime), 1, 37574472, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (25, 1, N'Southern Cross Station', N'Spencer St/Bourke St (Melbourne City)', 3, 1, 1, 1, 1, CAST(N'2015-07-20 14:26:45.730' AS DateTime), 1, 20112486, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (26, 1, N'Southern Cross Station', N'Batmans Hill/Collins St (Docklands)', 3, 1, 1, 1, 1, CAST(N'2015-07-20 14:26:45.683' AS DateTime), 1, 18054174, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (27, 1, N'Balaclava Station', N'Nelson St/Carlisle St (Balaclava)', 3, 1, 140, 1, 1, CAST(N'2015-07-20 14:26:46.357' AS DateTime), 1, 21848791, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (28, 1, N'Camberwell Station', N'Camberwell Station/Burke Rd (Hawthorn East)', 3, 6, 160, 1, 1, CAST(N'2015-07-20 14:26:46.537' AS DateTime), 1, 12941143, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (29, 1, N'Clifton Hill Station', N'Clifton Hill Interchange/Queens Pde (Clifton Hill)', 3, 2, 425, 1, 1, CAST(N'2015-07-20 14:26:47.063' AS DateTime), 1, 14350018, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (30, 1, N'Clifton Hill Station', N'Clifton Hill Interchange/Queens Pde (Clifton Hill)', 3, 2, 425, 1, 1, CAST(N'2015-07-20 14:26:47.063' AS DateTime), 1, 13657424, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (31, 1, N'Elsternwick Station', N'Elsternwick Railway Station/Glenhuntly Rd (Elsternwick)', 3, 6, 10, 1, 1, CAST(N'2015-07-20 14:26:46.783' AS DateTime), 1, 10426858, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (32, 2, N'Glenhuntly Station', N'Glenhuntly Railway Station/Glenhuntly Rd (Glen Huntly)', 3, 6, 1, 1, 1, CAST(N'2015-07-20 14:26:47.943' AS DateTime), 1, 16974102, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (33, 1, N'Hawthorn Station', N'Hawthorn Railway Station/Burwood Rd (Hawthorn)', 3, 6, 150, 1, 1, CAST(N'2015-07-20 14:26:47.380' AS DateTime), 1, 9995964, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (34, 1, N'Jolimont Station', N'Powlett St/Wellington Pde (East Melbourne)', 3, 6, 40, 1, 1, CAST(N'2015-07-20 14:26:48.433' AS DateTime), 1, 18838475, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (35, 1, N'Jolimont Station', N'Clarendon St/Wellington Pde (East Melbourne)', 3, 2, 40, 1, 1, CAST(N'2015-07-20 14:26:48.837' AS DateTime), 1, 20420970, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (36, 1, N'Kooyong Station', N'Kooyong Railway Station/Glenferrie Rd (Kooyong)', 3, 6, 1, 1, 1, CAST(N'2015-07-20 14:26:48.493' AS DateTime), 1, 14191095, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (37, 1, N'Malvern Station', N'Dandenong Rd/Glenferrie Rd (Malvern)', 3, 3, 55, 1, 1, CAST(N'2015-07-20 14:26:50.977' AS DateTime), 1, 38938188, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (38, 1, N'Newmarket Station', N'Newmarket Plaza/Racecourse Rd (Flemington)', 3, 6, 250, 1, 1, CAST(N'2015-07-20 14:26:48.497' AS DateTime), 1, 11160834, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (39, 1, N'North Richmond Station', N'North Richmond Railway Station/Victoria St (Richmond)', 3, 3, 40, 1, 1, CAST(N'2015-07-20 14:26:49.737' AS DateTime), 1, 23392681, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (40, 1, N'North Richmond Station', N'North Richmond Railway Station/Victoria St (Richmond)', 3, 3, 40, 1, 1, CAST(N'2015-07-20 14:26:49.667' AS DateTime), 1, 21987631, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (41, 1, N'Northcote Station', N'Separation St/High St (Northcote)', 3, 3, 410, 1, 1, CAST(N'2015-07-20 14:26:50.447' AS DateTime), 1, 24987688, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (42, 1, N'Northcote Station', N'Arthurton Rd/St Georges Rd (Northcote)', 3, 3, 480, 1, 1, CAST(N'2015-07-20 14:26:50.277' AS DateTime), 1, 21476672, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (43, 1, N'Richmond Station', N'Cremorne St/Swan St (Richmond)', 3, 1, 55, 1, 1, CAST(N'2015-07-20 14:26:49.717' AS DateTime), 1, 12752375, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (44, 1, N'Windsor Station', N'Chapel St/Dandenong Rd (Windsor)', 3, 2, 140, 1, 1, CAST(N'2015-07-20 14:26:50.403' AS DateTime), 1, 19111709, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (45, 1, N'Windsor Station', N'Windsor Railway Station/Chapel St (Windsor)', 3, 2, 140, 1, 1, CAST(N'2015-07-20 14:26:49.780' AS DateTime), 1, 12802597, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (46, 1, N'Anstey Station', N'Albion St/Sydney Rd (Brunswick)', 3, 6, 260, 1, 1, CAST(N'2015-07-20 14:26:49.990' AS DateTime), 1, 11464593, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (47, 1, N'Armadale Station', N'Armadale Station/High St (Armadale)', 3, 3, 120, 1, 1, CAST(N'2015-07-20 14:26:50.400' AS DateTime), 1, 14031311, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (48, 1, N'Batman Station', N'Gaffney St/Sydney Rd (Coburg North)', 3, 3, 1, 1, 1, CAST(N'2015-07-20 14:26:50.853' AS DateTime), 1, 11784900, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (49, 1, N'Brunswick Station', N'Albert St/Sydney Rd (Brunswick)', 3, 3, 300, 1, 1, CAST(N'2015-07-20 14:26:50.863' AS DateTime), 1, 11474670, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (50, 1, N'Burnley Station', N'Burnley St/Swan St (Burnley)', 3, 1, 50, 1, 1, CAST(N'2015-07-20 14:26:50.977' AS DateTime), 1, 12344893, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (51, 1, N'Burnley Station', N'Burnley St/Swan St (Burnley)', 3, 6, 1, 1, 1, CAST(N'2015-07-20 14:26:50.957' AS DateTime), 1, 11684194, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (52, 1, N'Coburg Station', N'Coburg Market/Sydney Rd (Coburg)', 3, 3, 390, 1, 1, CAST(N'2015-07-20 14:26:51.317' AS DateTime), 1, 13144705, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (53, 1, N'Croxton Station', N'Dennis St/High St (Northcote)', 3, 3, 230, 1, 1, CAST(N'2015-07-20 14:26:52.757' AS DateTime), 1, 24662609, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (54, 1, N'East Richmond Station', N'Church St/Swan St (Richmond)', 3, 2, 100, 1, 1, CAST(N'2015-07-20 14:26:53.090' AS DateTime), 1, 26744983, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (55, 1, N'East Richmond Station', N'East Richmond Railway Station/Church St (Richmond)', 3, 3, 1, 1, 1, CAST(N'2015-07-20 14:26:53.067' AS DateTime), 1, 26464649, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (56, 1, N'Gardiner Station', N'Malvern Rd/Burke Rd (Glen Iris)', 3, 1, 1, 1, 1, CAST(N'2015-07-20 14:26:51.467' AS DateTime), 1, 10069103, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (57, 1, N'Glen Iris Station', N'Glen Iris/High St (Glen Iris)', 3, 6, 160, 1, 1, CAST(N'2015-07-20 14:26:51.530' AS DateTime), 1, 6396824, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (58, 1, N'Jewell Station', N'Barkly Square/Sydney Rd (Brunswick)', 3, 1, 220, 1, 1, CAST(N'2015-07-20 14:26:52.177' AS DateTime), 1, 12858151, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (59, 1, N'Moreland Station', N'Moreland Rd/Sydney Rd (Coburg)', 3, 3, 1, 1, 1, CAST(N'2015-07-20 14:26:53.783' AS DateTime), 1, 28217199, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (60, 1, N'Prahran Station', N'Latrobe St/High St (Windsor)', 3, 3, 150, 1, 1, CAST(N'2015-07-20 14:26:52.177' AS DateTime), 1, 11947495, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (61, 1, N'Ripponlea Station', N'Glen Eira Rd/Brighton Rd (Elwood)', 3, 6, 400, 1, 1, CAST(N'2015-07-20 14:26:52.167' AS DateTime), 1, 11847424, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (62, 1, N'Riversdale Station', N'Spencer Rd/Riversdale Rd (Camberwell)', 3, 3, 170, 1, 1, CAST(N'2015-07-20 14:26:52.487' AS DateTime), 1, 11541738, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (63, 1, N'Royal Park Station', N'Royal Park Railway Station/Royal Park (Parkville)', 3, 1, 1, 1, 1, CAST(N'2015-07-20 14:26:52.850' AS DateTime), 1, 13689540, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (64, 1, N'Thornbury Station', N'Hutton St/St Georges Rd (Thornbury)', 3, 3, 150, 1, 1, CAST(N'2015-07-20 14:26:54.490' AS DateTime), 1, 29559846, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (65, 1, N'Toorak Station', N'Orrong Rd/Malvern Rd (Toorak)', 3, 6, 290, 1, 1, CAST(N'2015-07-20 14:26:53.657' AS DateTime), 1, 14825298, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (66, 1, N'Westgarth Station', N'Westgarth St/High St (Northcote)', 3, 6, 1, 1, 1, CAST(N'2015-07-20 14:26:53.747' AS DateTime), 1, 15619088, NULL, 0, 1)
GO
INSERT [dbo].[Configurations] ([Id], [OrientationMode], [PIDName], [Description], [NumOfLines], [DirectionsId], [Distance], [Enabled], [MonitorHeartBeat], [HeartbeatLastUpdatedTime], [MonitorPageProcessing], [PageProcessingTimeTaken], [NetworkName], [ErrorStatus], [DisplayOrderId]) VALUES (67, 1, N'Application team ', N'112 Eastern Road, South Melbourne', 3, 1, 50, 1, 1, CAST(N'2015-07-20 14:26:56.360' AS DateTime), 1, 41579771, NULL, 0, 1)
GO
SET IDENTITY_INSERT [dbo].[Configurations] OFF
GO
SET IDENTITY_INSERT [dbo].[Stops] ON 

GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2829, 1, 1758, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2830, 2, 1758, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2831, 3, 1089, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2832, 3, 2089, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2833, 4, 1089, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2834, 4, 2089, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2835, 5, 1302, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2836, 5, 2302, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2837, 6, 3064, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2838, 6, 3166, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2839, 7, 3064, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2840, 7, 3166, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2841, 8, 3113, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2842, 9, 3605, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2843, 9, 3705, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2844, 10, 3801, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2845, 12, 3604, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2846, 12, 3704, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2847, 13, 4364, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2848, 14, 1474, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2849, 14, 2474, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2850, 15, 3008, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2851, 15, 3108, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2856, 18, 3210, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2857, 18, 3310, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2858, 19, 3409, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2859, 19, 3509, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2862, 21, 3410, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2863, 21, 3510, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2864, 22, 4050, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2865, 22, 4150, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2866, 23, 3401, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2867, 23, 3501, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2868, 24, 3401, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2869, 24, 3501, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2870, 25, 3201, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2871, 25, 3301, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2872, 26, 3400, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2873, 26, 3500, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2874, 27, 1073, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2875, 27, 2073, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2876, 28, 1664, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2877, 28, 2664, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2878, 29, 1825, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2879, 29, 2825, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2880, 30, 1825, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2881, 30, 2825, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2882, 31, 1044, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2883, 31, 2044, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2884, 32, 1061, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2885, 32, 2060, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2886, 33, 1926, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2887, 33, 2926, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2888, 34, 3612, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2889, 34, 3712, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2890, 35, 3611, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2891, 35, 3711, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2892, 36, 1465, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2893, 36, 2465, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2896, 38, 1326, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2897, 38, 2326, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2898, 39, 1719, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2899, 39, 2719, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2900, 40, 1719, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2901, 40, 2719, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2902, 41, 1833, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2903, 41, 2833, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2904, 42, 1369, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2905, 42, 2369, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2906, 43, 1761, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2907, 43, 2761, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2908, 44, 1102, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2909, 44, 2102, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2910, 45, 4043, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2911, 45, 4143, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2912, 46, 1426, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2913, 46, 2426, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2914, 47, 1162, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2915, 47, 2162, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2916, 48, 1437, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2917, 48, 2437, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2918, 49, 1422, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2919, 49, 2422, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2920, 50, 1767, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2921, 50, 2767, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2922, 51, 1767, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2923, 51, 2767, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2924, 52, 1433, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2925, 52, 2433, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2926, 53, 1835, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2927, 53, 2835, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2928, 54, 1764, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2929, 54, 2764, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2930, 55, 4156, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2931, 56, 1651, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2932, 56, 2651, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2933, 57, 1175, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2934, 58, 1420, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2935, 58, 2420, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2936, 59, 1428, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2937, 59, 2428, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2938, 60, 1152, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2939, 60, 2152, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2940, 61, 1039, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2941, 61, 2039, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2942, 62, 1778, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2943, 62, 2778, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2944, 63, 1256, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2945, 63, 2256, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2946, 64, 1373, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2947, 64, 2373, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2948, 65, 1637, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2949, 65, 2637, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2950, 66, 1827, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2951, 66, 2827, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2956, 7, 3553, N'Ab')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2957, 7, 3653, N'Ab')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2958, 8, 3013, N'Ab')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2959, 11, 3604, N'ab')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2960, 11, 3704, N'ab')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2961, 11, 3801, N'ab')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2962, 16, 3556, N'ab')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2963, 16, 3656, N'ab')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2964, 17, 3805, N'ab')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2965, 17, 3905, N'ab')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2966, 20, 3309, N'ab')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2967, 20, 3209, N'ab')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2968, 22, 1565, N'ab')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2969, 22, 2565, N'ab')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2970, 67, 1024, N'a')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2971, 37, 1453, N'ab')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2972, 37, 2453, N'ab')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2973, 37, 1140, N'ab')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2974, 37, 2140, N'ab')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2975, 37, 1976, N'ab')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2976, 37, 2115, N'ab')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2977, 53, 1371, N'ab')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2978, 53, 2371, N'ab')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2979, 54, 4156, N'ab')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2980, 54, 4057, N'ab')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2981, 55, 1764, N'ab')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2982, 55, 2764, N'ab')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2983, 55, 4057, N'ab')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2984, 64, 1840, N'ab')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2985, 64, 2840, N'ab')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2986, 67, 2024, N'b')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2988, 6, 3553, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2989, 6, 3653, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2990, 59, 1551, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2991, 41, 1369, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2992, 41, 2369, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2993, 42, 1833, N'')
GO
INSERT [dbo].[Stops] ([Id], [ConfigurationId], [TrackerId], [StopText]) VALUES (2994, 42, 2833, N'')
GO
SET IDENTITY_INSERT [dbo].[Stops] OFF
GO
SET IDENTITY_INSERT [dbo].[GlobalSettings] ON 

GO
INSERT [dbo].[GlobalSettings] ([Id], [Name], [Value]) VALUES (1, N'GenericMessageEnabled', N'True')
GO
INSERT [dbo].[GlobalSettings] ([Id], [Name], [Value]) VALUES (2, N'GenericMessage', N'Real-time tram information on your mobile. Get tramTRACKER app for iPhone or Android. Follow @yarratrams on Twitter.')
GO
INSERT [dbo].[GlobalSettings] ([Id], [Name], [Value]) VALUES (3, N'DefaultDisruptionMessage', N'Route {0} is currently disrupted and delays may occur')
GO
SET IDENTITY_INSERT [dbo].[GlobalSettings] OFF
GO
SET IDENTITY_INSERT [dbo].[UserGroups] ON 

GO
INSERT [dbo].[UserGroups] ([Id], [Name], [Readonly], [SortOrder], [Enabled]) VALUES (2, N'SG-DEV-TramTracker Master Data -Readonly', 1, 1, 1)
GO
INSERT [dbo].[UserGroups] ([Id], [Name], [Readonly], [SortOrder], [Enabled]) VALUES (4, N'SG-DEV-TramTracker Master Data -Update', 0, 2, 1)
GO
SET IDENTITY_INSERT [dbo].[UserGroups] OFF
GO
