
PRINT N'Dropping table TSPIDS_Directions'
DROP TABLE TSPIDS_Directions

PRINT N'Dropping table TSPIDS_DisplayOrder'
DROP TABLE TSPIDS_DisplayOrder

PRINT N'Dropping table TSPIDS_PID'
DROP TABLE TSPIDS_PID

PRINT N'Dropping table TSPIDS_Stops'
DROP TABLE TSPIDS_Stops




-- No foreign keys
PRINT N'Dropping table TSPIDS_Log'
DROP TABLE TSPIDS_Log

PRINT N'Dropping table TSPIDS_LogService'
DROP TABLE TSPIDS_LogService

PRINT N'Dropping table TSPIDS_GlobalSettings'
DROP TABLE TSPIDS_GlobalSettings

PRINT N'Dropping table TSPIDS_PidsLog'
DROP TABLE TSPIDS_PidsLog

PRINT N'Dropping table TSPIDS_PidsElevatedLog'
DROP TABLE TSPIDS_PidsElevatedLog

PRINT N'Dropping table TSPIDS_PidsLogHist'
DROP TABLE TSPIDS_PidsLogHist

PRINT N'Dropping table TSPIDS_ConfigurationsHistory'
DROP TABLE TSPIDS_ConfigurationsHistory

PRINT N'Dropping table TSPIDS_ELMAH_Error'
DROP TABLE TSPIDS_ELMAH_Error

PRINT N'Dropping table TSPIDS_UserGroups'
DROP TABLE TSPIDS_UserGroups
