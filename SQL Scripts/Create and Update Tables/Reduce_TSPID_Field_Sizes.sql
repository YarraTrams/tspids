/*
This script will reduce column sizes of following tables:

	TSPIDS_ConfigurationsHistory
	TSPIDS_GlobalSettings
	TSPIDS_PidsLog
*/

Alter table TSPIDS_ConfigurationsHistory ALTER Column [Message] varchar(500) NULL
Alter table TSPIDS_ConfigurationsHistory ALTER Column [UserName] varchar(50) NOT NULL
Alter table TSPIDS_GlobalSettings ALTER Column [Name] varchar(50) NOT NULL
Alter table TSPIDS_PidsLog ALTER Column [Level] varchar(30) NOT NULL
Alter table TSPIDS_PidsLog ALTER Column [Message] varchar(30) NOT NULL
