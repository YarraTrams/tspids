﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebPids.EntityFramework.DapperModels
{
    public class StopInformation
    {
        public string FlagStopNo { get; set; }
        public string StopName { get; set; }
        public string CityDirection { get; set; }
        public string SuburbName { get; set; }
    }
}
