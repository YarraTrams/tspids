﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebPids.EntityFramework
{
    public partial class PidsLog : EntityBase
    {
        public override string ToString()
        {
            return string.Format("ConfigurationId {0} Date {1} Level {2} Message {3}", ConfigurationId, Date, Level, Message);
        }
    }
}
