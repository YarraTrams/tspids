﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;

namespace WebPids.EntityFramework
{
    /// <summary>
    /// Health status class 
    /// </summary>
    public class HealthStatus
    {
        #region Pre-defines
        // Wildcard
        public static readonly HealthStatus WildCard = new HealthStatus() { Color = Color.Empty, Text = "All" };
        // Disabled
        public static readonly HealthStatus Disabled = new HealthStatus() { Color = Color.LightGray, Text = "Disabled", Severity = 0 };
        // Healthy pid
        public static readonly HealthStatus Healthy = new HealthStatus() {Color = Color.LimeGreen, Text = "Healthy", Severity = 1};
        // Pid might have problems
        public static readonly HealthStatus Warning = new HealthStatus() {Color = Color.Yellow, Text = "Warning", Severity = 2};
        // Faulty pid
        public static readonly HealthStatus Faulty = new HealthStatus() {Color = Color.Red, Text = "Faulty", Severity = 3};
        #endregion

        public byte Severity { get; set; } 
        public string Text { get; set; }
        public Color Color { get; set; }

        /// <summary>
        /// Convert the conventional color to html
        /// </summary>
        public string HtmlColor
        {
            get
            {
                return Color.IsEmpty ? string.Empty : ColorTranslator.ToHtml(Color);
            }
        }
    }

    [MetadataType(typeof(ConfigurationMetaData))]
    public partial class Configuration : EntityBase
    {
        /// <summary>
        /// The max number of stops to display for the Edit page
        /// </summary>
        public static readonly int StopsPageSize = 3;

        /// <summary>
        /// The max number of errors to display for the Pid Health page
        /// </summary>
        public static readonly int ErrorPageSize = 10;

        /// <summary>
        /// PID errors
        /// </summary>
        public IEnumerable<PidsElevatedLog> Errors { get; set; }

        /// <summary>
        /// The health status color
        /// </summary>
        public HealthStatus HealthStatus { get; set; }

        /// <summary>
        /// URL to view the PID in real time
        /// </summary>
        public string BaseDisplayUrl { get; set; }

        /// <summary>
        /// URL to view the PID in real time
        /// </summary>
        public string DisplayUrl 
        { 
            get
            {
                return string.Format("{0}{1}", BaseDisplayUrl, Id);
            }
        }

        public override string ToString()
        {
            return string.Format("{0} - {1}", PIDName, Description);
        }

        public string MinutesSinceLastUpdate
        {
            get
            {
                var duration = Math.Floor(DateTime.Now.Subtract(HeartbeatLastUpdatedTime).TotalMinutes);
                if (duration > 320000)
                {
                    return "-";
                }
                if (duration <= 0)
                {
                    return "Now";
                }
                return duration.ToString();
            }
        }
    }

    public class ConfigurationMetaData
    {
        [Required(ErrorMessage = "Description is required")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Station Name is required")]
        public string StationName { get; set; }

        [Required(ErrorMessage = "PID Asset Location Number is required")]
        public string PIDAssetLocationNo { get; set; }

        public string VicTrackId { get; set; }

        [Required(ErrorMessage = "Distance is required")]
        [Range(0, 1000, ErrorMessage = "Distance should be between 0 and 1000")]
        public int Distance { get; set; }
    }
}
