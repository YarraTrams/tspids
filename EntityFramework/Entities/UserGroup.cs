﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebPids.EntityFramework
{
    public partial class UserGroup : EntityBase
    {
        public override string ToString()
        {
            return Name;
        }
    }
}
