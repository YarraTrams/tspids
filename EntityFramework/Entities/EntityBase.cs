﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace WebPids.EntityFramework
{
    public abstract class EntityBase : INotifyPropertyChanged
    {
        /// <summary>
        /// Backup for Id property
        /// </summary>
        private long id = -1;

        /// <summary>
        /// Get the Id of derived class.
        /// All our entities have a property 'Id' (same spelling).
        /// This base class property provides access to the derived class's Id property.
        /// This fools SQL queries in generic methods where only a base class for all entities can be provided.
        /// </summary>
        public long Id
        {
            get
            {
                if (id <= 0)
                {
                    var propertyInfo = this.GetType().GetProperty("Id");
                    if (propertyInfo != null)
                    {
                        id = (long)propertyInfo.GetValue(this);
                    }
                }

                return id;
            }
        }

        /// <summary>
        /// Override for Equals, which tests for equality of type and Ids.
        /// </summary>
        /// <param name="obj">The object to test</param>
        /// <returns>True if both objects are of the same type and have same Id, otherwise false</returns>
        public override bool Equals(object obj)
        {
            if (obj != null && obj.GetType() == this.GetType())
            {
                return this.Id == (obj as EntityBase).Id;
            }

            return base.Equals(obj);
        }

        #region PropertyChangedNotification

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public void RaisePropertyChanged(string propertyName)
        {
            this.OnPropertyChanged(propertyName);
        }

        #endregion


    }
}
