﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebPids.EntityFramework
{
    /// <summary>
    /// The direction class
    /// </summary>
    public partial class Direction : EntityBase
    {
        public override string ToString()
        {
            return string.Format("{0} - {1}", Direction1, ImageName);
        }
    }
}
