﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebPids.EntityFramework
{
    public partial class ConfigurationsHistory : EntityBase
    {
        public Configuration Configuration { get; set; }
    }
}
