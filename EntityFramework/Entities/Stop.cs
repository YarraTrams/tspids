﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebPids.EntityFramework
{
    [MetadataType(typeof(StopMetaData))]
    public partial class Stop : EntityBase
    {
        public override string ToString()
        {
            return string.Format("Route {0}", TrackerId);
        }
    }

    public class StopMetaData
    {
        [Required(ErrorMessage = "Tracker Id is required")]
        [Range(1, 999999, ErrorMessage = "Tracker Id must be between 1 and 999999")]
        public long TrackerId { get; set; }

        [Required(ErrorMessage = "Street Name is required")]
        [MaxLength(100, ErrorMessage = "Street Name should not exceed 100 characters.")]
        public string DisplayTitle { get; set; }

        [Required(ErrorMessage = "Distance is required")]
        [Range(1, 2000, ErrorMessage = "Distance must be between 1 and 2000")]
        public string Distance { get; set; }

        [Required(ErrorMessage = "Direction is required")]
        public string Direction { get; set; }

    }

}
