﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebPids.EntityFramework
{
    public partial class PidsElevatedLog : EntityBase
    {
        public static byte ErrorPageSize = 50;
        public override string ToString()
        {
            return string.Format("ConfigurationId {0} Date {1} Level {2} Message {3}", ConfigurationId, Date, Level, Message);
        }
    }
}
