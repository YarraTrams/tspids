﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebPids.EntityFramework 
{
    [MetadataType(typeof(GlobalSettingMetaData))]
    public partial class GlobalSetting : EntityBase
    {
        public override string ToString()
        {
            return string.Format("{0} - {1}", Name, Value);
        }
    }

    public class GlobalSettingMetaData
    {
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }

        [MaxLength(200, ErrorMessage = "Ticker message length should not exceed 200 characters.")]
        public string Value { get; set; }


    }
}
