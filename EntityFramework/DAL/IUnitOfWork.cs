﻿using System;
using System.Data.Entity;
 

namespace WebPids.EntityFramework 
{
    /// <summary>
    /// Provides a unit of work.
    /// All db access provided by this interface shares the same DbContext
    /// </summary>
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// Get a repository of type TEntity.
        /// It ensures internally that there is always only one incarnation of a repository for a given type.
        /// </summary>
        /// <typeparam name="TEntity">The type of entity (must derive from EntityBase)</typeparam>
        /// <returns>The repository</returns>
        IRepository<TEntity> GetRepository<TEntity>() where TEntity : EntityBase;

        /// <summary>
        /// Execute a SQL query directly on the database.
        /// </summary>
        /// <param name="sql">The SQL query.</param>
        /// <param name="parameters">Possible paramteres (includes output)</param>
        /// <returns>The status of the query (this is NOT the return value)</returns>
        int ExecuteSql(string sql, params object[] parameters);

        /// <summary>
        /// Detach an entity from the context.
        /// </summary>
        /// <param name="entity">The entity to be detached.</param>
        void Detach(object entity);

        /// <summary>
        /// Save all changes made by all repositories.
        /// </summary>
        /// <returns>The number of objects written.</returns>
        int SaveChanges();

        /// <summary>
        /// Provides access to the DbContext of this unit.
        /// </summary>
        /// <returns></returns>
        DbContext GetContext();

        /// <summary>
        /// Get the database name
        /// </summary>
        string DatabaseName { get; }
    }
}
