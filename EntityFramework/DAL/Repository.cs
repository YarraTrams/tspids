﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace WebPids.EntityFramework 
{/// <summary>
    /// The Repository provides access to all entities of a given type.
    /// All entities must derive from EntityBase
    /// </summary>
    /// <typeparam name="TEntity">The type of entity for this repository</typeparam>
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : EntityBase
    {
        /// <summary>
        /// The DbContext
        /// </summary>
        private readonly DbContext dbContext;

        /// <summary>
        /// The set of entities.
        /// </summary>
        private readonly IDbSet<TEntity> dbSet;

        private ObservableCollection<TEntity> collection;

        /// <summary>
        /// Initializes a new instance of the Repository class.
        /// </summary>
        /// <param name="context">The DbContext</param>
        public Repository(DbContext context)
        {
            this.dbContext = context;
            this.dbSet = context.Set<TEntity>();
            //dbContext.Configuration.AutoDetectChangesEnabled = false;
        }

        /// <summary>
        /// Default include properties
        /// </summary>
        public string DefaultIncludes { get; set; }

        /// <summary>
        /// Flag indicating whether or not the default includes should be used.
        /// This can be changed before any transaction.
        /// </summary>
        public bool UseDefaultIncludes { get; set; }

        /// <summary>
        /// Add a new entity.
        /// </summary>
        /// <param name="entity">The entity to add.</param>
        public void Add(TEntity entity)
        {
            this.dbSet.Add(entity);
        }

        /// <summary>
        /// Delete an entity.
        /// </summary>
        /// <param name="entity">The entity to be deleted.</param>
        public void Delete(TEntity entity)
        {
            this.dbSet.Remove(entity);
        }

        /// <summary>
        /// Get all entities of type TEntity.
        /// </summary>
        /// <param name="includeProperties">Optional include properties</param>
        /// <returns>All entities of type TEntity.</returns>
        public IQueryable<TEntity> GetAll(string includeProperties = "")
        {
            IQueryable<TEntity> query = this.dbSet;

            if (this.UseDefaultIncludes && string.IsNullOrEmpty(includeProperties))
            {
                includeProperties = this.DefaultIncludes;
            }

            foreach (var includeProperty in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            foreach (var item in query)
            {
                Reload(item);
            }

            return query;
        }

        /// <summary>
        /// This gets an ObservableCollection of all entities of type TEntity.
        /// The collection will be created on first call and the same collection will be returned on all subsequent calls.
        /// </summary>
        /// <param name="includeProperties">Optional include properties</param>
        /// <returns>An ObservableCollection of all entities of type TEntity</returns>
        public ObservableCollection<TEntity> GetCollection(string includeProperties = "")
        {
            if (collection == null)
            {
                collection = new ObservableCollection<TEntity>(this.GetAll(includeProperties));
            }

            return collection;
        }

        /// <summary>
        /// Get the first entity meeting the filter requirement.
        /// </summary>
        /// <param name="filter">The filter to be used for the query.</param>
        /// <returns>An entity.</returns>
        public TEntity GetOne(Expression<Func<TEntity, bool>> filter)
        {
            IQueryable<TEntity> query = this.dbSet;

            if (this.UseDefaultIncludes)
            {
                foreach (var includeProperty in this.DefaultIncludes.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProperty);
                }
            }

            return query.SingleOrDefault(filter);
        }

        /// <summary>
        /// Get entities meeting the filter requirement and order them.
        /// Examples:
        /// Get(orderBy: q => q.OrderBy(m => m.ImdbUrl));
        /// Get(filter: run => run.ScheduledDepartureTime > time, includeProperties: "SourceDepot,AssignedTram");
        /// </summary>
        /// <param name="filter">The (optional) filter for the query.</param>
        /// <param name="orderBy">The order function.</param>
        /// <param name="includeProperties">The incude properties as a comma-separated string of property names.</param>
        /// <returns>The requested entities.</returns>
        public IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null,
                                        Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
                                        string includeProperties = "")
        {
            IQueryable<TEntity> query = this.dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (this.UseDefaultIncludes && string.IsNullOrEmpty(includeProperties))
            {
                includeProperties = this.DefaultIncludes;
            }

            foreach (var includeProperty in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }

        /// <summary>
        /// Get an entity by Id.
        /// </summary>
        /// <param name="id">The Id of the entity.</param>
        /// <returns>The entity or null</returns>
        public TEntity GetById(long id)
        {
            if (this.UseDefaultIncludes)
            {
                // Get will automatically use DefaultIncludes if flag is set.
                var obj = this.Get(filter: e => e.Id == id).FirstOrDefault();
                if (obj != null)
                {
                    this.Reload(obj);
                }
                return obj;
            }
            else
            {
                var obj = this.dbSet.Find(id);
                if (obj != null)
                    this.Reload(obj);
                return obj;
            }
        }

        /// <summary>
        /// Reload an entity.
        /// </summary>
        /// <param name="entity">The entity to be reloaded.</param>
        public void Reload(TEntity entity)
        {
            this.dbContext.Entry(entity).Reload();
        }

        /// <summary>
        /// Detach an entity from the context.
        /// </summary>
        /// <param name="entity">The entity to be detached.</param>
        public void Detach(object entity)
        {
            ((IObjectContextAdapter)dbContext).ObjectContext.Detach(entity);
        }

        /// <summary>
        /// Save changes to db
        /// </summary>
        public void SaveAllContextChanges()
        {
            this.dbContext.SaveChanges();
        }

    public void BeginTransaction()
    {
        _transaction = dbContext.Database.BeginTransaction();
    }

    public void CommitTransaction()
    {
        _transaction.Commit();
        _transaction = null;
    }

    private DbContextTransaction _transaction = null;

    /// <summary>
        /// Get the connection handle
        /// </summary>
        /// <returns>the handle to the sql connection</returns>
        public DbConnection GetConnection()
        {
            return this.dbContext.Database.Connection;
        }
    }
}
