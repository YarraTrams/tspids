﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebPids.EntityFramework
{/// <summary>
    /// Interface providing access to a DbContext
    /// </summary>
    public interface IDbContext
    {
        DbSet<T> Set<T>() where T : class;
        //DbEntityEntry<T> Entry<T>(T entity) where T : class;
        int SaveChanges();
        void Dispose();
    }
}
