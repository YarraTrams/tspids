﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Common;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace WebPids.EntityFramework 
{ /// <summary>
    /// Interface providing access to a repository for entities of type TEntity
    /// </summary>
    /// <typeparam name="TEntity">The type of entity</typeparam>
    public interface IRepository<TEntity> where TEntity : EntityBase
    {
        /// <summary>
        /// Default include properties
        /// </summary>
        string DefaultIncludes { get; set; }

        /// <summary>
        /// Flag indicating whether or not the default includes should be used.
        /// This can be changed before any transaction.
        /// </summary>
        bool UseDefaultIncludes { get; set; }

        /// <summary>
        /// Add a new entity.
        /// </summary>
        /// <param name="entity">The entity to add.</param>
        void Add(TEntity entity);

        void BeginTransaction();
        void CommitTransaction();

        /// <summary>
        /// Delete an entity.
        /// </summary>
        /// <param name="entity">The entity to be deleted.</param>
        void Delete(TEntity entity);

        /// <summary>
        /// Get all entities of type TEntity.
        /// </summary>
        /// <param name="includeProperties">Optional include properties</param>
        /// <returns>All entities of type TEntity.</returns>
        IQueryable<TEntity> GetAll(string includeProperties = "");

        /// <summary>
        /// This gets an ObservableCollection of all entities of type TEntity.
        /// The collection will be created on first call and the same collection will be returned on all subsequent calls.
        /// </summary>
        /// <param name="includeProperties">Optional include properties</param>
        /// <returns>An ObservableCollection of all entities of type TEntity</returns>
        ObservableCollection<TEntity> GetCollection(string includeProperties = "");

        /// <summary>
        /// Get the first entity meeting the filter requirement.
        /// </summary>
        /// <param name="filter">The filter to be used for the query.</param>
        /// <returns>An entity.</returns>
        TEntity GetOne(Expression<Func<TEntity, bool>> filter);

        /// <summary>
        /// Get entities meeting the filter requirement and order them.
        /// Examples:
        /// Get(orderBy: q => q.OrderBy(m => m.ImdbUrl));
        /// Get(filter: run => run.ScheduledDepartureTime > time, includeProperties: "SourceDepot,AssignedTram");
        /// </summary>
        /// <param name="filter">The (optional) filter for the query.</param>
        /// <param name="orderBy">The order function.</param>
        /// <param name="includeProperties">The incude properties as a comma-separated string of property names.</param>
        /// <returns>The requested entities.</returns>
        IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null,
                                Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
                                string includeProperties = "");

        /// <summary>
        /// Get an entity by Id.
        /// </summary>
        /// <param name="id">The Id of the entity.</param>
        /// <returns>The entity or null</returns>
        TEntity GetById(long id);

        /// <summary>
        /// Reload an entity.
        /// </summary>
        /// <param name="entity">The entity to be reloaded.</param>
        void Reload(TEntity entity);

        /// <summary>
        /// Detach an entity from the context.
        /// </summary>
        /// <param name="entity">The entity to be detached.</param>
        void Detach(object entity);

        /// <summary>
        /// Commit changes to db
        /// </summary>
        void SaveAllContextChanges();

        /// <summary>
        /// Get the connection handle
        /// </summary>
        /// <returns>the handle to the sql connection</returns>
        DbConnection GetConnection();
    }
}
