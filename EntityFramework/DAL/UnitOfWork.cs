﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebPids.EntityFramework
{
    public class UnitOfWork<TContext> : IUnitOfWork where TContext : DbContext, new()
    {
        private readonly DbContext dbContext;
        private readonly Dictionary<Type, object> repositories = new Dictionary<Type, object>();
        private bool isDisposed;

        /// <summary>
        /// Initializes a new instance of the UnitOfWork class.
        /// </summary>
        public UnitOfWork()
        {
            dbContext = new TContext();
            this.isLazyLoading = dbContext.Configuration.LazyLoadingEnabled;
        }

        /// <summary>
        /// Represents the IsLazyLoading property.
        /// </summary>
        private bool isLazyLoading;

        /// <summary>
        /// Gets or sets the IsLazyLoading property.
        /// Changes to that property's value raise the PropertyChanged event.
        /// </summary>
        public bool IsLazyLoading
        {
            get { return this.isLazyLoading; }

            set
            {
                if (this.isLazyLoading != value)
                {
                    this.isLazyLoading = value;
                    this.dbContext.Configuration.LazyLoadingEnabled = value;
                }
            }
        }

        /// <summary>
        /// Get a repository of type TEntity.
        /// It ensures internally that there is always only one incarnation of a repository for a given type.
        /// </summary>
        /// <typeparam name="TEntity">The type of entity (must derive from EntityBase)</typeparam>
        /// <returns>The repository</returns>
        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : EntityBase
        {
            // Checks if the Dictionary Key contains the Model class
            if (this.repositories.Keys.Contains(typeof(TEntity)))
            {
                // Return the repository for that Model class
                return this.repositories[typeof(TEntity)] as IRepository<TEntity>;
            }

            // If the repository for that Model class doesn't exist, create it
            var repository = new Repository<TEntity>(dbContext);

            // Add it to the dictionary
            this.repositories.Add(typeof(TEntity), repository);

            return repository;
        }

        /// <summary>
        /// Save all changes made by all repositories.
        /// </summary>
        /// <returns>The number of objects written.</returns>
        public int SaveChanges()
        {
            try
            {
                return this.dbContext.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }

        }

        /// <summary>
        /// Provides access to the DbContext of this unit.
        /// </summary>
        /// <returns></returns>
        public DbContext GetContext()
        {
            return this.dbContext;
        }

        /// <summary>
        /// Execute a SQL query directly on the database.
        /// </summary>
        /// <param name="sql">The SQL query.</param>
        /// <param name="parameters">Possible paramteres (includes output)</param>
        /// <returns>The status of the query (this is NOT the return value)</returns>
        public int ExecuteSql(string sql, params object[] parameters)
        {
            return dbContext.Database.ExecuteSqlCommand(sql, parameters);
        }

        /// <summary>
        /// Detach an entity from the context.
        /// </summary>
        /// <param name="entity">The entity to be detached.</param>
        public void Detach(object entity)
        {
            ((IObjectContextAdapter)dbContext).ObjectContext.Detach(entity);
        }

        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!this.isDisposed)
            {
                if (disposing)
                {
                    this.dbContext.Dispose();
                }

                this.isDisposed = true;
            }
        }

        /// <summary>
        /// Get the database name
        /// </summary>
        public string DatabaseName
        {
            get
            {
                return string.Format("{0} on {1}", dbContext.Database.Connection.Database,
                    dbContext.Database.Connection.DataSource);
            }
        }
    }
}
