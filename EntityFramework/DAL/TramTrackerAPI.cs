﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using WebPids.EntityFramework.DapperModels;

namespace WebPids.EntityFramework.DAL
{
    public class TramTrackerAPI
    {
        // Internal connection to the database
        private readonly SqlConnection _connection;

        public TramTrackerAPI()
        {
            // Automatically open connection to tramtracker database on creation of API instance
            _connection = GetOpenConnection();
        }


        /// <summary>
        /// Calls TT stored procedure that returns detailed information regarding the stop (trackerid)
        /// </summary>
        /// <param name="stopId">The stop id to get detailed info on</param>
        /// <returns>Detailed stop information for the passed in stop</returns>
        /// <exception cref="TramTrackerAPIException">Error calling the GetStopInformation stored procedure</exception>
        public StopInformation GetStopInformation(long stopId)
        {
            try
            {
                // Write to the log stating a request for stop information has been made
                //_connection.Execute("[LogPIDSServiceRequest]");

                return _connection.Query<StopInformation>("[GetStopInformation]", new {StopNo = stopId},
                    commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            catch (Exception e)
            {

                throw new TramTrackerAPIException("Error calling the GetStopInformation stored procedure", e);
            }

        }

        /// <summary>
        /// Calls a TT stored procedure which, given a specific stop (tracker id) generates
        /// a set of predictions for the given stop
        /// It returns predictions for all routes on the stop
        /// </summary>
        /// <param name="stopId">The given stop or tracker id</param>
        /// <returns>A list of upcoming predictions for the given stop</returns>
        /// <exception cref="TramTrackerAPIException">Error calling the GetPredictionsForStop stored procedure</exception>
        public IEnumerable<StopPrediction> GetPredictionsForStop(Stop stopId)
        {
            List<StopPrediction> preds = new List<StopPrediction>();

            try
            {
                // Write to the log stating a request for stop information has been made
                //_connection.Execute("[LogPIDSServiceRequest]");

                List<StopPrediction> stops =
                    _connection.Query<StopPrediction>("[GetPredictionsForStop]",
                        new {StopNo = stopId.TrackerId, RouteNo = 0, lowFloor = 0},
                        commandType: CommandType.StoredProcedure).ToList();
                return stops;
            }
            catch (Exception e)
            {

                throw new TramTrackerAPIException("Error calling the GetPredictionsForStop stored procedure", e);
            }
        }

        /// <summary>
        /// Builds a list of all predictions for all passed in stops
        /// The results are managed as a single set of predictions
        /// Predictions can be separated by monitoring when the stop / tracker id 
        /// in the prediction result changes
        /// </summary>
        /// <param name="stopIds">A list of stops / tracker ids</param>
        /// <returns>A list of all predictions for a set of stops</returns>
        public IEnumerable<StopPrediction> GetPredictionsForStops(IEnumerable<Stop> stopIds)
        {
            List<StopPrediction> allPredictions = new List<StopPrediction>();
            foreach (Stop stopId in stopIds)
            {
                IEnumerable<StopPrediction> stops = GetPredictionsForStop(stopId);
                allPredictions.AddRange(stops);
            }
            return allPredictions;
        }

        /// <summary>
        /// Open a new connection to the tramtracker database
        /// Requires that the connection string TramTracker has been added to the {web/app}.config
        /// </summary>
        private SqlConnection GetOpenConnection()
        {
            try
            {
                return new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["TramTracker"].ConnectionString);
            }
            catch (TramTrackerAPIException e)
            {
                throw new TramTrackerAPIException("Error opening connection to TramTracker database", e);
            }
        }
    }
}
