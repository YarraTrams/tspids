﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebPids.EntityFramework
{
    /// <summary>
    /// Simple overlay of the exception object to indicate the exception was raised by the TramTrackerAPI object
    /// </summary>
    class TramTrackerAPIException : Exception
    {
        public TramTrackerAPIException(string message, Exception e):base(message, e)
        {
        }
    }
}
