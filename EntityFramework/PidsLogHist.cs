//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebPids.EntityFramework
{
    using System;
    using System.Collections.Generic;
    
    public partial class PidsLogHist
    {
        public System.DateTime LogDateHour { get; set; }
        public short ConfigurationId { get; set; }
        public int AvgResponseTimeInSec { get; set; }
    }
}
